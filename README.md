Hyperbolic Browser is a Java written software to get a better understanding of [Hyperbolic Geometry](https://en.wikipedia.org/wiki/Hyperbolic_geometry).

For now, you can download the [last build](https://framagit.org/Kii/hyperbolic-browser/builds/artifacts/master/download?job=validate%3Ajdk7).
Then you will explore and interact with the Poincaré Disk, the Poincaré Half-Plane and the Beltrami-Klein Disk.

Feature List :
- Create points, lines, segments and circles
- Drag and drop a point updating objects tied to it
- Select an object by clicking it (keep old selection by pressing Ctrl)
- Rename an object by right-clicking it in selection mode
- Change the selected objects color
- Switch space visualization between different models
- Show the hyperbolic length of a segment by selecting it
- Create a point in the middle of a segment
- Save and Load your hyperbolic spaces in files (.hyp)
- Apply dynamic Transformations  :
	- Elliptic Isometries (similar to Euclidean Rotations)
	- Hyperbolic Isometries (similar to Euclidean Translations)
- Tiling generator : 
	- Regular Polygon Tiling, with P the number of side a polygon has and Q the number of polygons around each vertex.
		- (P - 2)*(Q - 2) > 4 is mandatory in the hyperbolic space
	- Hyperbolic Tree with custom number of branches, depth and size
	- Polygon repetition by its vertex, depth and the length from the center to a vertex
		- An interesting one is with 4 vertices and a distance of 0.5305
	- Polygon repetition by its sides, depth and the length from the center to a vertex
	- Random Path to show the chaotic nature of random moving in this space 
	- Circle Tree to plot some evenly spaced circles and see some hypercycles
	
Bug List :
- Some lines and segments are not well drawn
	- when a point lie on the vertical or horizontal axis in Poincaré's Disk
	- when the line is vertical in Poincaré's Half-Plane
- Some lines are drawn multiple times
	- when a point lie on the horizontal axis in Poincaré's Disk
- Sometimes the selection miss its target
    - when multiple segments lie on the same geodesic in Poincaré's Disk
- Some points are generated multiple times in the regular polygon tiling leading to poor performances 
	
Future Feature List : 
- Implement Polygon and Paths as new drawable objects
- Resize drawing lines of an object
- Place a point on an object and make it stick to it if the object is modified
- Make an object visible or invisible
- Be able to place infinite points
- Find intersection(s) between objects
- Find the angle between two geodesics
- Implement the Ray Casting algorithm : Is a point inside a polygon ?
- Add custom Möbius Transforms
- Add other interesting repeating patterns
- Increase performances by using GPU calculus via JOCL

Background and thanks
- When graduating a M.D in Engineering and Computer Sciences at ISEN Lille
- Worked with Rémi Rousselle during college (from October 2012 to March 2013)
- Gabriel Chênevert and Benjamin Parent was our maths teachers
- The French wikipedian [Theon](https://fr.wikipedia.org/wiki/Utilisateur:Theon) for providing me formulas to draw Hyperbolic Circles in the Beltrami-Klein model
- The [JavaGeom Project](http://geom-java.sourceforge.net/) which I partially used to plot ellipses

Useful resources :
- [Hyperbolic Trigonometry](http://www.maths.gla.ac.uk/wws/cabripages/hyperbolic/hypertrig.html)
- [Online App by Malin Christersson](http://www.malinc.se/m/ImageTiling.php)
- [Tilings of the hyperbolic space and their visualization by Bulatov](http://bulatov.org/math/1101/Bulatov%20V.%20(2011)%20Tilings%20of%20the%20hyperbolic%20space%20and%20their%20visualization.pdf)
- [How to generate Regular Tiling by Dunham](https://www.d.umn.edu/~ddunham/dunisam07.pdf)
