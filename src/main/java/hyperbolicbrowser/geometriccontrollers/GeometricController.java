/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometriccontrollers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricviews.GeometricView;
import hyperbolicbrowser.geometricviews.ObjectModificationFrame;
import hyperbolicbrowser.listeners.ActionUserListener;
import hyperbolicbrowser.toolbar.ActionsEnum;
import hyperbolicbrowser.utils.mUtils;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Rousselle RÃ©mi (remi.rousselle@isen-lille.fr)
 */
public abstract class GeometricController implements ActionUserListener, MathematicTranslation {

    protected HyperboloidSpace model;
    //protected GeometricView view;

    protected static int numberClickedGeneratedPoints = 0;
    protected static int numberClickedGeneratedLines = 0;
    protected static int numberClickedGeneratedSegments = 0;
    protected static int numberClickedGeneratedCircles = 0;
    protected EuclideanPoint grabbedPoint, anchorComplexPoint;
    protected ActionsEnum currentAction;
    protected static final String pointType1 = "Hyperboloid (X, Y, Z, W)";
    protected static final String pointType2 = "Complex (X, Y)";
    protected static final String pointType3 = "Complex (R, Theta)";

    public static final EuclideanPoint pointI = new EuclideanPoint(0, 1);

    protected double distancePixelMax = 10d;

    public GeometricController(HyperboloidSpace model) {
        this.model = model;
    }

    @Override
    public abstract TransformablePoint hyperboloidToHyperbolicModel(HyperboloidPoint p);

    @Override
    public abstract HyperboloidPoint hyperbolicModelPointToHyperboloid(TransformablePoint p);

    @Override
    public abstract double distanceSegmentHyperbolic(TransformablePoint p1, TransformablePoint p2);

    public abstract double getAngle(EuclideanPoint complexPoint);

    public abstract TransformablePoint getMiddlePoint(TransformablePoint p1, TransformablePoint p2);

    public void rotate(double angle, List<TransformablePoint> points) {
        TransformablePoint.rotate(angle, points);
    }

    public void translate(double distance, List<TransformablePoint> points) {
        PoincareHalfPlanePoint.translate(distance, points);
    }

    @Override
    public void currentActionChanged(GeometricView view, ActionsEnum ae) {
        if (ae != null) {
            //If the CLEAR button is clicked two times in a row
            if (ae == ActionsEnum.CLEAR && currentAction == ActionsEnum.CLEAR) {
                clearModel();
            }
            //If the suppress button is clicked two times in a row
            else if (ae == ActionsEnum.REMOVE && currentAction == ActionsEnum.REMOVE) {
                removeSelectedObjects();
            }

            if(ae == ActionsEnum.SELECT || ae == ActionsEnum.ADD_POINT) {
                view.addMouseMotionListener(view);
            } else {
                view.removeMouseMotionListener(view);
            }
            currentAction = ae;
        }
    }

    @Override
    public void clearModel() {
        model.clearModel();
    }

    @Override
    public void removeSelectedObjects() {
        ArrayList<PrintablePoint> points = new ArrayList<>(model.getPoints());
        for (PrintablePoint p : points) {
            if (p.isSelected()) {
                model.removePoint(p);
            }
        }

        ArrayList<PrintablePath> lines = new ArrayList<>(model.getLines());
        for (PrintablePath l : lines) {
            if (l.isSelected()) {
                model.removeLine(l);
            }
        }

        ArrayList<PrintablePath> segments = new ArrayList<>(model.getSegments());
        for (PrintablePath s : segments) {
            if (s.isSelected()) {
                model.removeSegment(s);
            }
        }

        ArrayList<PrintableCircle> circles = new ArrayList<>(model.getCircles());
        for(PrintableCircle c : circles) {
            if(c.isSelected()) {
                model.removeCircle(c);
            }
        }
    }

    public void deselectObjects() {
        for (PrintablePoint p : model.getPoints()) {
            if (p.isSelected()) {
                model.selectPoint(p);
            }
        }
        for (PrintablePath l : model.getLines()) {
            if (l.isSelected()) {
                model.selectLine(l);
            }
        }
        for (PrintablePath s : model.getSegments()) {
            if (s.isSelected()) {
                model.selectSegment(s);
            }
        }
        for(PrintableCircle c : model.getCircles()) {
            if(c.isSelected()) {
                model.selectCircle(c);
            }
        }
    }

    @Override
    public void mousePressed(GeometricView view, int x, int y) {
        if (currentAction != null) {
            if (view.isPixelInsideView(x, y)) {
                EuclideanPoint clickedPoint = new EuclideanPoint(x, y, "clickedPoint");
                switch (currentAction) {
                    case GRAB:
                        grabPoint(view, clickedPoint);
                        break;
                    case DYNAMIC_TRANSLATE:
                        translateDynamic(view, clickedPoint);
                        break;
                    case ROTATE:
                        rotateDynamic(view, clickedPoint);
                        break;
                }
            }
        }
    }

    @Override
    public void mouseReleased(GeometricView view, MouseEvent e) {

        boolean ctrlPressed = ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK);
        boolean leftClick = (e.getButton() == MouseEvent.BUTTON1);
        int x = e.getX();
        int y = e.getY();
        EuclideanPoint clickedPixelPoint = new EuclideanPoint(x, y, "clickedPoint");

        if (leftClick && view.isPixelInsideView(x, y)) {
            switch (currentAction) {
                case SELECT:
                    selectObjectFromPoint(view, clickedPixelPoint, ctrlPressed);
                    break;
                case ADD_POINTS:
                    addPoints(view);
                    break;
                case ADD_POINT:
                    clickedPixelPoint.setName("Point" + numberClickedGeneratedPoints);
                    model.addPoint(view.pixelToHyperboloid(clickedPixelPoint));
                    numberClickedGeneratedPoints++;
                    break;
                case ADD_LINE:
                    addLine(view, clickedPixelPoint);
                    break;
                case ADD_SEGMENT:
                    addSegment(view, clickedPixelPoint);
                    break;
                case ADD_MIDDLE_POINT:
                    addMiddlePoint(view, clickedPixelPoint);
                    break;
                case ADD_CIRCLE:
                    addCircle(view, clickedPixelPoint);
                    break;
                case GRAB:
                    release(view);
                    break;
                case CENTER_POINT:
                    centerPoint(view, clickedPixelPoint);
                    break;
                case ROTATE:
                    rotateDynamicStop(view);
                    break;
                case DYNAMIC_TRANSLATE:
                    translateDynamicStop(view);
                    break;
            }
        } else if(!leftClick && view.isPixelInsideView(x, y)){
            if (currentAction == ActionsEnum.SELECT) {
                if (view.isPixelInsideView(x, y)) {
                    //selectObjectFromPoint(view, clickedPixelPoint, false);
                    List<PrintableObject> selection = getSelection();
                    if (selection.size() == 1) {
                        PrintableObject modificationObject = selection.get(0);

                        ObjectModificationFrame modificationFrame = new ObjectModificationFrame(model, modificationObject);
                        modificationFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        modificationFrame.setLocation(e.getLocationOnScreen().x, e.getLocationOnScreen().y);
                        modificationFrame.setVisible(true);
                        modificationFrame.setSize(100, 180);
                    } else {
                        ObjectModificationFrame modificationFrame = new ObjectModificationFrame(model, selection);
                        modificationFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        modificationFrame.setLocation(e.getLocationOnScreen().x, e.getLocationOnScreen().y);
                        modificationFrame.setVisible(true);
                        modificationFrame.setSize(100, 180);
                    }
                }
            }
        }
    }

    public List<PrintableObject> getSelection() {
        List<PrintableObject> selection = new LinkedList<>();
        for (PrintablePoint p : model.getPoints()) {
            if (p.isSelected()) {
                selection.add(p);
            }
        }
        for (PrintablePath l : model.getLines()) {
            if (l.isSelected()) {
                l.setBounded(false);
                selection.add(l);
            }
        }
        for (PrintablePath s : model.getSegments()) {
            if (s.isSelected()) {
                s.setBounded(true);
                selection.add(s);
            }
        }
        for (PrintableCircle c : model.getCircles()) {
            if (c.isSelected()) {
                selection.add(c);
            }
        }
        return selection;
    }

    @Override
    public void mouseMoved(GeometricView view, int x, int y) {
        EuclideanPoint mousePos = new EuclideanPoint(x, y, "movingPoint");
        switch (currentAction) {
            case SELECT:
                model.clearFlyOver();
                if(view.isPixelInsideView(mousePos)) {
                    flyOverPoint(view, mousePos);
                    flyOverSegment(view, mousePos);
                    flyOverLine(view, mousePos);
                    flyOverCircle(view, mousePos);
                }
                break;
            case ADD_POINT:
                addPixelPoint(view, mousePos);
                break;
            case GRAB:
                moveGrabbedPoint(view, mousePos);
                break;
            case DYNAMIC_TRANSLATE:
                translateDynamicMove(view, mousePos);
                break;
            case ROTATE:
                rotateDynamicMove(view, mousePos);
                break;
            case ADD_CIRCLE:
                if (centerPlaced) {
                    changeRadiusCircle(view, mousePos);
                }
                break;
        }
    }

    private HyperboloidPoint TechnicalPoint;
    private static final String TechnicalPointName = "MovingPointTechnical";

    public void addPixelPoint(GeometricView view, EuclideanPoint mousePos) {
        HyperboloidPoint hyperboloidMousePos = view.pixelToHyperboloid(mousePos);
        model.clearFlyOver();
        if(TechnicalPoint == null) {
            if(view.isPixelInsideView(mousePos)) {
                TechnicalPoint = new HyperboloidPoint(TechnicalPointName, hyperboloidMousePos);
                model.addPoint(TechnicalPoint);
            }
        } else {
            if(!view.isPixelInsideView(mousePos)) {
                model.removePoint(TechnicalPoint);
                TechnicalPoint = null;
            } else {
                flyOverPoint(view, mousePos);

                PrintablePoint p = model.getPointByName(TechnicalPointName);
                HyperboloidPoint hypP = (HyperboloidPoint) p;
                hypP.setCoords(hyperboloidMousePos);
                model.firePointMovedEvent(hypP);
            }
        }
    }

    public boolean flyOverPoint(GeometricView view, EuclideanPoint pixelPoint) {
        Map<PrintableObject, Double> selection = new HashMap<>();
        boolean flyOverSomething = false;
        boolean flyOver = selectPoint(view, pixelPoint, selection);
        if (flyOver) {
            Double min = Double.MAX_VALUE;
            PrintableObject closeObject;
            for (PrintableObject obj : selection.keySet()) {
                if (selection.get(obj) < min && !TechnicalPointName.equals(obj.getName())) {
                    min = selection.get(obj);
                    closeObject = obj;
                    flyOverSomething = true;
                    if (closeObject instanceof PrintablePoint) {
                        view.getInternModel().flyOverPoint((PrintablePoint) closeObject, true);
                    }
                }
            }
        }
        return flyOverSomething;
    }

    public boolean flyOverSegment(GeometricView view, EuclideanPoint pixelPoint) {
        Map<PrintableObject, Double> selection = new HashMap<>();
        boolean flyOverSomething = false;
        boolean flyOver = selectSegment(view, pixelPoint, selection);
        if (flyOver) {
            Double min = Double.MAX_VALUE;
            PrintableObject closeObject;
            for (PrintableObject obj : selection.keySet()) {
                if (selection.get(obj) < min) {
                    min = selection.get(obj);
                    closeObject = obj;
                    flyOverSomething = true;
                    if (closeObject instanceof PrintablePath) {
                        view.getInternModel().flyOverSegment((PrintablePath) closeObject, true);
                    }
                }
            }
        }
        return flyOverSomething;
    }

    public boolean flyOverLine(GeometricView view, EuclideanPoint pixelPoint) {
        Map<PrintableObject, Double> selection = new HashMap<>();
        boolean flyOverSomething = false;
        boolean flyOver = selectLine(view, pixelPoint, selection);
        if (flyOver) {
            Double min = Double.MAX_VALUE;
            PrintableObject closeObject;
            for (PrintableObject obj : selection.keySet()) {
                if (selection.get(obj) < min) {
                    min = selection.get(obj);
                    closeObject = obj;
                    flyOverSomething = true;
                    if (closeObject instanceof PrintablePath) {
                        view.getInternModel().flyOverLine((PrintablePath) closeObject, true);
                    }
                }
            }
        }
        return flyOverSomething;
    }

    public boolean flyOverCircle(GeometricView view, EuclideanPoint pixelPoint) {
        Map<PrintableObject, Double> selection = new HashMap<>();
        boolean flyOverSomething = false;
        boolean flyOver = selectCircle(view, pixelPoint, selection);
        if (flyOver) {
            Double min = Double.MAX_VALUE;
            PrintableObject closeObject;
            for (PrintableObject obj : selection.keySet()) {
                if (selection.get(obj) < min) {
                    min = selection.get(obj);
                    closeObject = obj;
                    flyOverSomething = true;
                    if (closeObject instanceof PrintableCircle) {
                        view.getInternModel().flyOverCircle((PrintableCircle) closeObject, true);
                    }
                }
            }
        }
        return flyOverSomething;
    }

    public boolean selectPoint(GeometricView view, EuclideanPoint clickedPoint, Map<PrintableObject, Double> selection) {
        double distance;
        boolean hasAlreadySelectSomething = false;
        for (PrintablePoint point : view.getInternModel().getPoints()) {
            if (point instanceof EuclideanPoint) {
                EuclideanPoint eP = (EuclideanPoint) point;
                distance = clickedPoint.distance(eP);
                if (distance < distancePixelMax) {
                    selection.put(point, distance);
                    hasAlreadySelectSomething = true;
                }
            }
        }
        return hasAlreadySelectSomething;
    }

    public boolean selectSegment(GeometricView view, EuclideanPoint clickedPoint, Map<PrintableObject, Double> selection) {
        double distance;
        boolean hasAlreadySelectSomething = false;
        for (PrintablePath segment : view.getInternModel().getSegments()) {
            distance = view.distancePixelFromPointToHyperbolicLine(clickedPoint, segment, true);
            if (distance < distancePixelMax) {
                segment.setBounded(true);
                selection.put(segment, distance);
                hasAlreadySelectSomething = true;
            }
        }
        return hasAlreadySelectSomething;
    }

    public boolean selectLine(GeometricView view, EuclideanPoint clickedPoint, Map<PrintableObject, Double> selection) {
        double distance;
        boolean hasAlreadySelectSomething = false;
        for (PrintablePath line : view.getInternModel().getLines()) {
            distance = view.distancePixelFromPointToHyperbolicLine(clickedPoint, line, false);
            if (distance < distancePixelMax) {
                line.setBounded(false);
                selection.put(line, distance);
                hasAlreadySelectSomething = true;
            }
        }
        return hasAlreadySelectSomething;
    }

    public boolean selectCircle(GeometricView view, EuclideanPoint clickedPoint, Map<PrintableObject, Double> selection) {
        double distance;
        boolean hasAlreadySelectSomething = false;
        for(PrintableCircle circle : view.getInternModel().getCircles()) {
            distance = view.distancePixelFromPointToCircle(clickedPoint, circle);
            if (distance < distancePixelMax) {
                selection.put(circle, distance);
                hasAlreadySelectSomething = true;
            }
        }
        return hasAlreadySelectSomething;
    }

    @Override
    public void selectObjectFromPoint(GeometricView view, EuclideanPoint clickedPoint, boolean keepOldSelection) {

        if(!view.isPixelInsideView(clickedPoint)) {
            model.clearSelectedObjects();
            return;
        }

        Map<PrintableObject, Double> newSelectedObjects = new HashMap<>();

        if(!keepOldSelection) {
            model.clearSelectedObjects();
        }

        boolean somethingSelected;
        somethingSelected = selectPoint(view, clickedPoint, newSelectedObjects)
                | selectSegment(view, clickedPoint, newSelectedObjects)
                | selectLine(view, clickedPoint, newSelectedObjects)
                | selectCircle(view, clickedPoint, newSelectedObjects);

        if(somethingSelected) {
            List<PrintablePoint> newSelectedPoints = new ArrayList<>();
            List<PrintablePath> newSelectedLines = new ArrayList<>();
            List<PrintablePath> newSelectedSegments = new ArrayList<>();
            List<PrintableCircle> newSelectedCircles = new ArrayList<>();
            for (PrintableObject obj : newSelectedObjects.keySet()) {
                if (obj instanceof PrintablePoint) {
                    newSelectedPoints.add((PrintablePoint) obj);
                } else if (obj instanceof PrintablePath) {
                    PrintablePath line = (PrintablePath) obj;
                    if (line.isBounded()) {
                        newSelectedSegments.add(line);
                    } else {
                        newSelectedLines.add(line);
                    }
                } else if(obj instanceof PrintableCircle) {
                    newSelectedCircles.add((PrintableCircle) obj);
                }
            }
            selectPoints(newSelectedPoints);
            selectSegments(newSelectedSegments);
            selectLines(newSelectedLines);
            selectCircles(newSelectedCircles);
        }
    }

    @Override
    public void addLine(GeometricView view, EuclideanPoint clickedPoint) {
        EuclideanPoint p = view.getLastSelectedPoint();
        Map<EuclideanPoint, Double> closest = view.getInternModel().getClosestPoint(clickedPoint);
        if (p != null) {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                if (closestPoint != null && closestPoint != p) {
                    model.addLine(new PrintablePath(p, closestPoint, "Line" + numberClickedGeneratedLines));
                    numberClickedGeneratedLines++;
                    view.setLastSelectedPoint(null);
                }
            }
        } else {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                view.setLastSelectedPoint(closestPoint);
            }
        }
    }

    public void addMiddlePoint(GeometricView view, EuclideanPoint clickedPoint) {
        EuclideanPoint p = view.getLastSelectedPoint();
        Map<EuclideanPoint, Double> closest = view.getInternModel().getClosestPoint(clickedPoint);
        if (p != null) {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                if (closestPoint != null && closestPoint != p) {
                    model.addMiddlePoint(new PrintablePath(view.pixelToHyperboloid(p), view.pixelToHyperboloid(closestPoint), ""));
                    view.setLastSelectedPoint(null);
                }
            }
        } else {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                if (closestPoint != null) {
                    view.setLastSelectedPoint(closestPoint);
                }
            }
        }
    }

    private boolean centerPlaced = false;
    private boolean newCircleStarted = false;

    public void addCircle(GeometricView view, EuclideanPoint clickedPoint) {
        if (!centerPlaced) { //le premier click
            HyperboloidPoint hyperboloidCenter = null;
            Map<PrintableObject, Double> closeEnoughPoints = new HashMap<>();
            boolean foundPoint = selectPoint(view, clickedPoint, closeEnoughPoints);
            if(foundPoint && closeEnoughPoints.size() == 1) {
                PrintablePoint point;
                for(PrintableObject obj : closeEnoughPoints.keySet()) {
                    if(obj instanceof PrintablePoint) {
                        point = (PrintablePoint) obj;
                        PrintablePoint center = model.getPointByName(point.getName());
                        if(center instanceof HyperboloidPoint) {
                            hyperboloidCenter = (HyperboloidPoint) center;
                        }
                    }
                }
            } else {
                hyperboloidCenter = view.pixelToHyperboloid(clickedPoint);
                hyperboloidCenter.setName("center" + numberClickedGeneratedCircles);
                model.addPoint(hyperboloidCenter);
            }
            tmpCircle = new PrintableCircle(hyperboloidCenter, hyperboloidCenter, "Circle" + numberClickedGeneratedCircles);
            centerPlaced = true;
            newCircleStarted = true;
            view.addMouseMotionListener(view);
        } else { //the second click
            numberClickedGeneratedCircles++;
            centerPlaced = false;
            view.removeMouseMotionListener(view);
        }
    }

    private PrintableCircle tmpCircle = null;

    public void changeRadiusCircle(GeometricView view, EuclideanPoint mousePosition) {
        if (centerPlaced && view.isPixelInsideView(mousePosition)) {
            String radiusPointName = "radius" + numberClickedGeneratedCircles;
            HyperboloidPoint radiusPoint = view.pixelToHyperboloid(mousePosition);
            radiusPoint.setName(radiusPointName);

            if (!newCircleStarted) {
                PrintablePoint p = model.getPointByName(radiusPointName);
                if(p instanceof HyperboloidPoint) {
                    HyperboloidPoint hp = (HyperboloidPoint) p;
                    hp.setCoords(radiusPoint);
                    model.firePointMovedEvent(p);
                }
            } else {
                model.addPoint(radiusPoint);
                tmpCircle.setPointOnCircle(radiusPoint);
                model.addCircle(tmpCircle);
                newCircleStarted = false;
            }
        }
    }

    @Override
    public void addSegment(GeometricView view, EuclideanPoint clickedPoint) {
        EuclideanPoint p = view.getLastSelectedPoint();
        Map<EuclideanPoint, Double> closest = view.getInternModel().getClosestPoint(clickedPoint);
        if (p != null) {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                if (closestPoint != null && closestPoint != p) {
                    model.addSegment(new PrintablePath(p, closestPoint, "Segment" + numberClickedGeneratedSegments));
                    numberClickedGeneratedSegments++;
                    view.setLastSelectedPoint(null);
                }
            }
        } else {
            for (EuclideanPoint closestPoint : closest.keySet()) {
                if (closestPoint != null) {
                    view.setLastSelectedPoint(closestPoint);
                }
            }
        }
    }

    @Override
    public void grabPoint(GeometricView view, EuclideanPoint pixelClickedPoint) {
        Map<EuclideanPoint, Double> closest = view.getInternModel().getClosestPoint(pixelClickedPoint);
        for (EuclideanPoint closestPoint : closest.keySet()) {
            grabbedPoint = closestPoint;
            if (pixelClickedPoint.distance(grabbedPoint) > distancePixelMax) {
                grabbedPoint = null;
            } else {
                moveGrabbedPoint(view, pixelClickedPoint);
                view.addMouseMotionListener(view);
            }
        }
    }

    @Override
    public void moveGrabbedPoint(GeometricView view, EuclideanPoint pixelClickedPoint) {
        if (view.isPixelInsideView((int) pixelClickedPoint.getX(), (int) pixelClickedPoint.getY())) {
            HyperboloidPoint hCursorPos = view.pixelToHyperboloid(pixelClickedPoint);

            HyperboloidPoint hp = (HyperboloidPoint) model.getPointByName(grabbedPoint.getName());
            double w = hCursorPos.getW();

            double x = hCursorPos.getX() / w;
            double y = hCursorPos.getY() / w;
            double z = hCursorPos.getZ() / w;
            hp.setCoords(x, y, z, w);
            model.firePointMovedEvent(hp);
        }
    }

    @Override
    public void release(GeometricView view) {
        view.removeMouseMotionListener(view);
    }

    @Override
    public void centerPoint(GeometricView view, EuclideanPoint clickedPoint) {
        Map<EuclideanPoint, Double> closest = view.getInternModel().getClosestPoint(clickedPoint);
        for (EuclideanPoint pixelPoint : closest.keySet()) {
            translatePoints(view.pixelToComplexEuclidean(pixelPoint));
        }
    }

    @Override
    public void selectPoints(List<PrintablePoint> selection) {
        for (PrintablePoint p : selection) {
            PrintablePoint modelP = model.getPointByName(p.getName());
            model.selectPoint(modelP, true);
        }
    }

    @Override
    public void selectLines(List<PrintablePath> selection) {
        for (PrintableObject l : selection) {
            PrintablePath modelL = model.getLineByName(l.getName());
            model.selectLine(modelL, true);
        }
    }

    @Override
    public void selectSegments(List<PrintablePath> selection) {
        for (PrintableObject l : selection) {
            PrintablePath internS = model.getSegmentByName(l.getName());
            model.selectSegment(internS, true);
        }
    }

    @Override
    public void selectCircles(List<PrintableCircle> selection) {
        for (PrintableObject l : selection) {
            PrintableCircle internC = model.getCircleByName(l.getName());
            model.selectCircle(internC, true);
        }
    }

    @Override
    public void translateDynamic(GeometricView view, EuclideanPoint clickedPoint) {
        view.addMouseMotionListener(view);
        if (anchorComplexPoint == null) {
            anchorComplexPoint = view.pixelToComplexEuclidean(clickedPoint);
        } else {
            translateDynamicMove(view, clickedPoint);
        }
    }

    @Override
    public void translateDynamicMove(GeometricView view, EuclideanPoint clickedPoint) {
        if (view.isPixelInsideView((int) clickedPoint.getX(), (int) clickedPoint.getY())) {
            EuclideanPoint complexClicked = view.pixelToComplexEuclidean(clickedPoint);
            EuclideanPoint vector = EuclideanPoint.sum(anchorComplexPoint, EuclideanPoint.multiply(complexClicked, -1d));
            anchorComplexPoint = complexClicked;
            translatePoints(vector);
        }
    }

    @Override
    public void translateDynamicStop(GeometricView view) {
        anchorComplexPoint = null;
        view.removeMouseMotionListener(view);
    }
    
    @Override
    public void rotateDynamic(GeometricView view, EuclideanPoint clickedPoint) {
        view.addMouseMotionListener(view);
        if (anchorComplexPoint == null) {
            anchorComplexPoint = view.pixelToComplexEuclidean(clickedPoint);
        } else {
            rotateDynamicMove(view, clickedPoint);
        }
    }

    @Override
    public void rotateDynamicMove(GeometricView view, EuclideanPoint clickedPoint) {
        if (view.isPixelInsideView((int) clickedPoint.getX(), (int) clickedPoint.getY())) {
            EuclideanPoint complexClicked = view.pixelToComplexEuclidean(clickedPoint);
            
            double angle = getAngle(anchorComplexPoint) - getAngle(complexClicked);
            EuclideanPoint vector = new EuclideanPoint(new PolarCoordinate(0.5d, angle), "rotation");
            anchorComplexPoint = complexClicked;
            rotate(vector);
        }
    }

    @Override
    public void rotateDynamicStop(GeometricView view) {
        anchorComplexPoint = null;
        view.removeMouseMotionListener(view);
    }

    public double tilingDistance(int nbFace, int nbPolygon) {
        double angleBase, halfAnglePolygon;
        angleBase = Math.PI * 2d / (double) nbFace;
        halfAnglePolygon = Math.PI / (double) nbPolygon;

        //deducing distance from the isosceles triangle with all angles known
        double coshDistance = (Math.cos(halfAnglePolygon) + Math.cos(halfAnglePolygon) * Math.cos(angleBase)) / (Math.sin(halfAnglePolygon) * Math.sin(angleBase));

        return mUtils.argcosh(coshDistance);
    }

    @Override
    public final void addPoints(GeometricView view) {
        model.clearModel();

        List<TransformablePoint> points = new LinkedList<>();
        List<PrintablePath> segments = new LinkedList<>();
        List<PrintableCircle> circles = new LinkedList<>();

        EuclideanPoint O;
        O = new EuclideanPoint(0, 0, "O");

        int nbPoints, depth, nbPolygonsAroundVertex;
        double dist;
        boolean createTree, tilingSummit, regularTiling, sideTiling, randomPath, circleTree;

        String[] choice = {"Regular Tiling", "Tree Tiling", "Vertex Tiling", "Side Tiling", "Random Path", "Circle tree"};
        String nom = (String) JOptionPane.showInputDialog(null, "Shape type to create.", "Shape type", JOptionPane.QUESTION_MESSAGE, null, choice, choice[0]); //TODO check if this function returns the same instance or a copy
        if (nom != null) {
            regularTiling = (choice[0].equals(nom));
            createTree = (choice[1].equals(nom));
            tilingSummit = (choice[2].equals(nom));
            sideTiling = (choice[3].equals(nom));
            randomPath = (choice[4].equals(nom));
            circleTree = (choice[5].equals(nom));


            if (regularTiling) {
                GeometricView.TilingParameters params = view.showRegularTilingDialogInput();
                nbPoints = params.nbSides;
                depth = params.depth;
                nbPolygonsAroundVertex = params.nbPolygons;
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter((tilingDistance(nbPoints, nbPolygonsAroundVertex)));
                createHyperbolicGoneBySide(nbPoints, dist, points, segments, depth);
            } else if (createTree) {
                nbPoints = view.showBuildingNbBranchesDialogInput();
                depth = view.showBuildingDepthDialogInput();
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput());
                createHyperbolicTree(nbPoints, dist, points, segments, O, depth);
            } else if (tilingSummit){
                nbPoints = view.showBuildingNbVertexDialogInput();
                depth = view.showBuildingDepthDialogInput();
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput());
                createHyperbolicGoneByPoint(nbPoints, dist, points, segments, O, depth);
            } else if(sideTiling) {
                nbPoints = view.showBuildingNbVertexDialogInput();
                depth = view.showBuildingDepthDialogInput();
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput());
                createHyperbolicGoneBySide(nbPoints, dist, points, segments, depth);
            } else if (randomPath) {
                depth = view.showBuildingDepthDialogInput();
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput());
                createRandomPath(depth, dist, points, segments);
            } else if (circleTree) {
                nbPoints = view.showBuildingNbBranchesDialogInput();
                depth = view.showBuildingDepthDialogInput();
                double radius = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput("radius of circle"));
                dist = PoincareDiskPoint.euclideanDistanceFromDiskCenter(view.showBuildingLengthDialogInput("space between circles"));
                createCircleTree(nbPoints, depth, radius, dist, points, circles);
            }
        }

        for (TransformablePoint complex : points) {
            model.addPoint(hyperbolicModelPointToHyperboloid(complex));
        }
        for (PrintablePath segment : segments) {
            model.addSegment(segment);
        }
        for(PrintableCircle circle : circles) {
            model.addCircle(circle);
        }
    }

    public void createCircleTree(int nbBranches, int nbCircles, double radius, double metricValue, List<TransformablePoint> points, List<PrintableCircle> circles) {
        TransformablePoint tmpOrigin, tmpRadius;
        int counter = 0;
        tmpOrigin = new PoincareDiskPoint("O");
        points.add(tmpOrigin);
        tmpRadius = new PoincareDiskPoint(0d, radius, "p1c0");
        points.add(tmpRadius);
        tmpRadius = new PoincareDiskPoint(0d, -radius, "p2c0");
        points.add(tmpRadius);
        circles.add(new PrintableCircle(tmpOrigin, tmpRadius, "circle0"));
        for(int branch = 1; branch <= nbBranches; branch++) {
            for(int i = 1; i <= nbCircles; i++) {
                translate(metricValue, points);
                counter++;
                tmpOrigin = new PoincareDiskPoint(String.valueOf(counter));
                points.add(tmpOrigin);
                tmpRadius = new PoincareDiskPoint(0d, radius, "p1c"+String.valueOf(counter));
                points.add(tmpRadius);
                tmpRadius = new PoincareDiskPoint(0d, -radius, "p2c"+String.valueOf(counter));
                points.add(tmpRadius);
                circles.add(new PrintableCircle(tmpOrigin, tmpRadius, "circle"+String.valueOf(counter)));
            }
            for(int i = 1; i <= nbCircles; i++) {
                translate(-metricValue, points);
            }
            rotate((2d*Math.PI) / (double) nbBranches, points);
        }
    }

    public void createHyperbolicGoneByPoint(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines, EuclideanPoint origin, int depth) {
        double angle = (2d * Math.PI) / nbPoint;
        TransformablePoint tmp, oldTmp = null, firstTmp = null;

        for (int acc = 0; acc < nbPoint; acc++) {
            if (acc != 0) {
                rotate(angle, points);
            }

            tmp = new PoincareDiskPoint(-metricValue, 0d, (points.size()) + "");
            points.add(tmp);

            if (oldTmp != null) {
                lines.add(new PrintablePath(oldTmp, tmp, "line" + lines.size()));
            } else {
                firstTmp = tmp;
            }

            oldTmp = tmp;
            if (acc == nbPoint - 1) {
                lines.add(new PrintablePath(oldTmp, firstTmp, "line" + lines.size()));
            }

            recursiveCreateHyperbolicSubGone(nbPoint, metricValue, points, lines, tmp, depth);
        }
    }

    public void recursiveCreateHyperbolicSubGone(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines, EuclideanPoint origin, int depth) {
        double angle = (2d * Math.PI) / nbPoint;
        TransformablePoint tmp, oldTmp = new PoincareDiskPoint(origin);

        if (depth > 0) {

            translate(metricValue, points);
            translate(metricValue, points);
            rotate(Math.PI, points);

            for (int acc = 1; acc < nbPoint; acc++) {
                rotate(angle, points);

                tmp = new PoincareDiskPoint(-metricValue, 0d, (points.size()) + "");
                points.add(tmp);

                lines.add(new PrintablePath(oldTmp, tmp, "line" + lines.size()));

                if (acc == nbPoint - 1) {
                    lines.add(new PrintablePath(tmp, origin, "line" + lines.size()));
                }

                oldTmp = tmp;
                recursiveCreateHyperbolicSubGone(nbPoint, metricValue, points, lines, tmp, depth - 1);
            }
            rotate(angle, points);

            rotate(Math.PI, points);
            translate(-metricValue, points);
            translate(-metricValue, points);
        }
    }

    public void createHyperbolicGoneBySide(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines, int depth) {
        double angle = (2d * Math.PI) / nbPoint;
        TransformablePoint tmp, oldTmp = null, firstTmp = null, midPoint;
        int tmpIdx, oldIdx = 0, firstIdx = 0;

        tmp = new PoincareDiskPoint((points.size()) + "");
        points.add(tmp);

        for (int acc = 0; acc < nbPoint; acc++) {
            if (acc != 0) {
                rotate(angle, points);
            }

            tmp = new PoincareDiskPoint(metricValue, 0d, (points.size()) + "");
            tmpIdx = points.size();
            points.add(tmp);

            if (oldTmp != null) {
                lines.add(new PrintablePath(oldTmp, tmp, "line" + lines.size()));
                midPoint = new PoincareDiskPoint(getMiddlePoint(points.get(oldIdx), points.get(tmpIdx)));
                rotate(-midPoint.getAngle(), points);
                translate(-midPoint.getRadius(), points);
                translate(-midPoint.getRadius(), points);
                recursiveCreateHyperbolicSubGoneBySide(nbPoint, metricValue, points, lines, oldIdx, tmpIdx, depth);
                translate(midPoint.getRadius(), points);
                translate(midPoint.getRadius(), points);
                rotate(midPoint.getAngle(), points);
            } else {
                firstTmp = tmp;
                firstIdx = tmpIdx;
            }

            oldTmp = tmp;
            oldIdx = tmpIdx;
            if (acc == nbPoint - 1) {
                lines.add(new PrintablePath(oldTmp, firstTmp, "line" + lines.size()));
                midPoint = new PoincareDiskPoint(getMiddlePoint(points.get(oldIdx), points.get(firstIdx)));
                rotate(-midPoint.getAngle(), points);
                translate(-midPoint.getRadius(), points);
                translate(-midPoint.getRadius(), points);
                recursiveCreateHyperbolicSubGoneBySide(nbPoint, metricValue, points, lines, oldIdx, firstIdx, depth);
                translate(midPoint.getRadius(), points);
                translate(midPoint.getRadius(), points);
                rotate(midPoint.getAngle(), points);
            }

        }

        rotate(-angle / 2d, points);
    }

    private void recursiveCreateHyperbolicSubGoneBySide(int nbPoints, double distanceRadius, List<TransformablePoint> points, List<PrintablePath> lines, int firstIdx, int lastIdx, int depth) {
        double angle = (2d * Math.PI) / (double) nbPoints;

        TransformablePoint tmp, midPoint;
        int tmpIdx, previousIdx = firstIdx, depthTmp;

        if (depth > 0) {
            tmp = new PoincareDiskPoint("proj:" + firstIdx + ";" + lastIdx);
            points.add(tmp);
            for (int acc = 1; acc < nbPoints - 1; acc++) {
                rotate(angle, points);
                rotate(angle / 2d, points);

                tmp = new PoincareDiskPoint(-distanceRadius, 0d, (points.size()) + "");
                tmpIdx = points.size();
                points.add(tmp);
                if (acc == 1) {
                    depthTmp = depth - 1;
                    lines.add(new PrintablePath(points.get(tmpIdx), points.get(firstIdx), "line" + lines.size()));
                    midPoint = new PoincareDiskPoint(getMiddlePoint(points.get(tmpIdx), points.get(firstIdx)));
                    rotate(-midPoint.getAngle(), points);
                    translate(-midPoint.getRadius(), points);
                    translate(-midPoint.getRadius(), points);
                    recursiveCreateHyperbolicSubGoneBySide(nbPoints, distanceRadius, points, lines, firstIdx, tmpIdx, depthTmp);
                    //(depth-1 > 1) ? 2 : depth-1
                    translate(midPoint.getRadius(), points);
                    translate(midPoint.getRadius(), points);
                    rotate(midPoint.getAngle(), points);
                }
                if (acc != 1) {
                    depthTmp = depth - 1;
                    lines.add(new PrintablePath(points.get(tmpIdx), points.get(previousIdx), "line" + lines.size()));
                    midPoint = new PoincareDiskPoint(getMiddlePoint(points.get(tmpIdx), points.get(previousIdx)));
                    rotate(-midPoint.getAngle(), points);
                    translate(-midPoint.getRadius(), points);
                    translate(-midPoint.getRadius(), points);
                    recursiveCreateHyperbolicSubGoneBySide(nbPoints, distanceRadius, points, lines, previousIdx, tmpIdx, depthTmp);
                    translate(midPoint.getRadius(), points);
                    translate(midPoint.getRadius(), points);
                    rotate(midPoint.getAngle(), points);
                }
                if (acc == nbPoints - 2) {
                    depthTmp = depth - 1;
                    lines.add(new PrintablePath(tmp, points.get(lastIdx), "line" + lines.size()));
                    midPoint = new PoincareDiskPoint(getMiddlePoint(tmp, points.get(lastIdx)));
                    rotate(-midPoint.getAngle(), points);
                    translate(-midPoint.getRadius(), points);
                    translate(-midPoint.getRadius(), points);
                    recursiveCreateHyperbolicSubGoneBySide(nbPoints, distanceRadius, points, lines, tmpIdx, lastIdx, depthTmp);
                    translate(midPoint.getRadius(), points);
                    translate(midPoint.getRadius(), points);
                    rotate(midPoint.getAngle(), points);
                }

                rotate(-angle / 2d, points);

                previousIdx = tmpIdx;
            }
            rotate(angle, points);
            rotate(angle, points);
        }
    }

    public void createRandomPath(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines) {
        double chosenAngle; //create a constant for obvious angles (pi/2, 3pi/4 et 2pi)
        points.add(new PoincareDiskPoint("0"));

        for(int i = 1; i < nbPoint; i++) {
            chosenAngle = Math.floor(Math.random() * 4d);
            rotate(chosenAngle*Math.PI/2d, points);
            translate(metricValue, points);

            points.add(new PoincareDiskPoint(String.valueOf(i)));
            PrintablePoint p1, p2;
            p1 = points.get(points.size()-2);
            p2 = points.get(points.size()-1);
            lines.add(new PrintablePath(p1, p2, "path-" + p1.getName() + p2.getName()));
        }
    }

    public void createHyperbolicTree(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines, EuclideanPoint origin, int depth) {
        points.add(new PoincareDiskPoint(origin));
        double angle = (2d * Math.PI) / nbPoint;
        TransformablePoint tmp;

        for (int acc = 0; acc < nbPoint; acc++) {
            if (acc != 0) {
                rotate(angle, points);
            }

            tmp = new PoincareDiskPoint(metricValue, 0d, (points.size()) + "");
            points.add(tmp);
            lines.add(new PrintablePath(origin, tmp, "line" + lines.size()));

            recursiveCreateHyperbolicSubTree(nbPoint, metricValue, points, lines, tmp, depth);
        }
    }

    public void recursiveCreateHyperbolicSubTree(int nbPoint, double metricValue, List<TransformablePoint> points, List<PrintablePath> lines, EuclideanPoint origin, int depth) {
        double angle = (2d * Math.PI) / nbPoint;
        if (depth > 0) {
            rotate(Math.PI, points);
            TransformablePoint tmp;
            translate(metricValue, points);
            for (int acc = 1; acc < nbPoint; acc++) {
                rotate(angle, points);
                tmp = new PoincareDiskPoint(metricValue, 0d, (points.size()) + "");
                points.add(tmp);
                lines.add(new PrintablePath(origin, tmp, "line" + lines.size()));

                recursiveCreateHyperbolicSubTree(nbPoint, metricValue, points, lines, tmp, depth - 1);
            }
            rotate(angle, points);
            translate(-metricValue, points);
            rotate(Math.PI, points);
        }
    }
}
