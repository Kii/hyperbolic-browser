/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometriccontrollers;

import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;

import java.util.List;

/**
 *
 * @author Rousselle RÃ©mi (remi.rousselle@isen-lille.fr)
 */
public class PoincareDiskController extends GeometricController {

    public PoincareDiskController(HyperboloidSpace space) {
        super(space);
    }

    @Override
    public double getAngle(EuclideanPoint complexPoint) {
        return complexPoint.getAngle();
    }

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p1, TransformablePoint p2) {
        TransformablePoint middlePoint = p1.getMiddlePoint(p2);
        middlePoint.setName("mid" + p1.getName() + ":" + p2.getName());
        return middlePoint;
    }

    @Override
    public void translatePoints(EuclideanPoint clickedComplex) {
        HyperboloidPoint hypPoint;
        PoincareDiskPoint complexP;

        for (PrintablePoint point : model.getPoints()) {
            if (point instanceof HyperboloidPoint) {
                hypPoint = (HyperboloidPoint) point;
                complexP = hypPoint.toPoincareDiskPoint();
                complexP.translate(clickedComplex);
                hypPoint.setCoords(complexP.toHyperboloid());
                model.firePointMovedEvent(point);
            }
        }
    }

    @Override
    public void rotate(EuclideanPoint clickedComplex) {
        HyperboloidPoint hTmp;
        PoincareDiskPoint complexP;

        double angleRotation = -1d * clickedComplex.getAngle();
        for (PrintablePoint p : model.getPoints()) {
            if (p instanceof HyperboloidPoint) {
                hTmp = (HyperboloidPoint) p;
                complexP = hTmp.toPoincareDiskPoint();
                complexP.rotate(angleRotation);
                hTmp.setCoords(complexP.toHyperboloid());
                model.firePointMovedEvent(p);
            }
        }
    }

    @Override
    public TransformablePoint hyperboloidToHyperbolicModel(HyperboloidPoint p) {
        return p.toPoincareDiskPoint();
    }

    @Override
    public HyperboloidPoint hyperbolicModelPointToHyperboloid(TransformablePoint hyperbolicPoint) {
        return hyperbolicPoint.toHyperboloid();
    }

    @Override
    public double distanceSegmentHyperbolic(TransformablePoint complex1, TransformablePoint complex2) {
        return complex1.distance(complex2);
    }

    public PrintableObject getLine(PrintablePath line) {
        EuclideanPoint p1 = (EuclideanPoint) line.getP1();
        EuclideanPoint p2 = (EuclideanPoint) line.getP2();

        EuclideanCircle abstractCircle = new EuclideanCircle(1d, "poincare-disk");
        List<EuclideanPoint> pointOnCircles;
        EuclideanLine p1line, orthoP1;
        if (EuclideanPoint.ORIGIN.equals(p1)) {
            p1line = new EuclideanLine(EuclideanPoint.ORIGIN, p1);
            orthoP1 = p1line.orthogonal(p1);
        } else {
            p1line = new EuclideanLine(EuclideanPoint.ORIGIN, p2);
            orthoP1 = p1line.orthogonal(p2);
        }

        pointOnCircles = orthoP1.intersect(abstractCircle);
        if (pointOnCircles.size() == 2) {
            EuclideanLine orthoTangent1, orthoTangent2, tangent1, tangent2;
            orthoTangent1 = new EuclideanLine(pointOnCircles.get(0), EuclideanPoint.ORIGIN);
            orthoTangent2 = new EuclideanLine(pointOnCircles.get(1), EuclideanPoint.ORIGIN);
            tangent1 = orthoTangent1.orthogonal(pointOnCircles.get(0));
            tangent2 = orthoTangent2.orthogonal(pointOnCircles.get(1));
            EuclideanPoint thirdPoint = tangent1.intersect(tangent2);
            return EuclideanCircle.build(p1, p2, thirdPoint, p1.getName() + "-" + p2.getName() + "-line");
        } else {
            return null;
        }
    }
}
