package hyperbolicbrowser.geometriccontrollers;

import hyperbolicbrowser.geometricmodels.geometricobjects.points.HyperboloidPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;

public interface MathematicTranslation {
	HyperboloidPoint hyperbolicModelPointToHyperboloid(TransformablePoint p);
	TransformablePoint hyperboloidToHyperbolicModel(HyperboloidPoint p);
	double distanceSegmentHyperbolic(TransformablePoint p1, TransformablePoint p2);
}
