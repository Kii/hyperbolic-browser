/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometriccontrollers;

import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class BeltramiDiskController extends GeometricController {

    public BeltramiDiskController(HyperboloidSpace space) {
        super(space);
    }

    @Override
    public void translatePoints(EuclideanPoint clickedComplex) {
        HyperboloidPoint hypPoint;

        PoincareDiskPoint complexP;
        clickedComplex.setPolar(clickedComplex.getRadius()/2d, clickedComplex.getAngle());
        for (PrintablePoint point : model.getPoints()) {
            if (point instanceof HyperboloidPoint) {
                hypPoint = (HyperboloidPoint) point;
                complexP = hypPoint.toPoincareDiskPoint();
                complexP.translate(clickedComplex);
                hypPoint.setCoords(complexP.toHyperboloid());
                model.firePointMovedEvent(point);
            }
        }
    }

    @Override
    public void rotate(EuclideanPoint clickedComplex) {
        HyperboloidPoint hTmp;

        BeltramiDiskPoint complexP;
        double angleRotation = -1d * clickedComplex.getAngle();
        for (PrintablePoint p : model.getPoints()) {
            if (p instanceof HyperboloidPoint) {
                hTmp = (HyperboloidPoint) p;
                complexP = hTmp.toBeltramiDiskPoint();
                complexP.rotate(angleRotation);
                hTmp.setCoords(complexP.toHyperboloid());
                model.firePointMovedEvent(p);
            }
        }
    }

    @Override
    public TransformablePoint hyperboloidToHyperbolicModel(HyperboloidPoint p) {
        return p.toBeltramiDiskPoint();
    }

    @Override
    public HyperboloidPoint hyperbolicModelPointToHyperboloid(TransformablePoint beltramiDiskPoint) {
        return beltramiDiskPoint.toHyperboloid();
    }

    @Override
    public double distanceSegmentHyperbolic(TransformablePoint p, TransformablePoint q) {
        return p.distance(q);
    }
    
    @Override
	public double getAngle(EuclideanPoint complexPoint) {
		return complexPoint.getAngle();
	}

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p1, TransformablePoint p2) {
        return p1.getMiddlePoint(p2);
    }
}
