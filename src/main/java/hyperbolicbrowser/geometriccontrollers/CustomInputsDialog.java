/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometriccontrollers;

import javax.swing.JOptionPane;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */

/* 1.4 example used by DialogDemo.java. */
public class CustomInputsDialog {

    public static String[] showInputsDialog(String[] input) {

        String[] results = new String[input.length];

        int i = 0;
        for(String s : input){
            //read in value from user as a string
            results[i] = JOptionPane.showInputDialog(s);
            i++;
        }

//convert numbers from type String to type int
        //number1 = Integer.parseInt(firstNumber);

        return results;
    }
    
    public static Double[] showDoubleInputsDialog(String[] input) {
        
        String[] res = CustomInputsDialog.showInputsDialog(input);
        Double[] doubles = new Double[res.length];
        
        int i = 0;
        for(String s : res){
            try{
                doubles[i] = Double.parseDouble(s);
            } catch (NumberFormatException e){
                doubles[i] = Integer.valueOf(s)+.0;
            } catch (Exception e){
                return null;
            }
            i++;
        }
        
        return doubles;
    }
}