/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometriccontrollers;

import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricviews.GeometricView;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class PoincareHalfPlaneController extends GeometricController {

    public PoincareHalfPlaneController(HyperboloidSpace space) {
        super(space);
    }

    @Override
    public void rotate(EuclideanPoint clickedComplex) {
        HyperboloidPoint hTmp;
        PoincareHalfPlanePoint complexP;
        double angleRotation = -1d * clickedComplex.getAngle() / 2d;

        for (PrintablePoint p : model.getPoints()) {
            if (p instanceof HyperboloidPoint) {
                hTmp = (HyperboloidPoint) p;
                complexP = hTmp.toPoincareHalfPlanePoint();
                complexP.rotate(angleRotation);
                hTmp.setCoords(hyperbolicModelPointToHyperboloid(complexP));
                model.firePointMovedEvent(p);
            }
        }
    }

    @Override
    public void translateDynamicMove(GeometricView view, EuclideanPoint movedPoint) {
        if (view.isPixelInsideView((int) movedPoint.getX(), (int) movedPoint.getY())) {
            EuclideanPoint complexMoved = view.pixelToComplexEuclidean(movedPoint);
            EuclideanPoint vector = EuclideanPoint.sum(anchorComplexPoint, EuclideanPoint.multiply(complexMoved, -1d));
            anchorComplexPoint = complexMoved;
            vector.sum(new EuclideanPoint(0d, 1d));
            translatePoints(vector);
        }
    }
    
    @Override
    public void translatePoints(EuclideanPoint clickedComplex) {
        HyperboloidPoint hypPoint;

        PoincareHalfPlanePoint complexP;
        for (PrintablePoint point : model.getPoints()) {
            if (point instanceof HyperboloidPoint) {
                hypPoint = (HyperboloidPoint) point;
                complexP = hypPoint.toPoincareHalfPlanePoint();
                complexP.translate(clickedComplex);
                hypPoint.setCoords(hyperbolicModelPointToHyperboloid(complexP));
                model.firePointMovedEvent(point);
            }
        }
    }

    @Override
    public TransformablePoint hyperboloidToHyperbolicModel(HyperboloidPoint p) {
        return p.toPoincareHalfPlanePoint();
    }

    @Override
    public HyperboloidPoint hyperbolicModelPointToHyperboloid(TransformablePoint p) {
        return p.toHyperboloid();
    }

    @Override
    public double distanceSegmentHyperbolic(TransformablePoint p1, TransformablePoint p2) {
        return p1.distance(p2);
    }
    
    @Override
	public double getAngle(EuclideanPoint complexPoint) {
    	EuclideanPoint anglePoint = EuclideanPoint.sum(complexPoint, EuclideanPoint.multiply(pointI, -1));
		return anglePoint.getAngle();
	}

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p1, TransformablePoint p2) {
        return p1.getMiddlePoint(p2);
    }
}
