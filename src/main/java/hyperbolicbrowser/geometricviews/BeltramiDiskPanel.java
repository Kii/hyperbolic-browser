/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import hyperbolicbrowser.geometriccontrollers.MathematicTranslation;
import hyperbolicbrowser.geometricmodels.geometricobjects.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.BeltramiDiskPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;
import hyperbolicbrowser.geometricmodels.markings.GeometricMarking;
import math.geom2d.conic.Ellipse2D;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class BeltramiDiskPanel extends DiskPanel {

    /**
	 * Generated
	 */
	private static final long serialVersionUID = 3716741928622979020L;
    public static final double metricValue = BeltramiDiskPoint.euclideanDistanceFromDiskCenter(1d);

	public BeltramiDiskPanel(int width, int height, MathematicTranslation mt) {
        super(metricValue, width, height, mt);

        name = "Beltrami-Klein Disk";

        EuclideanPoint diskCenter = disk.getCenter();
        int diskDiameter = (int) (disk.getRadius()*2);
        
        ArrayList<PrintablePoint> axesPoints = new ArrayList<>();
        axesPoints.add(new EuclideanPoint(diskCenter.getX() + metricValue * 0.5d * diskDiameter, diskCenter.getY(), "i"));
        axesPoints.add(new EuclideanPoint(diskCenter.getX(), diskCenter.getY() - metricValue * 0.5d * diskDiameter, "j"));

        marking = new GeometricMarking(new EuclideanPoint(diskCenter.getX(), diskCenter.getY(), "origin"), axesPoints);
    }

    /**
     * @param p euclidean coordinates point
     * @return pixel coordinates on the screen
     */
    @Override
    public EuclideanPoint complexEuclideanToPixel(TransformablePoint p) {
        EuclideanPoint diskCenter = disk.getCenter();
        return new EuclideanPoint(margin + getDiskRadius() * (1d + p.getX()), margin + getDiskRadius() * (1d - p.getY()), p.getName());
    }
    
    @Override
    public TransformablePoint pixelToComplexEuclidean(EuclideanPoint p) {
        EuclideanPoint diskCenter = disk.getCenter();
        return new BeltramiDiskPoint((p.getX() - diskCenter.getX()) / getDiskRadius(), (diskCenter.getY() - p.getY()) / getDiskRadius(), p.getName());
    }

    @Override
    protected Shape getHyperbolicLinePixelRepresentation(PrintablePath l, boolean isBounded) {
    	if (l.getP1() instanceof EuclideanPoint && l.getP2() instanceof EuclideanPoint) {
            EuclideanPoint p1Pixel = (EuclideanPoint) l.getP1();
            EuclideanPoint p2Pixel = (EuclideanPoint) l.getP2();

            TransformablePoint p1 = pixelToComplexEuclidean(p1Pixel);
            TransformablePoint p2 = pixelToComplexEuclidean(p2Pixel);
            
            if (isBounded) {
                return getPixelSegment(p1, p2);
            } else {
                EuclideanLine p1line = new EuclideanLine(p1, p2);
                List<EuclideanPoint> interCircles = p1line.intersect(EuclideanCircle.UNIT_CIRCLE);
                if (interCircles.size() == 2) {
                	return getPixelSegment(new BeltramiDiskPoint(interCircles.get(0)), new BeltramiDiskPoint(interCircles.get(1)));
                }
            }
        }
    	return null;
    }

    @Override
    public void drawHyperbolicCircle(Graphics g, PrintableCircle c) {
        //euclCenter : (hypCenter) / (cosh(dist)^2 * (1 - A^2) + A^2)
        //demi-axe : (cosh(r) * sinh(r) * (1 - A^2)) / (cosh(dist)^2 * (1 - A^2) + A^2) //direction OA
        //demi-axe : (sinh(r) * sqrt( 1 - A^2)) / sqrt((cosh(dist)^2 * (1 - A^2) + A^2)) //direction orthogonale

        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
        TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

        if(complexCenter.distance(new EuclideanPoint()) > Math.pow(10d, -5d)) {

            double hypDistance = complexCenter.distance(complexPointOnCircle);

            double square = Math.pow(complexCenter.getX(), 2d) + Math.pow(complexCenter.getY(), 2d);
            double inverseSquare = (1d - square);
            double denum = (Math.pow(Math.cosh(hypDistance), 2d) * inverseSquare) + square;
            double halfAxisEuclideanDistance = (Math.cosh(hypDistance) * Math.sinh(hypDistance) * inverseSquare) / denum;
            double orthoAxisEuclideanDistance = (Math.sinh(hypDistance) * Math.sqrt(inverseSquare)) / Math.sqrt(denum);
            TransformablePoint euclideanCenter = new BeltramiDiskPoint(EuclideanPoint.multiply(complexCenter, 1d / denum));

            try {
                EuclideanLine lineFromCenter = new EuclideanLine(new EuclideanPoint(), euclideanCenter);
                EuclideanLine orthoLine = lineFromCenter.orthogonal(euclideanCenter);

                TransformablePoint halfAxisP1 = new BeltramiDiskPoint(lineFromCenter.getPointAtDistance(euclideanCenter, halfAxisEuclideanDistance / 2d));
                TransformablePoint halfAxisP2 = new BeltramiDiskPoint(lineFromCenter.getPointAtDistance(euclideanCenter, -halfAxisEuclideanDistance / 2d));
                TransformablePoint orthoAxisP1 = new BeltramiDiskPoint(orthoLine.getPointAtDistance(euclideanCenter, orthoAxisEuclideanDistance / 2d));
                TransformablePoint orthoAxisP2 = new BeltramiDiskPoint(orthoLine.getPointAtDistance(euclideanCenter, -orthoAxisEuclideanDistance / 2d));

                double halfAxisPixelDistance = complexEuclideanToPixel(halfAxisP1).distance(complexEuclideanToPixel(halfAxisP2));
                double orthoAxisPixelDistance = complexEuclideanToPixel(orthoAxisP1).distance(complexEuclideanToPixel(orthoAxisP2));
                EuclideanPoint euclideanPixelCenter = complexEuclideanToPixel(euclideanCenter);

                Ellipse2D ellipse = new Ellipse2D(euclideanPixelCenter.getX(), euclideanPixelCenter.getY(), halfAxisPixelDistance, orthoAxisPixelDistance, -complexCenter.getAngle());
                Graphics2D g2 = (Graphics2D) g;
                ellipse.draw(g2);
            } catch (RuntimeException e) {
                EuclideanCircle tmpCircle = new EuclideanCircle(pixelCenter, pixelPointOnCircle, c.getName());
                Rectangle boundingBox = tmpCircle.getBoundingBox();
                g.drawArc(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0, 360);
            }
        } else {
            EuclideanCircle tmpCircle = new EuclideanCircle(pixelCenter, pixelPointOnCircle, c.getName());
            Rectangle boundingBox = tmpCircle.getBoundingBox();
            g.drawArc(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0, 360);
        }
    }

    @Override
    public double distancePixelFromPointToCircle(EuclideanPoint clickedPoint, PrintableCircle c) {
        //euclCenter : (hypCenter) / (cosh(dist)^2 * (1 - A^2) + A^2)
        //demi-axe : (cosh(r) * sinh(r) * (1 - A^2)) / (cosh(dist)^2 * (1 - A^2) + A^2) //direction OA
        //demi-axe : (sinh(r) * sqrt( 1 - A^2)) / sqrt((cosh(dist)^2 * (1 - A^2) + A^2)) //direction orthogonale

        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
        TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

        if(!complexCenter.equals(new EuclideanPoint())) {

            double hypDistance = complexCenter.distance(complexPointOnCircle);

            double square = Math.pow(complexCenter.getX(), 2d) + Math.pow(complexCenter.getY(), 2d);
            double inverseSquare = (1d - square);
            double denum = (Math.pow(Math.cosh(hypDistance), 2d) * inverseSquare) + square;
            double halfAxisEuclideanDistance = (Math.cosh(hypDistance) * Math.sinh(hypDistance) * inverseSquare) / denum;
            double orthoAxisEuclideanDistance = (Math.sinh(hypDistance) * Math.sqrt(inverseSquare)) / Math.sqrt(denum);
            TransformablePoint euclideanCenter = new BeltramiDiskPoint(EuclideanPoint.multiply(complexCenter, 1d / denum));

            EuclideanLine lineFromCenter = new EuclideanLine(new EuclideanPoint(), euclideanCenter);
            EuclideanLine orthoLine = lineFromCenter.orthogonal(euclideanCenter);

            TransformablePoint halfAxisP1 = new BeltramiDiskPoint(lineFromCenter.getPointAtDistance(euclideanCenter, halfAxisEuclideanDistance / 2d));
            TransformablePoint halfAxisP2 = new BeltramiDiskPoint(lineFromCenter.getPointAtDistance(euclideanCenter, -halfAxisEuclideanDistance / 2d));
            TransformablePoint orthoAxisP1 = new BeltramiDiskPoint(orthoLine.getPointAtDistance(euclideanCenter, orthoAxisEuclideanDistance / 2d));
            TransformablePoint orthoAxisP2 = new BeltramiDiskPoint(orthoLine.getPointAtDistance(euclideanCenter, -orthoAxisEuclideanDistance / 2d));

            double halfAxisPixelDistance = complexEuclideanToPixel(halfAxisP1).distance(complexEuclideanToPixel(halfAxisP2));
            double orthoAxisPixelDistance = complexEuclideanToPixel(orthoAxisP1).distance(complexEuclideanToPixel(orthoAxisP2));
            EuclideanPoint euclideanPixelCenter = complexEuclideanToPixel(euclideanCenter);

            Ellipse2D ellipse = new Ellipse2D(euclideanPixelCenter.getX(), euclideanPixelCenter.getY(), halfAxisPixelDistance, orthoAxisPixelDistance, -complexCenter.getAngle());
            return ellipse.distance(clickedPoint.getX(), clickedPoint.getY());
        } else {
            EuclideanCircle tmpCircle = new EuclideanCircle(pixelCenter, pixelPointOnCircle, c.getName());
            return Math.abs(tmpCircle.getCenter().distance(clickedPoint) - tmpCircle.getRadius());
        }
    }

    @Override
    public TransformablePoint getEuclideanCircleCenter(TransformablePoint complexCenter, TransformablePoint complexPointOnCircle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
