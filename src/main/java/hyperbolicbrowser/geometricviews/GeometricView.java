/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import hyperbolicbrowser.geometriccontrollers.CustomInputsDialog;
import hyperbolicbrowser.geometriccontrollers.MathematicTranslation;
import hyperbolicbrowser.geometricmodels.EuclideanSpace;
import hyperbolicbrowser.geometricmodels.GeometricSpace;
import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.HyperboloidPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;
import hyperbolicbrowser.geometricmodels.markings.GeometricMarking;
import hyperbolicbrowser.listeners.ActionButtonListener;
import hyperbolicbrowser.listeners.ActionUserListener;
import hyperbolicbrowser.listeners.GeometricSpaceListener;
import hyperbolicbrowser.toolbar.ActionsEnum;

/**
 *
 * @author Rousselle RÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â©mi (remi.rousselle@isen-lille.fr)
 */
public abstract class GeometricView extends JPanel implements GeometricSpaceListener, ActionButtonListener, MouseListener, MouseMotionListener {

    /**
     * Generated
     */
    private static final long serialVersionUID = 569879021226088713L;

    protected String name;
    protected static int margin = 5;
    protected int width;
    protected int height;
    protected int pointSize = 7;
    public static final int crossSize = 10;
    protected int flyOverPointSize = 10;
    protected Color selectedObjectColor = Color.RED;
    protected List<ActionUserListener> actionUserListeners;
    protected GeometricSpace internModel;
    protected GeometricMarking marking;
    protected EuclideanPoint lastSelectedPoint;
    protected boolean showMarking;
    protected boolean showPointName;
    protected DecimalFormat df = new DecimalFormat("####0.00");
    private String idListener;
    private MathematicTranslation mt;

    public GeometricView(int width, int height, MathematicTranslation mt) {
        super(new BorderLayout());
        this.width = width;
        this.height = height;
        this.mt = mt;

        internModel = new EuclideanSpace();
        showMarking = true;
        showPointName = false;
        setSize(width, height);

        actionUserListeners = new LinkedList<>();
        int nbViews = 0;
        idListener = "GeometricView" + String.valueOf(nbViews);
        setFocusTraversalKeysEnabled(false);

        //deactivate keyboard shortcuts for scroll panels
        UIManager.getDefaults().put("ScrollPane.ancestorInputMap", new UIDefaults.LazyInputMap(new Object[]{}));
    }

    // public abstract HyperboloidPoint pixelToHyperboloid(EuclideanPoint p);
    // public abstract EuclideanPoint hyperboloidToPixel(HyperboloidPoint p);
    public abstract EuclideanPoint complexEuclideanToPixel(TransformablePoint p);

    public abstract TransformablePoint pixelToComplexEuclidean(EuclideanPoint p);

    protected abstract void drawInitialView(Graphics g);

    public abstract boolean isPixelInsideView(int x, int y);

    protected abstract Shape getHyperbolicLinePixelRepresentation(PrintablePath l, boolean isBounded);

    public abstract TransformablePoint getEuclideanCircleCenter(TransformablePoint complexCenter, TransformablePoint complexPointOnCircle);

    public abstract void drawHyperbolicCircle(Graphics g, PrintableCircle c);

    public boolean isPixelInsideView(EuclideanPoint pixel) {
        return isPixelInsideView((int) pixel.getX(), (int) pixel.getY());
    }

    public final HyperboloidPoint pixelToHyperboloid(EuclideanPoint pixelPoint) {
        TransformablePoint complexPoint = pixelToComplexEuclidean(pixelPoint);
        return mt.hyperbolicModelPointToHyperboloid(complexPoint);
    }

    public final EuclideanPoint hyperboloidToPixel(HyperboloidPoint p) {
        TransformablePoint complexPoint = mt.hyperboloidToHyperbolicModel(p);
        complexPoint.setColor(p.getColor());
        return complexEuclideanToPixel(complexPoint);
    }

    public void addActionUserListener(ActionUserListener listener) {
        actionUserListeners.add(listener);
    }

    public GeometricSpace getInternModel() {
        return internModel;
    }

    public EuclideanPoint getLastSelectedPoint() {
        return lastSelectedPoint;
    }

    public void setLastSelectedPoint(EuclideanPoint p) {
        lastSelectedPoint = p;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawInitialView(g);
        drawSpace(g);
    }

    protected void drawSpace(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        for (PrintablePoint point : internModel.getPoints()) {
            if(point.isSelected()) {
                g.setColor(selectedObjectColor);
            } else {
                g.setColor(point.getColor());
            }
            drawPoint(g, point);
        }

        for (PrintablePath segment : internModel.getSegments()) {
            if (segment.isSelected()) {
                g.setColor(selectedObjectColor);
            } else {
                g.setColor(segment.getColor());
            }
            if(segment.isFlyOver()) {
                g2.setStroke(new BasicStroke(3));
            } else {
                g2.setStroke(new BasicStroke(1));
            }
            drawHyperbolicLine(g2, segment, true);
        }

        for (PrintablePath line : internModel.getLines()) {
            if (line.isSelected()) {
                g.setColor(selectedObjectColor);
            } else {
                g.setColor(line.getColor());
            }
            if(line.isFlyOver()) {
                g2.setStroke(new BasicStroke(3));
            } else {
                g2.setStroke(new BasicStroke(1));
            }
            drawHyperbolicLine(g2, line, false);
        }

        for (PrintableCircle circle : internModel.getCircles()) {
            if (circle.isSelected()) {
                g.setColor(selectedObjectColor);
            } else {
                g.setColor(circle.getColor());
            }
            if(circle.isFlyOver()) {
                g2.setStroke(new BasicStroke(3));
            } else {
                g2.setStroke(new BasicStroke(1));
            }
            drawHyperbolicCircle(g, circle);
        }
    }

    //Draws a point on a disk from its pixel euclidean coordinates
    protected void drawPixelPoint(Graphics g, double x, double y, String s, boolean selected, boolean flyOver) {
        if (isPixelInsideView((int) x, (int) y)) {
            if (!flyOver) {
                g.fillOval((int) x - pointSize / 2, (int) y - pointSize / 2, pointSize, pointSize);
            } else {
                g.fillOval((int) x - flyOverPointSize / 2, (int) y - flyOverPointSize / 2, flyOverPointSize,
                        flyOverPointSize);
            }
            if (selected) {
                g.drawString(s, (int) x + 5, (int) y + 5);
            }
        }
    }

    protected void drawPoint(Graphics g, PrintablePoint p) {
        if (p instanceof EuclideanPoint && p.isVisible()) {
            EuclideanPoint ep = (EuclideanPoint) p;
            drawPixelPoint(g, ep.getX(), ep.getY(), p.getName(), p.isSelected(), p.isFlyOver());
        }
    }

    protected final void drawHyperbolicLine(Graphics2D g2, PrintablePath l, boolean bounded) {
        if (l.getP1() instanceof EuclideanPoint && l.getP2() instanceof EuclideanPoint) {
            Shape pixelPath = getHyperbolicLinePixelRepresentation(l, bounded);
            if (pixelPath != null) {
                g2.draw(pixelPath);
                if (bounded) {
                    if (l.isSelected()) {
                        EuclideanPoint p1Pixel = (EuclideanPoint) l.getP1();
                        EuclideanPoint p2Pixel = (EuclideanPoint) l.getP2();

                        TransformablePoint p1 = pixelToComplexEuclidean(p1Pixel);
                        TransformablePoint p2 = pixelToComplexEuclidean(p2Pixel);

                        EuclideanPoint stringPos = EuclideanPoint.getMiddlePoint(p1Pixel, p2Pixel);
                        drawSegmentDistance(g2, p1, p2, stringPos);
                    }
                }
            }
        }
    }

    public abstract double distancePixelFromPointToCircle(EuclideanPoint clickedPoint, PrintableCircle c);

    public final double distancePixelFromPointToHyperbolicLine(EuclideanPoint clickedPoint, PrintablePath l, boolean bounded) {
        Shape object = getHyperbolicLinePixelRepresentation(l, bounded);

        EuclideanPoint p1Pixel, p2Pixel;
        p1Pixel = (EuclideanPoint) l.getP1();
        p2Pixel = (EuclideanPoint) l.getP2();
        if (object instanceof Line2D.Double) {
            Line2D.Double line = (Line2D.Double) object;
            line.ptLineDist(clickedPoint.getX(), clickedPoint.getY());
        } else if (object instanceof Arc2D.Double) {
            Arc2D.Double circle = (Arc2D.Double) object;
            EuclideanPoint center = new EuclideanPoint(circle.getCenterX(), circle.getCenterY());
            double angle1 = EuclideanPoint.subtract(p1Pixel, center).getAngle();
            double angle2 = EuclideanPoint.subtract(p2Pixel, center).getAngle();
            return EuclideanCircle.distanceFromPointToEuclideanCircle(clickedPoint, new EuclideanCircle(center, p1Pixel, ""), angle1, angle2, bounded);
        }
        return Double.MAX_VALUE;
    }

    protected final Shape getPixelSegment(TransformablePoint p1, TransformablePoint p2) {
        if (p1 != null && p2 != null) {
            EuclideanPoint p1Pixel = complexEuclideanToPixel(p1);
            EuclideanPoint p2Pixel = complexEuclideanToPixel(p2);

            return new Line2D.Double(p1Pixel.getX(), p1Pixel.getY(), p2Pixel.getX(), p2Pixel.getY());
        } else {
            return null;
        }
    }

    protected abstract Shape getPixelCirle(TransformablePoint center, TransformablePoint pointOnCircle);

    protected abstract Shape getPixelBoundedCirle(TransformablePoint center, TransformablePoint p1, TransformablePoint p2, boolean isSelected);

    protected void drawSegmentDistance(Graphics g, TransformablePoint p1, TransformablePoint p2,
                                       EuclideanPoint pixelStringPos) {
        double d = mt.distanceSegmentHyperbolic(p1, p2);
        g.drawString("d=" + df.format(d), (int) pixelStringPos.getX(), (int) pixelStringPos.getY());
    }

    @Override
    public void spaceCleared() {
        internModel.clearModel();
        repaint();
    }

    @Override
    public void objectChanged(String oldName, PrintableObject p) {
        PrintableObject oldObj = internModel.getObjectByName(oldName);
        oldObj.setName(p.getName());
        oldObj.setColor(p.getColor());
    }
    
    @Override
    public void spaceUpdated(HyperboloidSpace model) {
        spaceCleared();

        if (model == null) {
            throw new RuntimeException("The saved model is null");
        } else if (model.getPoints() == null || model.getPoints().isEmpty()) {
            throw new RuntimeException("The saved model has no points");
        }
        
        for (PrintablePoint p : model.getPoints()) {
            internModel.addPoint(hyperboloidToPixel((HyperboloidPoint) p));
        }

        for (PrintablePath l : model.getLines()) {
            PrintablePoint p1 = internModel.getPointByName(l.getP1().getName());
            PrintablePoint p2 = internModel.getPointByName(l.getP2().getName());
            PrintablePath line = new PrintablePath(p1, p2, l.getName());
            line.setColor(l.getColor());
            internModel.addLine(line);
        }

        for (PrintablePath l : model.getSegments()) {
            PrintablePoint p1 = internModel.getPointByName(l.getP1().getName());
            PrintablePoint p2 = internModel.getPointByName(l.getP2().getName());
            PrintablePath segment = new PrintablePath(p1, p2, l.getName());
            segment.setColor(l.getColor());
            internModel.addSegment(segment);
        }

        for (PrintableCircle circle : model.getCircles()) {
            PrintablePoint center = internModel.getPointByName(circle.getCenter().getName());
            PrintablePoint pointOnCircle = internModel.getPointByName(circle.getPointOnCircle().getName());
            PrintableCircle hypCircle = new PrintableCircle(center, pointOnCircle, circle.getName());
            hypCircle.setColor(circle.getColor());
            internModel.addCircle(hypCircle);
        }
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (isPixelInsideView(e.getX(), e.getY())) {
            //requestFocusInWindow();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (ActionUserListener listener : actionUserListeners) {
            listener.mousePressed(this, e.getX(), e.getY());
        }
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for (ActionUserListener listener : actionUserListeners) {
            listener.mouseReleased(this, e);
        }
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        for (ActionUserListener listener : actionUserListeners) {
            listener.mouseMoved(this, me.getX(), me.getY());
        }
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        for (ActionUserListener listener : actionUserListeners) {
            listener.mouseMoved(this, me.getX(), me.getY());
        }
    }

    @Override
    public void currentActionChanged(ActionsEnum ae) {
        for (ActionUserListener listener : actionUserListeners) {
            listener.currentActionChanged(this, ae);
        }
    }

    @Override
    public void pointAdded(PrintablePoint p) {
        if (p instanceof HyperboloidPoint) {
            EuclideanPoint pixelPoint = hyperboloidToPixel((HyperboloidPoint) p);
            pixelPoint.setColor(p.getColor());
            internModel.addPoint(pixelPoint);
            repaint();
        } else {
            throw new RuntimeException("Point is not on hyperboloid");
        }
    }

    @Override
    public void pointMoved(PrintablePoint p) {
        if (p instanceof HyperboloidPoint) {
            EuclideanPoint internP = (EuclideanPoint) internModel.getPointByName(p.getName());
            if (internP != null) {
                EuclideanPoint pixelP = hyperboloidToPixel((HyperboloidPoint) p);
                internP.setCartesian(pixelP.getX(), pixelP.getY());
                internP.setSelected(p.isSelected());
                repaint();
            }
        }
    }

    @Override
    public void pointRemoved(PrintablePoint p) {
        if (p instanceof HyperboloidPoint) {
            EuclideanPoint internP = (EuclideanPoint) internModel.getPointByName(p.getName());
            if (internP != null) {
                internModel.removePoint(internP);
                repaint();
            }
        }
    }

    @Override
    public void pointSelection(PrintablePoint p) {
        if (p instanceof HyperboloidPoint) {
            EuclideanPoint internP = (EuclideanPoint) internModel.getPointByName(p.getName());
            if (internP != null) {
                internP.setSelected(p.isSelected());
                repaint();
            }
        }
    }

    @Override
    public void pointFlyOver(PrintablePoint p) {
        internModel.getPointByName(p.getName()).setFlyOver(p.isFlyOver());
        repaint();
    }

    @Override
    public void lineAdded(PrintablePath l) {
        PrintablePoint p1 = internModel.getPointByName(l.getP1().getName());
        PrintablePoint p2 = internModel.getPointByName(l.getP2().getName());

        internModel.addLine(new PrintablePath(p1, p2, l.getName()));
    }

    @Override
    public void lineRemoved(PrintablePath l) {
        PrintablePath internL = internModel.getLineByName(l.getName());
        if (internL != null) {
            internModel.removeLine(internL);
            repaint();
        }
    }

    @Override
    public void lineSelection(PrintablePath l) {
        PrintablePath internL = internModel.getLineByName(l.getName());
        if (internL != null) {
            internL.setSelected(l.isSelected());
            repaint();
        }
    }

    @Override
    public void lineFlyOver(PrintablePath l) {
        PrintablePath line = internModel.getLineByName(l.getName());
        if(line != null) {
            line.setFlyOver(l.isFlyOver());
            repaint();
        }
    }

    @Override
    public void segmentAdded(PrintablePath s) {
        PrintablePoint p1 = internModel.getPointByName(s.getP1().getName());
        PrintablePoint p2 = internModel.getPointByName(s.getP2().getName());

        internModel.addSegment(new PrintablePath(p1, p2, s.getName()));
    }

    @Override
    public void segmentFlyOver(PrintablePath s) {
        internModel.getSegmentByName(s.getName()).setFlyOver(s.isFlyOver());
        repaint();
    }

    @Override
    public void segmentRemoved(PrintablePath s) {
        PrintablePath internS = internModel.getSegmentByName(s.getName());
        if (internS != null) {
            internModel.removeSegment(internS);
            repaint();
        }
    }

    @Override
    public void segmentSelection(PrintablePath s) {
        PrintablePath internS = internModel.getSegmentByName(s.getName());
        if (internS != null) {
            internS.setSelected(s.isSelected());
            repaint();
        }
    }

    @Override
    public void circleAdded(PrintableCircle c) {
        PrintablePoint p1 = internModel.getPointByName(c.getCenter().getName());
        PrintablePoint p2 = internModel.getPointByName(c.getPointOnCircle().getName());

        internModel.addCircle(new PrintableCircle(p1, p2, c.getName()));
    }

    @Override
    public void circleFlyOver(PrintableCircle c) {
        internModel.getCircleByName(c.getName()).setFlyOver(c.isFlyOver());
        repaint();
    }

    @Override
    public void circleRemoved(PrintableCircle c) {
        PrintableCircle circle = internModel.getCircleByName(c.getName());
        if (circle != null) {
            internModel.removeCircle(circle);
            repaint();
        }
    }

    @Override
    public void circleSelection(PrintableCircle c) {
        PrintableCircle internC = internModel.getCircleByName(c.getName());
        if (internC != null) {
            internC.setSelected(c.isSelected());
            repaint();
        }
    }

    @Override
    public String getIdListener() {
        return idListener;
    }

    public class TilingParameters {
        public int nbSides;
        public int nbPolygons;
        public int depth;
    }

    public TilingParameters showRegularTilingDialogInput() {
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        JPanel regularTilingDialogInput = new JPanel(layout);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        JTextField description = new JTextField("You must choose 3 natural numbers. \n"
                + "P is the number of sides of each polygon and Q is the number of polygons around each vertex. \n"
                + "(P-2)*(Q-2) must be superior to 4");
        description.setEditable(false);
        regularTilingDialogInput.add(description, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;
        JTextField numberOfSidesLabel = new JTextField("The number of sides : ");
        numberOfSidesLabel.setEditable(false);
        regularTilingDialogInput.add(numberOfSidesLabel, c);

        c.gridx = 1;
        c.gridy = 1;
        JTextField numberOfSides = new JTextField(5);
        regularTilingDialogInput.add(numberOfSides, c);

        c.gridx = 0;
        c.gridy = 2;
        JTextField numberOfPolygonLabel = new JTextField("The number of polygons : ");
        numberOfPolygonLabel.setEditable(false);
        regularTilingDialogInput.add(numberOfPolygonLabel, c);

        c.gridx = 1;
        c.gridy = 2;
        JTextField numberOfPolygons = new JTextField(5);
        regularTilingDialogInput.add(numberOfPolygons, c);

        c.gridx = 0;
        c.gridy = 3;
        JTextField depthLabel = new JTextField("The depth : ");
        depthLabel.setEditable(false);
        regularTilingDialogInput.add(depthLabel, c);

        c.gridx = 1;
        c.gridy = 3;
        JTextField depth = new JTextField(5);
        regularTilingDialogInput.add(depth, c);

        JOptionPane.showMessageDialog(null, regularTilingDialogInput);

        TilingParameters params = new TilingParameters();

        boolean retry = false;

        try {
            params.nbSides = Integer.parseInt(numberOfSides.getText());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, "Wrong Value : The number of sides must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
            retry = true;
        }

        try {
            params.nbPolygons = Integer.parseInt(numberOfPolygons.getText());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, "Wrong Value : The number of polygons around each vertex must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
            retry = true;
        }

        try {
            params.depth = Integer.parseInt(depth.getText());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, "Wrong Value : The depth must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
            retry = true;
        }

        if ((params.nbSides - 2) * (params.nbPolygons - 2) <= 4) {
            JOptionPane.showMessageDialog(this, "Wrong Value : The product of the number of sides minus two and the number of polygons around each vertex minus two must be superior to four. Chose P and Q such as (P-2)*(Q-2) > 4", "Error", JOptionPane.WARNING_MESSAGE);
            retry = true;
        }

        if(retry) {
            params = showRegularTilingDialogInput();
        }

        return params;
    }

    public int showBuildingNbVertexDialogInput() {
        String[] coors = {"Enter the number of vertex (Natural number > 2)"};
        String[] inputs = CustomInputsDialog.showInputsDialog(coors);
        int nbPoints = 2;
        if (inputs != null) {
            try {
                nbPoints = Integer.parseInt(inputs[0]);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Wrong Values : must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
                nbPoints = showBuildingNbVertexDialogInput();
            }
        }
        return nbPoints;
    }

    public int showBuildingNbBranchesDialogInput() {
        String[] coors = {"Enter the number of branches (Natural number >= 2)"};
        String[] inputs = CustomInputsDialog.showInputsDialog(coors);
        int nbPoints = 2;
        if (inputs != null) {
            try {
                nbPoints = Integer.parseInt(inputs[0]);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Wrong Values : must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
                nbPoints = showBuildingNbVertexDialogInput();
            }
        }
        return nbPoints;
    }

    public double showBuildingLengthDialogInput() {
        return showBuildingLengthDialogInput(null);
    }

    public double showBuildingLengthDialogInput(String objLength) {
        String objectNameLabel;
        if(objLength != null) {
            objectNameLabel = " for " + objLength;
        } else {
            objectNameLabel = "";
        }

        String[]  indications = {"Enter an hyperbolic length (Real number > 0)" + objectNameLabel};
        String[] data = CustomInputsDialog.showInputsDialog(indications);

        double hyperbolicLength = 0;

        if (data != null) {
            try {
                hyperbolicLength = Double.parseDouble(data[0]);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Wrong Values : must be a positive double.", "Error", JOptionPane.WARNING_MESSAGE);
                hyperbolicLength = showBuildingLengthDialogInput(objLength);
            }
        }
        return hyperbolicLength;
    }

    public int showBuildingDepthDialogInput() {
        String[] indications = {"Enter a depth value (Natural number > 0)"};
        String[] data = CustomInputsDialog.showInputsDialog(indications);

        int depth = 0;

        if (data != null) {
            try {
                depth = Integer.parseInt(data[0]);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Wrong Values : must be a positive integer.", "Error", JOptionPane.WARNING_MESSAGE);
                depth = showBuildingDepthDialogInput();
            }
        }
        return depth;
    }
}
