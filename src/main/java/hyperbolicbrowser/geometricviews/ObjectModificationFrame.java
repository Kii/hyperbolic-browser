/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import hyperbolicbrowser.geometricmodels.GeometricSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author dolezv
 */
public class ObjectModificationFrame extends JFrame {

    public ObjectModificationFrame(String title) {
        super(title);
    }

    public ObjectModificationFrame(final GeometricSpace space, final PrintableObject obj) {
        super(obj.getName() + " modification");
        final String oldName = obj.getName();
        final JPanel informationPanel = new JPanel();

        JLabel lbl = new JLabel("Object name : ");
        informationPanel.add(lbl);
        final JTextField newName = new JTextField(8);
        newName.setText(obj.getName());
        informationPanel.add(newName);

        final JTextField colorSquare = new JTextField(2);
        colorSquare.setBackground(obj.getColor());
        colorSquare.setVisible(true);
        colorSquare.setOpaque(true);
        colorSquare.setEditable(false);
        colorSquare.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (colorSquare.contains(e.getPoint())) {
                    Color neoColor = JColorChooser.showDialog(null, "JColorChooser Sample", obj.getColor());
                    obj.setColor(neoColor);
                    colorSquare.setBackground(obj.getColor());
                    colorSquare.revalidate();
                    colorSquare.repaint();
                }
            }
        });
        informationPanel.add(colorSquare);

        JButton saveButton = new JButton("Save Changes");
        informationPanel.add(saveButton);
        saveButton.addActionListener(e -> {
            obj.setName(newName.getText());
            space.modifyObject(oldName, obj);
            dispose();
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        informationPanel.add(cancelButton);

        setPreferredSize(informationPanel.getPreferredSize());
        add(informationPanel);
    }
    
    public ObjectModificationFrame(final GeometricSpace space, final List<PrintableObject> selection) {
        super("Multiple modification");
        final JPanel informationPanel = new JPanel();

        final JTextField colorSquare = new JTextField(2);
        colorSquare.setBackground(selection.get(0).getColor());
        colorSquare.setVisible(true);
        colorSquare.setOpaque(true);
        colorSquare.setEditable(false);
        colorSquare.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (colorSquare.contains(e.getPoint())) {
                    Color neoColor = JColorChooser.showDialog(null, "JColorChooser Sample", selection.get(0).getColor());
                    for(PrintableObject object : selection) {
                        object.setColor(neoColor);
                    }
                    colorSquare.setBackground(selection.get(0).getColor());
                    colorSquare.revalidate();
                    colorSquare.repaint();
                }
            }
        });
        informationPanel.add(colorSquare);

        JButton saveButton = new JButton("Save Changes");
        informationPanel.add(saveButton);
        saveButton.addActionListener(e -> {
            for(PrintableObject object : selection) {
                space.modifyObject(object.getName(), object);
            }
            dispose();
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        informationPanel.add(cancelButton);

        setPreferredSize(informationPanel.getPreferredSize());
        add(informationPanel);
    }
}
