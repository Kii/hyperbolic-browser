/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import hyperbolicbrowser.geometriccontrollers.MathematicTranslation;
import hyperbolicbrowser.geometricmodels.geometricobjects.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.markings.GeometricMarking;

/**
 *
 * @author Rousselle RÃ©mi (remi.rousselle@isen-lille.fr)
 */
public class PoincareDiskPanel extends DiskPanel {

    /**
     * Generated
     */
    private static final long serialVersionUID = -3625807312517988210L;
    public static final double metricValue = PoincareDiskPoint.euclideanDistanceFromDiskCenter(1d);

    public PoincareDiskPanel(int width, int height, MathematicTranslation mt) {
        super(metricValue, width, height, mt);

        name = "Poincare Disk";

        EuclideanPoint diskCenter = disk.getCenter();
        int diskDiameter = (int) (disk.getRadius() * 2);

        ArrayList<PrintablePoint> axesPoints = new ArrayList<>();
        axesPoints.add(new EuclideanPoint(diskCenter.getX() + metricValue * 0.5 * diskDiameter, diskCenter.getY(), "i"));
        axesPoints.add(new EuclideanPoint(diskCenter.getX(), diskCenter.getY() - metricValue * 0.5 * diskDiameter, "j"));

        marking = new GeometricMarking(new EuclideanPoint(diskCenter.getX(), diskCenter.getY(), "origin"), axesPoints);
    }

    /**
     * @param p is an euclidean point expressed
     * @return pixel coordinates of this point on the screen
     */
    @Override
    public EuclideanPoint complexEuclideanToPixel(TransformablePoint p) {
        return new EuclideanPoint(margin + getDiskRadius() * (1 + p.getX()), margin + getDiskRadius() * (1 - p.getY()), p.getName());
    }

    /**
     * @param p is a pixel point on the screen
     * @return euclidean coordinates of this point on the Poincaré Disk
     */
    @Override
    public TransformablePoint pixelToComplexEuclidean(EuclideanPoint p) {
        EuclideanPoint diskCenter = disk.getCenter();
        return new PoincareDiskPoint((p.getX() - diskCenter.getX()) / getDiskRadius(), (diskCenter.getY() - p.getY()) / getDiskRadius(), p.getName());
    }

    protected Shape getHyperbolicLinePixelRepresentation(PrintablePath l, boolean isBounded) {
        if (l.getP1() instanceof EuclideanPoint && l.getP2() instanceof EuclideanPoint) {
            EuclideanPoint p1Pixel = (EuclideanPoint) l.getP1();
            EuclideanPoint p2Pixel = (EuclideanPoint) l.getP2();

            TransformablePoint p1 = pixelToComplexEuclidean(p1Pixel);
            TransformablePoint p2 = pixelToComplexEuclidean(p2Pixel);

            EuclideanPoint centrePixel = complexEuclideanToPixel(new PoincareDiskPoint("origin"));
            double pixelErrorAllowed = 1d;
            double errorAllowed = 0.01d;

            if (p1Pixel.distance(p2Pixel) > pixelErrorAllowed) {
                EuclideanLine line = new EuclideanLine(p1, p2);
                boolean throughOrigin = false;
                if (centrePixel.distance(p1Pixel) <= 1d || centrePixel.distance(p2Pixel) <= 1d) {
                    throughOrigin = true;
                }
                try {
                    if (Math.abs(line.getImage(0)) < errorAllowed) {
                        throughOrigin = true;
                    }
                } catch (Throwable ignored) {
                }
                try {
                    if (Math.abs(line.getCore(0)) < errorAllowed) {
                        throughOrigin = true;
                    }
                } catch (Throwable ignored) {
                }
                if (centrePixel.distance(p1Pixel) < pixelErrorAllowed || centrePixel.distance(p2Pixel) < pixelErrorAllowed) {
                    throughOrigin = true;
                }
                if (throughOrigin) { //the line goes through O (
                    //it's an euclidean line
                    if (isBounded) {
                        return getPixelSegment(p1, p2);
                    } else {
                        List<EuclideanPoint> linePoints = line.intersect(new EuclideanCircle("UnitCircle"));
                        if (linePoints.size() == 2) {
                            TransformablePoint p3 = new PoincareDiskPoint(new PolarCoordinate(1d, linePoints.get(0).getAngle()), "p3");
                            TransformablePoint p4 = new PoincareDiskPoint(new PolarCoordinate(1d, linePoints.get(1).getAngle()), "p4");
                            return getPixelSegment(p3, p4);
                        }
                    }
                } else {
                    //It's an euclidean arc
                    if (centrePixel.distance(p1Pixel) > 1d && centrePixel.distance(p2Pixel) > 1d) {
                        double inverseRadiusP1 = 1d / p1.getRadius();
                        double inverseRadiusP2 = 1d / p2.getRadius();
                        EuclideanPoint inverseP1 = new EuclideanPoint(new PolarCoordinate(inverseRadiusP1, p1.getAngle()), "");
                        EuclideanPoint inverseP2 = new EuclideanPoint(new PolarCoordinate(inverseRadiusP2, p2.getAngle()), "");

                        EuclideanPoint M, N;
                        M = EuclideanPoint.getMiddlePoint(p1, inverseP1);
                        N = EuclideanPoint.getMiddlePoint(p2, inverseP2);

                        EuclideanLine p1ToCenterLine = new EuclideanLine(EuclideanPoint.ORIGIN, p1);
                        EuclideanLine p2ToCenterLine = new EuclideanLine(EuclideanPoint.ORIGIN, p2);

                        EuclideanLine orthoM = p1ToCenterLine.orthogonal(M);
                        EuclideanLine orthoN = p2ToCenterLine.orthogonal(N);

                        PoincareDiskPoint center = new PoincareDiskPoint(orthoM.intersect(orthoN));
                        if (center != null) {
                            if (!isBounded) {
                                return getPixelCirle(center, p1);
                            } else {
                                return getPixelBoundedCirle(center, p1, p2, l.isSelected());
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public TransformablePoint getEuclideanCircleCenter(TransformablePoint complexCenter, TransformablePoint complexPointOnCircle) {
        PoincareDiskPoint centeredPointOnCircle = new PoincareDiskPoint(complexPointOnCircle);

        centeredPointOnCircle.rotate(-complexCenter.getAngle());
        centeredPointOnCircle.translate(-complexCenter.getRadius());
        centeredPointOnCircle.rotate(-complexCenter.getAngle());

        PoincareDiskPoint centeredFirstPointOfDiameter = new PoincareDiskPoint(new PolarCoordinate(centeredPointOnCircle.getRadius(), complexCenter.getAngle()), "");
        PoincareDiskPoint centeredSecondPointOfDiameter = new PoincareDiskPoint(EuclideanPoint.multiply(centeredFirstPointOfDiameter, -1d));

        centeredFirstPointOfDiameter.rotate(-complexCenter.getAngle());
        centeredFirstPointOfDiameter.translate(complexCenter.getRadius());
        centeredFirstPointOfDiameter.rotate(complexCenter.getAngle());

        centeredSecondPointOfDiameter.rotate(-complexCenter.getAngle());
        centeredSecondPointOfDiameter.translate(complexCenter.getRadius());
        centeredSecondPointOfDiameter.rotate(complexCenter.getAngle());

        return new PoincareDiskPoint(EuclideanPoint.getMiddlePoint(centeredFirstPointOfDiameter, centeredSecondPointOfDiameter));

    }

    @Override
    public void drawHyperbolicCircle(Graphics g, PrintableCircle c) {
        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        if (pixelCenter != null && pixelPointOnCircle != null) {
            TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
            TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

            TransformablePoint euclideanCenter = new PoincareDiskPoint(getEuclideanCircleCenter(complexCenter, complexPointOnCircle));
            EuclideanCircle tmpCircle = new EuclideanCircle(complexEuclideanToPixel(euclideanCenter), pixelPointOnCircle, c.getName());

            Rectangle boundingBox = tmpCircle.getBoundingBox();
            g.drawArc(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0, 360);
        }
    }

    @Override
    public double distancePixelFromPointToCircle(EuclideanPoint clickedPoint, PrintableCircle c) {
        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        if (pixelCenter != null && pixelPointOnCircle != null) {
            TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
            TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

            TransformablePoint euclideanCenter = new PoincareDiskPoint(getEuclideanCircleCenter(complexCenter, complexPointOnCircle));
            EuclideanPoint euclideanPixelCenter = complexEuclideanToPixel(euclideanCenter);
            EuclideanCircle tmpCircle = new EuclideanCircle(euclideanPixelCenter, pixelPointOnCircle, c.getName());

            return Math.abs(clickedPoint.distance(euclideanPixelCenter) - tmpCircle.getRadius());
        }

        return Double.MAX_VALUE;
    }
}
