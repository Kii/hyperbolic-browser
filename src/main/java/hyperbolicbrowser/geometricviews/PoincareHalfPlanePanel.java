/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.util.ArrayList;

import hyperbolicbrowser.geometriccontrollers.MathematicTranslation;
import hyperbolicbrowser.geometricmodels.geometricobjects.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.markings.GeometricMarking;

/**
 *
 * @author ISEN
 */
public class PoincareHalfPlanePanel extends GeometricView {

    /**
     * Generated
     */
    private static final long serialVersionUID = 6320914057850961719L;
    protected int scaleX = 10;
    protected int scaleY = 10;
    protected EuclideanPoint originPixel;

    public PoincareHalfPlanePanel(int width, int height, MathematicTranslation mt) {
        super(width, height, mt);
        addMouseListener(this);
        name = "Poincare Half-Plan";

        ArrayList<PrintablePoint> axesPoints = new ArrayList<>();
        axesPoints.add(new EuclideanPoint(margin + width, margin + height, "i"));
        axesPoints.add(new EuclideanPoint(margin + width / 2d, margin, "j"));

        marking = new GeometricMarking(new EuclideanPoint(margin + width / 2d, margin + height, "origin"), axesPoints);

    }

    @Override
    public EuclideanPoint complexEuclideanToPixel(TransformablePoint p) {
        EuclideanPoint origin = (EuclideanPoint) marking.getOrigin();
        double x = p.getX() * (width / scaleX) + origin.getX();
        double y = -p.getY() * (height / scaleY) + origin.getY();

        return new EuclideanPoint(x, y, p.getName());
    }

    @Override
    public TransformablePoint pixelToComplexEuclidean(EuclideanPoint p) {
        EuclideanPoint origin = (EuclideanPoint) marking.getOrigin();
        double x = (p.getX() - origin.getX()) / (width / scaleX);
        double y = (origin.getY() - p.getY()) / (height / scaleY);

        return new PoincareHalfPlanePoint(x, y, p.getName());
    }

    @Override
    protected void drawInitialView(Graphics g) {
        g.setColor(Color.lightGray);
        g.fillRect(0, 0, width + 2 * margin, height + 2 * margin);

        g.setColor(Color.white);
        g.fillRect(margin, margin, width, height);

        g.setColor(Color.black);
        g.drawLine(margin, margin + height, width + margin, margin + height);

        g.setFont(new Font("Arial", Font.BOLD, 13));
        g.drawString("Poincare Half-Plan", width / 2, height + 20 + margin);

        if (showMarking) {
            g.setColor(new Color(255, 0, 0, 40));

            g.setColor(new Color(0, 0, 0, 40));

            if (originPixel == null) {
                originPixel = hyperboloidToPixel(new HyperboloidPoint("origin", 0d, 0d, 1d));
            }

            if (originPixel != null) {
                g.drawLine((int) originPixel.getX(), (int) originPixel.getY() - crossSize, (int) originPixel.getX(), (int) originPixel.getY() + crossSize);
                g.drawLine((int) originPixel.getX() - crossSize, (int) originPixel.getY(), (int) originPixel.getX() + crossSize, (int) originPixel.getY());
            }

            g.setColor(Color.black);
            g.setFont(
                    new Font("Arial", Font.BOLD, 13));
        }

        Dimension dim = new Dimension(width + 1 + 2 * margin, height + 100 + 2 * margin);
        setPreferredSize(dim);
        setSize(dim);
    }

    @Override
    public boolean isPixelInsideView(int x, int y) {
        boolean res = false;
        if (x > margin && x < width + margin && y > margin && y < height + margin) {
            res = true;
        }
        return res;
    }
    
    @Override
    protected Shape getHyperbolicLinePixelRepresentation(PrintablePath l, boolean isBounded) {
	  if (l.getP1() instanceof EuclideanPoint && l.getP2() instanceof EuclideanPoint) {
          EuclideanPoint p1Pixel = (EuclideanPoint) l.getP1();
          EuclideanPoint p2Pixel = (EuclideanPoint) l.getP2();

          TransformablePoint p1Complex = pixelToComplexEuclidean(p1Pixel);
          TransformablePoint p2Complex = pixelToComplexEuclidean(p2Pixel);

          double pixelErrorAllowed = 2d;
          
          if (Math.abs(p1Pixel.getX() - p2Pixel.getX()) < pixelErrorAllowed) {
              if (isBounded) {
                  return getPixelSegment(p1Complex, p2Complex);
              } else {
                  //TODO draw a line instead of a segment => find intersection with model
                  return getPixelSegment(p1Complex, p2Complex);
              }
          } else {
              EuclideanPoint centerPixel;
              if (p1Pixel.getY() == p2Pixel.getY()) {
                  centerPixel = new EuclideanPoint((p1Pixel.getX() + p2Pixel.getX()) / 2d, height);
              } else {
                  EuclideanLine p1line = new EuclideanLine(p1Pixel, p2Pixel);

                  int x = (int) ((p1Pixel.getX() + p2Pixel.getX()) / 2d);
                  int y = (int) ((p1Pixel.getY() + p2Pixel.getY()) / 2d);
                  EuclideanLine p1ortho = p1line.orthogonal(new EuclideanPoint(x, y));

                  EuclideanLine horizon = EuclideanLine.build(0d, height, "");
                  centerPixel = p1ortho.intersect(horizon);
              }

              TransformablePoint centerComplex = pixelToComplexEuclidean(centerPixel);

              if (!isBounded) {
            	  return getPixelCirle(centerComplex, p1Complex);
              } else {
                  return getPixelBoundedCirle(centerComplex, p1Complex, p2Complex, l.isSelected());
              }
          }
      }
	  return null;
    }
    
    @Override
    protected final Shape getPixelCirle(TransformablePoint center, TransformablePoint pointOnCircle) {
        EuclideanPoint pixelCenterHyperbolic = complexEuclideanToPixel(center);
        EuclideanPoint pixelPointOnCircle = complexEuclideanToPixel(pointOnCircle);
        EuclideanCircle euclideanCirclePixel = new EuclideanCircle(pixelCenterHyperbolic, pixelCenterHyperbolic.distance(pixelPointOnCircle), "");
        EuclideanPoint pixelCenterEuclidean = euclideanCirclePixel.getCenter();
        if (euclideanCirclePixel.getRadius() != Double.NaN && pixelCenterEuclidean != null) {
            Rectangle boundingBox = euclideanCirclePixel.getBoundingBox();
            //TODO find intersections with half-plane
            return new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0d, 180d, Arc2D.OPEN);
        }
        return null;
    }
    
    @Override
    protected Shape getPixelBoundedCirle(TransformablePoint complexCenterHyperbolic, TransformablePoint complexP1, TransformablePoint complexP2, boolean isSelected) {
        EuclideanPoint pixelCenterHyperbolic = complexEuclideanToPixel(complexCenterHyperbolic);
        EuclideanPoint p1Pixel = complexEuclideanToPixel(complexP1);

        EuclideanCircle pixelCircle = new EuclideanCircle(pixelCenterHyperbolic, p1Pixel, "");
        if (pixelCircle.getRadius() != Double.NaN) {
            Rectangle boundingBox = pixelCircle.getBoundingBox();

            EuclideanPoint inverseComplexCenterHyperbolic = EuclideanPoint.multiply(complexCenterHyperbolic, -1d);

            EuclideanPoint translatedP1 = EuclideanPoint.sum(complexP1, inverseComplexCenterHyperbolic);
            EuclideanPoint translatedP2 = EuclideanPoint.sum(complexP2, inverseComplexCenterHyperbolic);

            double angleP1 = translatedP1.getAngle();
            double angleP2 = translatedP2.getAngle();

            double startAngle, diffAngle;
            if(angleP1 > angleP2) {
            	startAngle = angleP2;
            	diffAngle = angleP1 - angleP2;
                if(diffAngle > Math.PI) {
                	startAngle = angleP1;
                	diffAngle = 2d*Math.PI - diffAngle;
                }
            } else {
            	startAngle = angleP1;
            	diffAngle = angleP2 - angleP1;
            	if(diffAngle > Math.PI) {
                	startAngle = angleP2;
                	diffAngle = 2d*Math.PI - diffAngle;
                }
            }

            return new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, Math.toDegrees(startAngle), Math.toDegrees(diffAngle), Arc2D.OPEN);
        }
        return null;
    }

    @Override
    public TransformablePoint getEuclideanCircleCenter(TransformablePoint complexCenter, TransformablePoint complexPointOnCircle) {
        double hypDist = complexCenter.distance(complexPointOnCircle);
        EuclideanPoint bottomCirclePoint, topCirclePoint;

        bottomCirclePoint = getBottomPoint(complexCenter, hypDist);
        topCirclePoint = getTopPoint(complexCenter, hypDist);

        return new PoincareHalfPlanePoint(EuclideanPoint.getMiddlePoint(bottomCirclePoint, topCirclePoint));
    }

    public EuclideanPoint getBottomPoint(EuclideanPoint center, double distance) {
        //vertical_dist = ln(yB) - ln(yC) si yB > yC
        //yB = exp(dist + ln(yC))
        double y = Math.exp(distance + Math.log(center.getY()));
        return new EuclideanPoint(center.getX(), y, "bottom");
    }

    public EuclideanPoint getTopPoint(EuclideanPoint center, double distance) {
        //vertical_dist = ln(yC) - ln(yT) si yC > yT
        //yT = exp(ln(yC) - dist)
        double y = Math.exp(Math.log(center.getY()) - distance);
        return new EuclideanPoint(center.getX(), y, "top");
    }

    @Override
    public void drawHyperbolicCircle(Graphics g, PrintableCircle c) {
        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        if (pixelCenter != null && pixelPointOnCircle != null) {
            TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
            TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

            TransformablePoint euclideanCenter = new PoincareHalfPlanePoint(getEuclideanCircleCenter(complexCenter, complexPointOnCircle));
            EuclideanCircle tmpCircle = new EuclideanCircle(complexEuclideanToPixel(euclideanCenter), pixelPointOnCircle, c.getName());

            Rectangle boundingBox = tmpCircle.getBoundingBox();
            g.drawArc(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0, 360);
        }
    }

    @Override
    public double distancePixelFromPointToCircle(EuclideanPoint clickedPoint, PrintableCircle c) {
        EuclideanPoint pixelCenter = (EuclideanPoint) c.getCenter();
        EuclideanPoint pixelPointOnCircle = (EuclideanPoint) c.getPointOnCircle();

        if (pixelCenter != null && pixelPointOnCircle != null) {
            TransformablePoint complexCenter = pixelToComplexEuclidean(pixelCenter);
            TransformablePoint complexPointOnCircle = pixelToComplexEuclidean(pixelPointOnCircle);

            TransformablePoint euclideanCenter = new PoincareHalfPlanePoint(getEuclideanCircleCenter(complexCenter, complexPointOnCircle));
            EuclideanPoint euclideanPixelCenter = complexEuclideanToPixel(euclideanCenter);
            EuclideanCircle tmpCircle = new EuclideanCircle(euclideanPixelCenter, pixelPointOnCircle, c.getName());

            return Math.abs(clickedPoint.distance(euclideanPixelCenter) - tmpCircle.getRadius());
        }
        return Double.MAX_VALUE;
    }
}
