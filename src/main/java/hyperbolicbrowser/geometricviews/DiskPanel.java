/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricviews;

import java.awt.*;

import hyperbolicbrowser.geometriccontrollers.MathematicTranslation;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;

import java.awt.geom.Arc2D;
import java.util.List;

//Draw a circle
/**
 *
 * @author ISEN
 */
public abstract class DiskPanel extends GeometricView {
    private static final long serialVersionUID = 1743074239223449280L;

    protected double metricValue;

    protected EuclideanCircle disk;

    public DiskPanel(double metricValue, int width, int height, MathematicTranslation mt) {
        super(width, height, mt);
        addMouseListener(this);

        int diskDiameter = Math.min(width, height);

        EuclideanPoint diskCenter = new EuclideanPoint(margin + diskDiameter / 2, margin + diskDiameter / 2, "Center");
        disk = new EuclideanCircle(diskCenter,diskDiameter / 2, "Disk");
        this.metricValue = metricValue;

        setSize(2 * margin + diskDiameter, 2 * margin + diskDiameter);
    }

    public int getDiskRadius() {
        return (int) disk.getRadius();
    }


    @Override
    protected void drawInitialView(Graphics g) {
        int diskDiameter = (int) (disk.getRadius() * 2);
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, 2 * margin + diskDiameter, 2 * margin + diskDiameter);

        //Unit Circle
        g.setColor(Color.white);
        g.fillOval(margin, margin, diskDiameter, diskDiameter);

        if (showMarking) {
            g.setColor(new Color(255, 0, 0, 30));
            g.drawOval(margin + (int) (diskDiameter * (1 - metricValue) / 2), margin + (int) (diskDiameter * (1 - metricValue) / 2), (int) (diskDiameter * metricValue), (int) (diskDiameter * metricValue));

            g.setColor(new Color(0, 0, 0, 30));
            //g.setColor(Color.black);

            g.drawLine(diskDiameter
                    / 2 - crossSize + margin, diskDiameter / 2 + margin, diskDiameter / 2 + crossSize + margin, diskDiameter / 2 + margin);
            g.drawLine(diskDiameter
                    / 2 + margin, diskDiameter / 2 - crossSize + margin, diskDiameter / 2 + margin, diskDiameter / 2 + crossSize + margin);

            g.setColor(Color.BLACK);
            g.setFont(
                    new Font("Arial", Font.BOLD, 13));
            g.drawString(name, diskDiameter
                    / 2, diskDiameter + 20 + margin);
        }
    }

    @Override
    public boolean isPixelInsideView(int x, int y) {
        EuclideanPoint pixel = new EuclideanPoint(x, y, "");
        EuclideanPoint pixelCenter = disk.getCenter();
        double distancePixel = pixel.distance(pixelCenter);

        return distancePixel < disk.getRadius();
    }

    @Override
    protected final Shape getPixelCirle(TransformablePoint center, TransformablePoint pointOnCircle) {
        EuclideanPoint pixelCenter = complexEuclideanToPixel(center);
        EuclideanPoint pixelPointOnCircle = complexEuclideanToPixel(pointOnCircle);
        EuclideanCircle euclideanCirclePixel = new EuclideanCircle(pixelCenter, pixelCenter.distance(pixelPointOnCircle), "");
        EuclideanPoint centerPixel = euclideanCirclePixel.getCenter();
        if (euclideanCirclePixel.getRadius() != Double.NaN && centerPixel != null) {
            Rectangle boundingBox = euclideanCirclePixel.getBoundingBox();

            List<EuclideanPoint> intersections = disk.getIntersectionPoints(euclideanCirclePixel);
            if (intersections.size() == 2) {
                EuclideanPoint tmpCenterComplex = pixelToComplexEuclidean(euclideanCirclePixel.getCenter());
                tmpCenterComplex.multiply(-1);
                EuclideanPoint tmpInter0 = pixelToComplexEuclidean(intersections.get(0));
                EuclideanPoint tmpInter1 = pixelToComplexEuclidean(intersections.get(1));

                EuclideanPoint translate0 = EuclideanPoint.sum(tmpInter0, tmpCenterComplex);
                EuclideanPoint translate1 = EuclideanPoint.sum(tmpInter1, tmpCenterComplex);

                int angle0 = (int) Math.toDegrees(translate0.getAngle());
                int angle1 = (int) Math.toDegrees(translate1.getAngle());
                int diffAngle = ((angle1 - angle0) + 360) % 360;

                return new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, angle1, 360 - diffAngle, Arc2D.OPEN);
            }
        }
        return null;
    }
    
    @Override
    protected Shape getPixelBoundedCirle(TransformablePoint center, TransformablePoint p1, TransformablePoint p2, boolean isSelected) {
        EuclideanPoint centerPixel = complexEuclideanToPixel(center);
        EuclideanPoint p1Pixel = complexEuclideanToPixel(p1);
        EuclideanPoint p2Pixel = complexEuclideanToPixel(p2);

        EuclideanCircle euclideanCirclePixel = new EuclideanCircle(centerPixel, centerPixel.distance(p1Pixel), "");
        if (euclideanCirclePixel.getRadius() != Double.NaN) {
            Rectangle boundingBox = euclideanCirclePixel.getBoundingBox();

            List<EuclideanPoint> intersections = disk.getIntersectionPoints(euclideanCirclePixel);
            if (intersections.size() == 2) {
                EuclideanPoint tmpCenterComplex = EuclideanPoint.multiply(center, -1d);

                EuclideanPoint translate0 = EuclideanPoint.sum(p1, tmpCenterComplex);
                EuclideanPoint translate1 = EuclideanPoint.sum(p2, tmpCenterComplex);

                double angle0 = Math.toDegrees(translate0.getAngle());
                double angle1 = Math.toDegrees(translate1.getAngle());

                double diffAngle = angle1 - angle0;

                Arc2D arc;

                if (diffAngle > 0) {
                    if (diffAngle < 180) {
                        arc = new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, angle0, diffAngle, Arc2D.OPEN);
                    } else {
                        arc = new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, angle1, 360d - diffAngle, Arc2D.OPEN);
                    }
                } else {
                    if (diffAngle > -180) {
                        arc = new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, angle1, -diffAngle, Arc2D.OPEN);
                    } else {
                        arc = new Arc2D.Double(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, angle0, 360d + diffAngle, Arc2D.OPEN);
                    }
                }
                return arc;
            }
        }
        return null;
    }

}
