/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import hyperbolicbrowser.view.MultiViewsTabbedPane;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class MainTabbedPane extends MultiViewsTabbedPane {

    /**
	 * Generated
	 */
	private static final long serialVersionUID = 3490129420372246491L;

	public MainTabbedPane(int width, int height) {
        super("   Home   ", new JPanel());
        
        setSize(width, height);
        setMaximumSize(new Dimension(width, height));
        mainComponent.setLayout(new BorderLayout());
        mainComponent.setSize(width, height);

        JPanel titlePanel = new JPanel();
        titlePanel.setBackground(Color.LIGHT_GRAY);
        JLabel titleLabel = new JLabel("GEOMETRIC HYPER-EXPLORER", JLabel.CENTER);
        Font font = new Font("Serif", Font.PLAIN, 50);
        titleLabel.setFont(font); 
        titlePanel.add(titleLabel);
        titleLabel.setSize(width/2, height / 10);
        mainComponent.add(titlePanel, BorderLayout.NORTH);
        
        JPanel mainPanel = new JPanel(new BorderLayout());
        ImageIcon icon = new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/fractal_circle_limit_III_bulatov.png"));
        Dimension bestSize = getBestSizeForIcon(new Dimension(icon.getIconWidth(), icon.getIconHeight()), new Dimension(width, height));
        ImageIcon newIcon = new ImageIcon((icon.getImage()).getScaledInstance(bestSize.width, bestSize.height, java.awt.Image.SCALE_SMOOTH));
        JLabel imgLabel = new JLabel(newIcon, JLabel.CENTER);
        imgLabel.setSize(width, (int) (height * (8d/10d)));
        mainPanel.add(imgLabel, BorderLayout.CENTER);
        JLabel imgCredits = new JLabel("Fractal Circle Limit III - illustration by Vladimir Bulatov", JLabel.CENTER);
        mainPanel.add(imgCredits, BorderLayout.SOUTH);
        mainPanel.setSize(width, (int) (height * (8d/10d)));
        
        mainComponent.add(mainPanel, BorderLayout.CENTER);

        
        JPanel credits = new JPanel();
        credits.setBackground(Color.LIGHT_GRAY);
        JLabel creditsLabel = new JLabel("Developed by DOLEZ Vincent & ROUSSELLE Rémi, with the very useful help of CHENEVERT Gabriel & PARENT Benjamin.", JLabel.CENTER);
        creditsLabel.setSize(width/2, height/10);
        Font fontCredit = new Font("Serif", Font.PLAIN, 32);
        creditsLabel.setFont(fontCredit);
        credits.add(creditsLabel);
        credits.setSize(width, height/10);
        //mainComponent.add(credits, BorderLayout.SOUTH);
    }
	
	public Dimension getBestSizeForIcon(Dimension imageDim, Dimension screenDim) {
		double imageWidth = imageDim.getWidth();
		double imageHeight = imageDim.getHeight();
		double screenWidth = screenDim.getWidth();
		double screenHeight = screenDim.getHeight();
		
		double rapportHeight = screenHeight / imageHeight;
		double rapportWidth = screenWidth / imageWidth;
		
		double rapport = Math.min(rapportHeight, rapportWidth);
		
		return new Dimension((int) (imageDim.getWidth() * rapport), (int) (imageDim.getHeight() * rapport));
		
	}
}
