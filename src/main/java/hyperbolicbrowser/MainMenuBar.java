/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser;

import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author ISEN
 */
public class MainMenuBar extends JMenuBar {
    
    /**
	 * Generated
	 */
	private static final long serialVersionUID = 1065344488826640222L;
    private JMenu editMenu;
    private JMenuItem saveItem, loadItem, insertPointItem, insertLineItem;
    
    public MainMenuBar(){
        saveItem = new JMenuItem("Save Space");
        saveItem.setName("save");
        
        loadItem = new JMenuItem("Load Space");
        loadItem.setName("load");

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(saveItem);
        fileMenu.add(loadItem);
        add(fileMenu);
        
        insertPointItem = new JMenuItem("Insert Point");
        insertLineItem = new JMenuItem("Insert Line");
        editMenu = new JMenu("Edit");
        editMenu.add(insertPointItem);
        editMenu.add(insertLineItem);
        add(editMenu);
        
    }
    
    public void addActionListener(ActionListener l) {
        saveItem.addActionListener(l);
        loadItem.addActionListener(l);
    }
}
