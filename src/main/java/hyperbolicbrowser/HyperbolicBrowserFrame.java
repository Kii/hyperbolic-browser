/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import hyperbolicbrowser.geometriccontrollers.BeltramiDiskController;
import hyperbolicbrowser.geometriccontrollers.PoincareDiskController;
import hyperbolicbrowser.geometriccontrollers.PoincareHalfPlaneController;
import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricviews.BeltramiDiskPanel;
import hyperbolicbrowser.geometricviews.PoincareDiskPanel;
import hyperbolicbrowser.geometricviews.PoincareHalfPlanePanel;
import hyperbolicbrowser.toolbar.ActionButtonsToolbar;
import hyperbolicbrowser.toolbar.HyperbolicFileFilter;
import hyperbolicbrowser.toolbar.infosToolbar.ToolbarInformation;
import hyperbolicbrowser.view.MultiViewsTabbedPane;

/**
 *
 * @author ISEN
 */
public class HyperbolicBrowserFrame extends JFrame implements ActionListener {

	protected final PoincareDiskPanel pDiskView;
	protected final PoincareDiskController pDiskController;
	protected final PoincareHalfPlanePanel pHalfPlanView;
	protected final PoincareHalfPlaneController pHalfPlanController;
	protected final BeltramiDiskPanel bDiskView;
	protected final BeltramiDiskController bDiskController;
    protected HyperboloidSpace space;
    protected final HyperbolicFileFilter hyperbolicFilter = new HyperbolicFileFilter();

    public HyperbolicBrowserFrame() {
        super("Hyperbolic Browser");
    	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLayout(new BorderLayout());

        int tabWidth = screenSize.width;
        int tabHeight = (int) (screenSize.height * (2d/3d));

        int buttonToolbarWidth = screenSize.width;
        int buttonToolbarHeight = screenSize.height / 12;

        int circleDrawingWidth = (int) (tabWidth * 4d/5d);
        int circleDrawingHeight = tabHeight;

        int halfPlaneDrawingWidth = Math.min(circleDrawingWidth, circleDrawingHeight);
        int halfPlaneDrawingHeight = Math.min(circleDrawingWidth, circleDrawingHeight);

        int objectToolbarWidth = tabWidth - circleDrawingWidth;
        int objectToolbarHeight = (int) (tabHeight * 0.9d);

        MainMenuBar menuBar = new MainMenuBar();
        menuBar.addActionListener(this);

        setJMenuBar(menuBar);
        
        //CONTROLLERS

        space = new HyperboloidSpace();
        
        pDiskController = new PoincareDiskController(space);
        pDiskView = new PoincareDiskPanel(circleDrawingWidth, circleDrawingHeight, pDiskController);
        pDiskView.addActionUserListener(pDiskController);
        space.addGeometricSpaceListener(pDiskView);

        bDiskController = new BeltramiDiskController(space);
        bDiskView = new BeltramiDiskPanel(circleDrawingWidth, circleDrawingHeight, bDiskController);
        bDiskView.addActionUserListener(bDiskController);
        space.addGeometricSpaceListener(bDiskView);

        pHalfPlanController = new PoincareHalfPlaneController(space);
        pHalfPlanView = new PoincareHalfPlanePanel(halfPlaneDrawingWidth, halfPlaneDrawingHeight, pHalfPlanController);
        pHalfPlanView.addActionUserListener(pHalfPlanController);
        space.addGeometricSpaceListener(pHalfPlanView);

        ActionButtonsToolbar actionbar = new ActionButtonsToolbar(buttonToolbarWidth, buttonToolbarHeight);
        
        actionbar.addActionButtonListener(pDiskView);
        actionbar.addActionButtonListener(bDiskView);
        actionbar.addActionButtonListener(pHalfPlanView);
        
        add(actionbar, BorderLayout.NORTH);

        final ToolbarInformation pDiskToolbarInformation = new ToolbarInformation(space, objectToolbarWidth, objectToolbarHeight);
        pDiskToolbarInformation.setVisible(false);
        space.addGeometricSpaceListener(pDiskToolbarInformation);
        
        JPanel pDiskPanel = new JPanel(new BorderLayout());
        pDiskPanel.add(pDiskView, BorderLayout.CENTER);
        pDiskPanel.add(pDiskToolbarInformation, BorderLayout.WEST);
        
        final ToolbarInformation bDiskToolbarInformation = new ToolbarInformation(space, objectToolbarWidth, objectToolbarHeight);
        bDiskToolbarInformation.setVisible(false);
        space.addGeometricSpaceListener(bDiskToolbarInformation);
        
        JPanel bDiskPanel = new JPanel(new BorderLayout());
        bDiskPanel.add(bDiskView, BorderLayout.CENTER);
        bDiskPanel.add(bDiskToolbarInformation, BorderLayout.WEST);
        
        final ToolbarInformation pHalfPlanToolbarInformation = new ToolbarInformation(space, objectToolbarWidth, objectToolbarHeight);
        pHalfPlanToolbarInformation.setVisible(false);
        space.addGeometricSpaceListener(pHalfPlanToolbarInformation);
        
        JPanel pHalfPlanPanel = new JPanel(new BorderLayout());
        pHalfPlanPanel.add(pHalfPlanView, BorderLayout.CENTER);
        pHalfPlanPanel.add(pHalfPlanToolbarInformation, BorderLayout.WEST);

        //TABS
        MultiViewsTabbedPane tabs = new MainTabbedPane(tabWidth, tabHeight);
        tabs.addChangeListener(e -> {
            if(e.getSource() instanceof MainTabbedPane) {
                MainTabbedPane tab = (MainTabbedPane) e.getSource();
                pDiskToolbarInformation.setVisible(false);
                bDiskToolbarInformation.setVisible(false);
                pHalfPlanToolbarInformation.setVisible(false);
                if(tab.getSelectedIndex() == 1) {
                    pDiskToolbarInformation.setVisible(true);
                } else if(tab.getSelectedIndex() == 2) {
                    bDiskToolbarInformation.setVisible(true);
                } else if(tab.getSelectedIndex() == 3) {
                    pHalfPlanToolbarInformation.setVisible(true);
                }
            }
		});
        tabs.add("   Poincare Disk   ", pDiskPanel);
        tabs.add("   Beltrami-Klein Disk   ", bDiskPanel);
        tabs.add("   Poincare Half-Plan   ", pHalfPlanPanel);

        add(tabs, BorderLayout.CENTER);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            new HyperbolicBrowserFrame();
        } catch (Throwable e) {
            System.out.println("Exception in HyperbolicBrowserFrame main function : " + e.toString());
            e.printStackTrace();
        }
    }

    private void saveFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(hyperbolicFilter);
        int returnValue = fileChooser.showSaveDialog(this);
        File file;
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
            try {
                FileOutputStream fos;
                if(file.getAbsolutePath().endsWith(".hyp")) {
                    fos = new FileOutputStream(file.getAbsolutePath());
                } else {
                    fos = new FileOutputStream(file.getAbsolutePath()+".hyp");
                }
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(space);
                oos.flush();
                oos.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void loadFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.removeChoosableFileFilter(fileChooser.getAcceptAllFileFilter());
        fileChooser.addChoosableFileFilter(hyperbolicFilter);
        int returnValue = fileChooser.showOpenDialog(this);
        File file;
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            System.out.println("loading : " + fileChooser.getSelectedFile());
            file = fileChooser.getSelectedFile();
            try {
                FileInputStream fos = new FileInputStream(file.getAbsolutePath());
                ObjectInputStream ois = new ObjectInputStream(fos);
                HyperboloidSpace newSpace = (HyperboloidSpace) ois.readObject();
                space.set(newSpace);
                ois.close();
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        String name = item.getName();
        if (name.equals("save")) {
            saveFile();
        } else if (name.equals("load")) {
            loadFile();
        }
    }
}
