/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.listeners;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricviews.GeometricView;
import hyperbolicbrowser.toolbar.ActionsEnum;

import java.awt.event.MouseEvent;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public interface ActionUserListener extends ActionUserSelectionListener {

    void mousePressed(GeometricView view, int x, int y);

    void mouseReleased(GeometricView view, MouseEvent e);

    void mouseMoved(GeometricView view, int x, int y);

    void removeSelectedObjects();

    void clearModel();

    void currentActionChanged(GeometricView view, ActionsEnum ae);

    void addPoints(GeometricView view);
    
    void selectObjectFromPoint(GeometricView view, EuclideanPoint point, boolean keepOldSelection);

    void addLine(GeometricView view, EuclideanPoint point);

    void addSegment(GeometricView view, EuclideanPoint point);

    void grabPoint(GeometricView view, EuclideanPoint point);

    void moveGrabbedPoint(GeometricView view, EuclideanPoint point);

    void release(GeometricView view);

    void centerPoint(GeometricView view, EuclideanPoint clickedPoint);

    void translatePoints(EuclideanPoint clickedPoint);
    
    void translateDynamicMove(GeometricView view, EuclideanPoint clickedPoint);
    
    void translateDynamicStop(GeometricView view);

    void translateDynamic(GeometricView view, EuclideanPoint clickedPoint);

    void rotate(EuclideanPoint clickedPoint);
    
    void rotateDynamicMove(GeometricView view, EuclideanPoint clickedPoint);
    
    void rotateDynamicStop(GeometricView view);

    void rotateDynamic(GeometricView view, EuclideanPoint clickedPoint);
}