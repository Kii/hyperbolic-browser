/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.listeners;

import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public interface GeometricSpaceListener {

    String getIdListener();
    
    void pointAdded(PrintablePoint p);

    void pointMoved(PrintablePoint p);
    
    void pointRemoved(PrintablePoint p);
    
    void pointSelection(PrintablePoint p);

    void pointFlyOver(PrintablePoint p);

    void objectChanged(String oldName, PrintableObject p);
    
    void lineAdded(PrintablePath l);
    
    void lineRemoved(PrintablePath l);
    
    void lineSelection(PrintablePath l);

    void lineFlyOver(PrintablePath l);
    
    void segmentAdded(PrintablePath s);
    
    void segmentRemoved(PrintablePath s);
    
    void segmentSelection(PrintablePath s);

    void segmentFlyOver(PrintablePath s);
    
    void circleAdded(PrintableCircle c);
    
    void circleRemoved(PrintableCircle c);
    
    void circleSelection(PrintableCircle c);

    void circleFlyOver(PrintableCircle c);
    
    void spaceCleared();
    
    void spaceUpdated(HyperboloidSpace space);
}
