/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.listeners;

import java.util.List;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;

/**
 *
 * @author dolezv
 */
public interface ActionUserSelectionListener {
    
    void selectSegments(List<PrintablePath> selection);

    void selectPoints(List<PrintablePoint> selection);

    void selectLines(List<PrintablePath> selection);

    void selectCircles(List<PrintableCircle> selection);
}
