/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hyperbolicbrowser.listeners;

import hyperbolicbrowser.toolbar.ActionsEnum;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public interface ActionButtonListener {
    
    void currentActionChanged(ActionsEnum ae);
}
