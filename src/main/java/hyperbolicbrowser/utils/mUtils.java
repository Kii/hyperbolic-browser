/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.utils;

import java.util.List;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.PoincareDiskPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.PoincareHalfPlanePoint;

/**
 *
 * @author dolezv
 */
public class mUtils {

    public static int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = list.get(i);
        }
        return ret;
    }

    public static PoincareDiskPoint halfPlaneToDisk(PoincareHalfPlanePoint pointHalfPlane) {
        PoincareDiskPoint pointDisk;
        EuclideanPoint num, denum;

        num = EuclideanPoint.sum(pointHalfPlane, new EuclideanPoint(0d, -1d, ""));
        denum = EuclideanPoint.sum(pointHalfPlane, new EuclideanPoint(0d, 1d, ""));

        pointDisk = new PoincareDiskPoint(EuclideanPoint.divide(num, denum));

        return pointDisk;
    }

    public static PoincareHalfPlanePoint diskToHalfPlane(PoincareDiskPoint pointDisk) {
        PoincareHalfPlanePoint pointHalfPlane;
        EuclideanPoint num, denum;

        num = EuclideanPoint.sum(pointDisk, new EuclideanPoint(1d, 0d, ""));
        denum = EuclideanPoint.sum(new EuclideanPoint(1d, 0d, ""), EuclideanPoint.multiply(pointDisk, -1));

        pointHalfPlane = new PoincareHalfPlanePoint(EuclideanPoint.divide(num, denum));
        pointHalfPlane.multiply(new EuclideanPoint(0d, 1d, ""));

        return pointHalfPlane;
    }

    public static double argtanh(double x) {
        return Math.log((1d + x) / (1d - x)) / 2d;
    }

    public static double argcosh(double x) {
        return Math.log(x + Math.sqrt(Math.pow(x, 2d) - 1d));
    }
}
