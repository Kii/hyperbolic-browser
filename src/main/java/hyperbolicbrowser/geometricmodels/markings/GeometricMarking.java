/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hyperbolicbrowser.geometricmodels.markings;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import java.util.ArrayList;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class GeometricMarking {

    protected PrintablePoint origin;
    
    protected ArrayList<PrintablePoint> axesPoints;
    
    public GeometricMarking(PrintablePoint origin, ArrayList<PrintablePoint> axesPoints){
        this.origin = origin;
        this.axesPoints = axesPoints;
    }
    
    public PrintablePoint getOrigin(){
        return origin;
    }
    
    public ArrayList<PrintablePoint> getAxesPoints(){
        return axesPoints;
    }
}
