/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels.geometricobjects;

import java.awt.geom.Point2D;
import java.text.DecimalFormat;

import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.CartesianCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;

/**
 *
 * @author Vincent Dolez (vincent.dolez@isen-lille.fr)
 */
public class EuclideanPoint extends PrintablePoint {
    private static final long serialVersionUID = 916626785631680570L;

    public static final EuclideanPoint ORIGIN = new EuclideanPoint();

    protected final CartesianCoordinate cartesian;
    protected final PolarCoordinate polar;
    protected DecimalFormat df = new DecimalFormat("####0.00");

    public EuclideanPoint() {
        this(0d, 0d);
    }

    public EuclideanPoint(String name) {
        this(0d, 0d, name);
    }

    public EuclideanPoint(double x, double y) {
        this(x, y, "");
    }

    public EuclideanPoint(double x, double y, String name) {
        super(name);
        cartesian = new CartesianCoordinate(x, y);
        polar = cartesian.toPolar();
    }
    
    public EuclideanPoint(EuclideanPoint copy) {
        super(copy.name);
        cartesian = new CartesianCoordinate(copy.getX(), copy.getY());
        polar = new PolarCoordinate(copy.getRadius(), copy.getAngle());
    }

    public EuclideanPoint(CartesianCoordinate cartesianCoordinate) {
        this(cartesianCoordinate, "");
    }

    public EuclideanPoint(CartesianCoordinate cartesianCoordinate, String name) {
        super(name);
        cartesian = new CartesianCoordinate(cartesianCoordinate);
        polar = cartesian.toPolar();
    }

    public EuclideanPoint(PolarCoordinate polarCoordinate) {
        this(polarCoordinate, "");
    }

    public EuclideanPoint(PolarCoordinate polarCoordinate, String name) {
        super(name);
        polar = new PolarCoordinate(polarCoordinate);
        cartesian = polar.toCartesian();
    }

    public double getX() {
        return cartesian.getX();
    }

    public double getY() {
        return cartesian.getY();
    }

    public double getRadius() {
        return polar.getRadius();
    }

    public double getAngle() {
        return polar.getAngle();
    }

    public CartesianCoordinate getCartesian() {
        return new CartesianCoordinate(cartesian);
    }
    public void setCartesian(double x, double y) {
        cartesian.setX(x);
        cartesian.setY(y);
        polar.set(cartesian.toPolar());
    }

    public void setPolar(double radius, double angle) {
        polar.setRadius(radius);
        polar.setAngle(angle);
        cartesian.set(polar.toCartesian());
    }

    public void set(EuclideanPoint copy) {
        polar.set(copy.polar);
        cartesian.set(copy.cartesian);
    }

    public double distance(EuclideanPoint other) {
        return Point2D.distance(getX(), getY(), other.getX(), other.getY());
    }

    public void sum(EuclideanPoint other) {
        double returnX = getX() + other.getX();
        double returnY = getY() + other.getY();
        setCartesian(returnX, returnY);
    }

    public static EuclideanPoint sum(EuclideanPoint first, EuclideanPoint other) {
        double returnX = first.getX() + other.getX();
        double returnY = first.getY() + other.getY();
        return new EuclideanPoint(returnX, returnY, first.name);
    }

    public void subtract(EuclideanPoint other) {
        double returnX = getX() - other.getX();
        double returnY = getY() - other.getY();
        setCartesian(returnX, returnY);
    }

    public static EuclideanPoint subtract(EuclideanPoint first, EuclideanPoint other) {
        double returnX = first.getX() - other.getX();
        double returnY = first.getY() - other.getY();
        return new EuclideanPoint(returnX, returnY, first.name);
    }

    //Add a scalar to a complex number
    public void sum(double scalar) {
        setCartesian(getX() + scalar, getY());
    }

    //Add a scalar to a complex number
    public static EuclideanPoint sum(EuclideanPoint first, double scalar) {
        double returnX = first.getX() + scalar;
        double returnY = first.getY();
        return new EuclideanPoint(returnX, returnY, first.name);
    }

    public void multiply(EuclideanPoint other) {
        double returnX = (getX() * other.getX()) - (getY() * other.getY());
        double returnY = (getX() * other.getY()) + (getY() * other.getX());
        setCartesian(returnX, returnY);
    }

    public static EuclideanPoint multiply(EuclideanPoint p1, EuclideanPoint p2) {
        double returnX = (p1.getX() * p2.getX()) - (p1.getY() * p2.getY());
        double returnY = (p1.getX() * p2.getY()) + (p1.getY() * p2.getX());
        return new EuclideanPoint(returnX, returnY, p1.name);
    }

    public void multiply(double scalar) {
        setCartesian(getX() * scalar, getY() * scalar);
    }

    //Multiply a complex number by a scalar
    public static EuclideanPoint multiply(EuclideanPoint p, double scalar) {
        return new EuclideanPoint(p.getX() * scalar, p.getY() * scalar, p.name);
    }

    //Divide a complex number by another
    public void divide(EuclideanPoint other) {
        double divisor = (Math.pow(other.getX(), 2) + Math.pow(other.getY(), 2));
        double returnX = ((getX() * other.getX()) + (getY() * other.getY())) / divisor;
        double returnY = ((getY() * other.getX()) - (getX() * other.getY())) / divisor;
        setCartesian(returnX, returnY);
    }

    //Divide a complex number by another
    public static EuclideanPoint divide(EuclideanPoint p1, EuclideanPoint p2) {
        double divisor = Math.pow(p2.getX(), 2) + Math.pow(p2.getY(), 2);
        double returnX = ((p1.getX() * p2.getX()) + (p1.getY() * p2.getY())) / divisor;
        double returnY = ((p1.getY() * p2.getX()) - (p1.getX() * p2.getY())) / divisor;
        return new EuclideanPoint(returnX, returnY, p1.name);
    }

    public void inverse() {
        EuclideanPoint num, denum;
        num = new EuclideanPoint(this);
        denum = new EuclideanPoint(Math.pow(getX(), 2) - Math.pow(getY(), 2), 2 * getX() * getY());
        if (denum.getRadius() == 0d) {
            denum.setPolar(Math.pow(10, Double.MIN_VALUE), denum.getAngle());
        }
        set(divide(num, denum));
    }

    public static EuclideanPoint inverse(EuclideanPoint p) {
        EuclideanPoint returnPoint, num, denum;
        num = new EuclideanPoint(p);
        denum = new EuclideanPoint(Math.pow(p.getX(), 2) - Math.pow(p.getY(), 2), 2 * p.getX() * p.getY());
        if (denum.getRadius() == 0d) {
            denum.setPolar(Math.pow(10, Double.MIN_VALUE), denum.getAngle());
        }
        returnPoint = divide(num, denum);
        returnPoint.name = p.name;
        return returnPoint;
    }

    public static EuclideanPoint getMiddlePoint(EuclideanPoint p1, EuclideanPoint p2) {
        return new EuclideanPoint((p1.getX() + p2.getX()) /2d, (p1.getY()+p2.getY())/2d);
    }

    @Override
    public int hashCode() {
        return (int) Math.round(Math.toDegrees(getAngle()));
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EuclideanPoint) {
            EuclideanPoint p = (EuclideanPoint) obj;
            return (cartesian.equals(p.cartesian)) || (polar.equals(p.polar)) ;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return "EuclideanPoint " + name + " : [" + df.format(getX()) + " ; " + df.format(getY()) + "] // (" + df.format(getRadius()) + " ; " + df.format(Math.toDegrees(getAngle())) + "°)";
    }
}
