package hyperbolicbrowser.geometricmodels.geometricobjects.coordinates;

public class CartesianCoordinate extends Coordinate {
	private static final long serialVersionUID = 3362724051332095720L;
	protected double x, y;
	
	public CartesianCoordinate() {
		this(0, 0);
	}
	
	public CartesianCoordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public CartesianCoordinate(CartesianCoordinate copy) {
		x = copy.x;
		y = copy.y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public PolarCoordinate toPolar() {
		return new PolarCoordinate(Math.hypot(x, y), Math.atan2(y, x));
	}

	public void set(CartesianCoordinate cartesianCoordinate) {
		x = cartesianCoordinate.x;
		y = cartesianCoordinate.y;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CartesianCoordinate) {
			CartesianCoordinate c = (CartesianCoordinate) obj;
			return (x == c.x) && (y == c.y);
		} else {
			return false;
		}
	}
}
