package hyperbolicbrowser.geometricmodels.geometricobjects.coordinates;

public class PolarCoordinate extends Coordinate {
	private static final long serialVersionUID = -181394461903690658L;
	protected double radius, angle;
	
	public PolarCoordinate() {
		this(0d, 0d);
	}
	
	public PolarCoordinate(double radius, double angle) {
		if (radius >= 0) {
			this.radius = radius;
			setAngle(angle);
		} else {
			this.radius = -radius;
			setAngle(angle + Math.PI);
		}
	}

	public PolarCoordinate(PolarCoordinate copy) {
		radius = copy.radius;
		angle = copy.angle;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		double modulo2PI = angle % (Math.PI * 2d);
		if(modulo2PI < 0d) {
			modulo2PI = (angle % (Math.PI * 2d)) + (Math.PI * 2d);
		}
		this.angle = modulo2PI;
	}

	public void set(PolarCoordinate polarCoordinate) {
		radius = polarCoordinate.radius;
		angle = polarCoordinate.angle;
	}

	public CartesianCoordinate toCartesian() {
		double x, y;
		x = radius * Math.cos(angle);
		y = radius * Math.sin(angle);
		return new CartesianCoordinate(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PolarCoordinate) {
			PolarCoordinate c = (PolarCoordinate) obj;
			return (radius == c.radius) && (((angle - c.angle) % (Math.PI*2d)) == 0d);
		} else {
			return false;
		}
	}
}
