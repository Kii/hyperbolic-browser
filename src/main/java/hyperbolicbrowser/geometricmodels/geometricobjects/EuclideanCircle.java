/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels.geometricobjects;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableFigure;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincent Dolez (vincent.dolez@isen-lille.fr)
 */
public class EuclideanCircle extends PrintableCircle {
	private static final long serialVersionUID = 8885975644753156237L;

    public static final EuclideanCircle UNIT_CIRCLE = new EuclideanCircle("unit-circle");

	protected EuclideanPoint center;
    protected double radius;
    private static double errorAllowed = Math.pow(10d, -5d);

    public EuclideanCircle(String name) {
        this(1d, name);
    }

    public EuclideanCircle(double radius, String name) {
    	this(new EuclideanPoint("center-" + name), radius, name);
    }

    public EuclideanCircle(EuclideanPoint center, double radius) {
        this(center, radius, "");
    }

    public EuclideanCircle(EuclideanPoint center, double radius, String name) {
    	this(center, EuclideanPoint.sum(center, radius), name);
    }

    public EuclideanCircle(EuclideanPoint center, EuclideanPoint pointOnCircle, String name) {
    	super(center, pointOnCircle, name);
        this.center = center;
        this.radius = center.distance(pointOnCircle);
    }
    
    public static EuclideanCircle build(EuclideanPoint p1, EuclideanPoint p2, EuclideanPoint p3, String name) {
        EuclideanLine p1p3 = new EuclideanLine(p1, p3);
        EuclideanPoint milieuD1 = new EuclideanPoint((p1.getX() + p3.getX()) / 2d, (p1.getY() + p3.getY()) / 2d);
        EuclideanLine d1 = p1p3.orthogonal(milieuD1);

        EuclideanLine p2p3 = new EuclideanLine(p2, p3);
        EuclideanPoint milieuD2 = new EuclideanPoint((p2.getX() + p3.getX()) / 2d, (p2.getY() + p3.getY()) / 2d);
        EuclideanLine d2 = p2p3.orthogonal(milieuD2);
        
        EuclideanPoint p0 = d1.intersect(d2);

        return new EuclideanCircle(p0, p0.distance(p1), name);
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public EuclideanPoint getCenter() {
        return (EuclideanPoint) super.getCenter();
    }

    public void setRadius(double radius) {
        if (radius >= 0d) {
            this.radius = radius;
        }
    }
    
    public Rectangle getBoundingBox() {
        int x, y, width, height;
        x = (int) center.getX() - (int) radius;
        y = (int) center.getY() - (int) radius;
        width = (int) (radius * 2d);
        height = (int) (radius * 2d);
        return new Rectangle(x, y, width, height);
    }
    
    public List<EuclideanPoint> getIntersectionPoints(EuclideanCircle c) {
        List<EuclideanPoint> result = new ArrayList<>(2);
        double x0, y0, x1, y1;
        x0 = center.getX();
        y0 = center.getY();
        x1 = c.getCenter().getX();
        y1 = c.getCenter().getY();
        
        double dy = y1 - y0;
        double dx = x1 - x0;

        double distanceBetweenCenters = center.distance(c.center);
        
        if(distanceBetweenCenters > (radius + c.getRadius())) {
            return result;
        } else if(distanceBetweenCenters < Math.abs(radius - c.getRadius())) {
            return result;
        }
        
        double difference = (Math.pow(radius, 2d) - Math.pow(c.radius, 2d) + Math.pow(distanceBetweenCenters, 2d)) / (2d*distanceBetweenCenters);
        double x2, y2;
        x2 = x0 + (dx * (difference/distanceBetweenCenters));
        y2 = y0 + (dy * (difference/distanceBetweenCenters));
        
        double h = Math.sqrt(Math.pow(radius, 2d) - Math.pow(difference, 2d));
        double rx , ry;
        rx = -dy * (h/distanceBetweenCenters);
        ry =  dx * (h/distanceBetweenCenters);
        
        double xi, xi_prime, yi, yi_prime;
        xi = x2 + rx;
        xi_prime = x2 - rx;
        yi = y2 + ry;
        yi_prime = y2 - ry;

        EuclideanPoint firstResult = new EuclideanPoint(xi, yi, "xi");
        EuclideanPoint secondResult = new EuclideanPoint(xi_prime, yi_prime, "xi'");

        if(firstResult.distance(secondResult) < errorAllowed) {
            result.add(firstResult);
        } else {
            result.add(firstResult);
            result.add(secondResult);
        }
        
        return result;
    }

    public EuclideanPoint getPointOnCircle(double angle) {
    	return new EuclideanPoint(center.getX() + radius*Math.cos(angle), center.getY() + radius*Math.sin(angle), "p"+name+angle);
    }

    public boolean isOnCircle(EuclideanPoint p) {
        return (distanceFromPointToCircle(p, this) < errorAllowed);
    }

    public double distance(EuclideanPoint p) {
        return distanceFromPointToCircle(p, this);
    }

    public static double distanceFromPointToCircle(EuclideanPoint p, EuclideanCircle circle) {
        return (Math.abs(circle.center.distance(p) - circle.radius));
    }

    public double distance(EuclideanPoint point, double angle1, double angle2) {
        return distanceFromPointToEuclideanCircle(point, this, angle1, angle2, true);
    }

    public static double distanceFromPointToEuclideanCircle(EuclideanPoint point, EuclideanCircle circle, double angle1, double angle2, boolean bounded) {
        EuclideanPoint center = circle.getCenter();
        double distance = circle.distance(point);
        if (!bounded) {
            return distance;
        } else {
            double minAngle = Math.min(angle1, angle2);
            double maxAngle = Math.max(angle1, angle2);

            double angleClicked = EuclideanPoint.subtract(point, center).getAngle();
            double diffAngle = Math.abs(maxAngle - minAngle);

            if (diffAngle < Math.PI && angleClicked >= minAngle && angleClicked <= maxAngle) {
                return distance;
            } else if (diffAngle > Math.PI && (angleClicked < minAngle || angleClicked > maxAngle)) {
                return distance;
            } else {
                EuclideanPoint borne1 = circle.getPointOnCircle(angle1);
                EuclideanPoint borne2 = circle.getPointOnCircle(angle2);
                return Math.min(point.distance(borne1), point.distance(borne2));
            }
        }
    }

    public boolean isOnArc(EuclideanPoint p, double angleStart, double angleStop) {
        if((Math.abs(center.distance(p) - radius) > errorAllowed)) {
            return false;
        }
        EuclideanPoint diffCenter = EuclideanPoint.subtract(p, center);
        double angle = diffCenter.getAngle();
        if(angleStart > angleStop) {
            return (angle >= angleStart && angle <= angleStop);
        } else {
            return (angle >= angleStop && angle <= angleStart);
        }
    }
}
