/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels.geometricobjects.printables;

import java.awt.Color;
import java.awt.Shape;
import java.io.Serializable;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public abstract class PrintableObject implements Serializable {
    private static final long serialVersionUID = -5168164889101445013L;

    protected String name;

    protected boolean selected = false;

    protected boolean visible = true;

    protected boolean flyOver = false;
    
    protected Color color = Color.BLACK;
    
    protected static Color selectedColor = Color.RED;
    
    public PrintableObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSelected(boolean b) {
        selected = b;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setFlyOver(boolean b) {
        flyOver = b;
    }

    public boolean isFlyOver() {
        return flyOver;
    }

    public void printAllInfo() {
        System.out.println(toString());
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return name;
    }
}
