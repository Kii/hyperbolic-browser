/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hyperbolicbrowser.geometricmodels.geometricobjects.printables;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class PrintablePoint extends PrintableObject {
	private static final long serialVersionUID = -765995842644396244L;

	public PrintablePoint(String name){
        super(name);
    }
}
