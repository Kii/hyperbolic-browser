/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels.geometricobjects.printables;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author dolezv
 */
public abstract class PrintableFigure extends PrintableObject {

	private static final long serialVersionUID = 8239318087771766618L;
	
	protected List<PrintablePoint> listPoints = new LinkedList<>();

    public PrintableFigure(String name) {
        super(name);
    }

    public List<PrintablePoint> getListPoints() {
        return listPoints;
    }

    public void setListPoints(List<PrintablePoint> listPoints) {
        this.listPoints = listPoints;
    }

    public void addPoint(PrintablePoint p){
        if(p != null){
            listPoints.add(p);
        }
    }

    public void removePoint(PrintablePoint p){
        if(p != null){
            listPoints.remove(p);
        }
    }
}
