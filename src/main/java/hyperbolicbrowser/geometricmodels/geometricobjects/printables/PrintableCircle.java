package hyperbolicbrowser.geometricmodels.geometricobjects.printables;

public class PrintableCircle extends PrintableFigure {

	private static final long serialVersionUID = 3584096208130967078L;
	protected PrintablePoint center, pointOnCircle;
	
	public PrintableCircle(PrintablePoint center, PrintablePoint pointOnCircle, String name) {
		super(name);
		this.center = center;
		listPoints.add(center);
		this.pointOnCircle = pointOnCircle;
		listPoints.add(pointOnCircle);
	}

	public PrintablePoint getCenter() {
		return center;
	}

	public void setCenter(PrintablePoint center) {
		this.center = center;
	}

	public PrintablePoint getPointOnCircle() {
		return pointOnCircle;
	}

	public void setPointOnCircle(PrintablePoint pointOnCircle) {
		this.pointOnCircle = pointOnCircle;
	}
}
