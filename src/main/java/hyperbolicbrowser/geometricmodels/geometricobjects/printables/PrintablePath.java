package hyperbolicbrowser.geometricmodels.geometricobjects.printables;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class PrintablePath extends PrintableFigure {
    private static final long serialVersionUID = 1351192520530916391L;
    protected PrintablePoint p1;
    protected PrintablePoint p2;
    protected boolean bounded;

    public PrintablePath(PrintablePoint p1, PrintablePoint p2, String name) {
        super(name);
        this.p1 = p1;
        this.p2 = p2;
        listPoints.add(p1);
        listPoints.add(p2);
        bounded = false;
    }

    public PrintablePoint getP1() {
        return p1;
    }

    public PrintablePoint getP2() {
        return p2;
    }

    public boolean isBounded() {
        return bounded;
    }

    public void setBounded(boolean bounded) {
        this.bounded = bounded;
    }
    
    @Override
    public String toString() {
        return name + " : [P1 " + p1.getName() + "  / P2 " + p2.getName() + "]";
    }
}
