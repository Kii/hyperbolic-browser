package hyperbolicbrowser.geometricmodels.geometricobjects.points;

import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.HyperboloidCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.utils.mUtils;

import java.text.DecimalFormat;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class HyperboloidPoint extends PrintablePoint {
	private static final long serialVersionUID = -6110971066811293368L;
	protected HyperboloidCoordinate coords;
    protected DecimalFormat df = new DecimalFormat("####0.00");

    public HyperboloidPoint(String name, Double x, Double y, Double w) {
        this(name, x, y, Math.sqrt(Math.pow(x, 2d) + Math.pow(y, 2d) + 1d), w);
    }

    public HyperboloidPoint(String name, Double x, Double y, Double z, Double w) {
        super(name);
        coords = new HyperboloidCoordinate(x, y, z, w);
    }

    public HyperboloidPoint(String name, HyperboloidPoint other) {
        super(name);
        coords = new HyperboloidCoordinate(other.coords);
    }

    public void setCoords(HyperboloidPoint p) {
        coords.setCoordinates(p.getX(), p.getY(), p.getZ(), p.getW());
    }

    public void setCoords(double x, double y, double z, double w) {
    	coords.setCoordinates(x, y, z, w);
    }
    
    public Double getX() {
        return coords.getX();
    }

    public Double getY() {
        return coords.getY();
    }

    public Double getZ() {
        return coords.getZ();
    }

    public Double getW() {
        return coords.getW();
    }

    public void computeZ() {
    	double z = Math.sqrt(Math.pow(coords.getX(), 2d) + Math.pow(coords.getY(), 2d) + Math.pow(coords.getW(), 2d));
        coords.setZ(z);
    }

    public PoincareDiskPoint toPoincareDiskPoint() {
        double denum = getW() + getZ();
        return new PoincareDiskPoint(getX() / denum, getY() / denum, getName());
    }

    public BeltramiDiskPoint toBeltramiDiskPoint() {
        return new BeltramiDiskPoint(getX() / getZ(), getY() / getZ(), getName());
    }

    public PoincareHalfPlanePoint toPoincareHalfPlanePoint() {
        return new PoincareHalfPlanePoint(mUtils.diskToHalfPlane(toPoincareDiskPoint()));
    }

    @Override
    public String toString() {
        return name + " : [" + df.format(getX()) + " | " + df.format(getY()) + " | " + df.format(getZ()) + " | " + df.format(getW()) + "]";
    }
}
