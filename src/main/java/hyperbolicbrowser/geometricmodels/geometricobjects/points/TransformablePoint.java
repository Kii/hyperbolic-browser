package hyperbolicbrowser.geometricmodels.geometricobjects.points;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.CartesianCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;

import java.util.List;

public abstract class TransformablePoint extends EuclideanPoint {
    public TransformablePoint(String name) {
        super(name);
    }

    public TransformablePoint(double x, double y) {
        super(x, y);
    }

    public TransformablePoint(double x, double y, String name) {
        super(x, y, name);
    }

    public TransformablePoint(EuclideanPoint copy) {
        super(copy);
    }

    public TransformablePoint(CartesianCoordinate cartesianCoordinate) {
        super(cartesianCoordinate);
    }

    public TransformablePoint(CartesianCoordinate cartesianCoordinate, String name) {
        super(cartesianCoordinate, name);
    }

    public TransformablePoint(PolarCoordinate polarCoordinate) {
        super(polarCoordinate);
    }

    public TransformablePoint(PolarCoordinate polarCoordinate, String name) {
        super(polarCoordinate, name);
    }

    public abstract void rotate(double angleRotation);
    public abstract void translate(double distanceEucl);
    public abstract void translate(EuclideanPoint translationPoint);
    public abstract HyperboloidPoint toHyperboloid();
    public abstract double distance(TransformablePoint p);
    public abstract TransformablePoint getMiddlePoint(TransformablePoint p);

    public static void rotate(double angleRotation, List<TransformablePoint> points) {
        if (angleRotation % (Math.PI * 2) != 0) {
            for (TransformablePoint tmpPoint : points) {
                tmpPoint.rotate(angleRotation);
            }
        }
    }

    public static void translate(double distance, List<TransformablePoint> points) {
        for (TransformablePoint tmpPoint : points) {
            tmpPoint.translate(distance);
        }
    }

    public static void translate(EuclideanPoint translationPoint, List<TransformablePoint> points) {
        for (TransformablePoint tmpPoint : points) {
            tmpPoint.translate(translationPoint);
        }
    }
}
