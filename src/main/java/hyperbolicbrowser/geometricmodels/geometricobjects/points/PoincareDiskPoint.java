package hyperbolicbrowser.geometricmodels.geometricobjects.points;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.CartesianCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.utils.mUtils;


public class PoincareDiskPoint extends TransformablePoint {
    public PoincareDiskPoint(String name) {
        super(name);
    }

    public PoincareDiskPoint(double x, double y) {
        super(x, y);
    }

    public PoincareDiskPoint(double x, double y, String name) {
        super(x, y, name);
    }

    public PoincareDiskPoint(EuclideanPoint copy) {
        super(copy.getCartesian(), copy.getName());
    }

    public PoincareDiskPoint(CartesianCoordinate cartesian) {
        super(cartesian);
    }

    public PoincareDiskPoint(CartesianCoordinate cartesian, String name) {
        super(cartesian, name);
    }

    public PoincareDiskPoint(PolarCoordinate polar) {
        super(polar);
    }

    public PoincareDiskPoint(PolarCoordinate polar, String name) {
        super(polar, name);
    }

    @Override
    public HyperboloidPoint toHyperboloid() {
        Double x = 2d * getX();
        Double y = 2d * getY();
        Double z = 1d + Math.pow(getX(), 2d) + Math.pow(getY(), 2d);
        Double w = 1d - (Math.pow(getX(), 2d) + Math.pow(getY(), 2d));

        return new HyperboloidPoint(getName(), x / w, y / w, z / w, w / w);
    }

    @Override
    public void rotate(double angleRotation) {
        setPolar(getRadius(), getAngle() + (angleRotation%(2d*Math.PI)));
    }

    @Override
    public void translate(double distanceEucl) {
        //Fonction "oeil de chat" möbius
        //f(p, dist) = (p - dist) / (1 - dist*p)
        EuclideanPoint denum = EuclideanPoint.multiply(this, distanceEucl);
        denum.sum(1d);

        sum(distanceEucl);
        divide(denum);
    }

    @Override
    public void translate(EuclideanPoint translationPoint) {
        rotate(-translationPoint.getAngle());
        translate(-translationPoint.getRadius());
        rotate(translationPoint.getAngle());
    }

    @Override
    public double distance(TransformablePoint p) {
        EuclideanPoint antiP2 = EuclideanPoint.multiply(p, -1d);

        EuclideanPoint subtract = EuclideanPoint.sum(this, antiP2);
        double num = subtract.getRadius();
        num = Math.pow(num, 2d) * 2d;

        double denum1 = 1d - Math.pow(getRadius(), 2d);
        double denum2 = 1d - Math.pow(p.getRadius(), 2d);

        double fraction = num / (denum1 * denum2);

        fraction = fraction + 1d;

        return mUtils.argcosh(fraction);
    }

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p) {
        p.rotate(-getAngle());
        p.translate(-getRadius());

        //Get the second point
        double secondAngle = p.getAngle();
        double secondEuclideanRadius = p.getRadius();
        double distanceHyperbolic = hyperbolicDistanceFromDiskCenter(secondEuclideanRadius);

        double halfDist = euclideanDistanceFromDiskCenter(distanceHyperbolic / 2d);

        PoincareDiskPoint middlePoint = new PoincareDiskPoint(new PolarCoordinate(halfDist, secondAngle));

        middlePoint.translate(getRadius());
        middlePoint.rotate(getAngle());

        p.translate(getRadius());
        p.rotate(getAngle());

        return middlePoint;
    }

    public static double euclideanDistanceFromDiskCenter(double distanceHyperbolic) {
        return Math.tanh(distanceHyperbolic / 2d);
    }

    public static double hyperbolicDistanceFromDiskCenter(double euclideanRadius) {
        return mUtils.argtanh(euclideanRadius) * 2d;
    }
}
