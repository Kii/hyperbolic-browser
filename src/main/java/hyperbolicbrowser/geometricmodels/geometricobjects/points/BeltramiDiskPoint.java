package hyperbolicbrowser.geometricmodels.geometricobjects.points;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanLine;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.CartesianCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.utils.mUtils;

import java.util.List;



public class BeltramiDiskPoint extends TransformablePoint {
    public BeltramiDiskPoint(String name) {
        super(name);
    }

    public BeltramiDiskPoint(double x, double y) {
        super(x, y);
    }

    public BeltramiDiskPoint(double x, double y, String name) {
        super(x, y, name);
    }

    public BeltramiDiskPoint(EuclideanPoint copy) {
        super(copy.getCartesian(), copy.getName());
    }

    public BeltramiDiskPoint(CartesianCoordinate cartesian) {
        super(cartesian);
    }

    public BeltramiDiskPoint(CartesianCoordinate cartesian, String name) {
        super(cartesian, name);
    }

    public BeltramiDiskPoint(PolarCoordinate polar) {
        super(polar);
    }

    public BeltramiDiskPoint(PolarCoordinate polar, String name) {
        super(polar, name);
    }

    @Override
    public HyperboloidPoint toHyperboloid() {
        Double x = getX();
        Double y = getY();
        Double z = 1d;
        Double w = Math.sqrt(1d - (Math.pow(x, 2d) + Math.pow(y, 2d)));

        return new HyperboloidPoint(getName(), x / w, y / w, z / w, w / w);
    }

    @Override
    public void rotate(double angleRotation) {
        setPolar(getRadius(), getAngle() + (angleRotation%(2d*Math.PI)));
    }

    @Override
    public void translate(double euclideanDistance) {
        double hypDist = hyperbolicDistanceFromDiskCenter(euclideanDistance);
        double distPd = PoincareDiskPoint.euclideanDistanceFromDiskCenter(hypDist);

        PoincareDiskPoint pdp = toHyperboloid().toPoincareDiskPoint();
        pdp.translate(distPd);

        this.set(pdp.toHyperboloid().toBeltramiDiskPoint());
    }

    @Override
    public void translate(EuclideanPoint p) {
        rotate(-p.getAngle());
        translate(-p.getRadius());
        rotate(p.getAngle());
    }

    @Override
    public double distance(TransformablePoint p) {
        EuclideanLine pqLine = new EuclideanLine(this, p);
        EuclideanCircle abstractCircle = new EuclideanCircle("Beltrami-klein disk");

        List<EuclideanPoint> interCircles = pqLine.intersect(abstractCircle);
        if (interCircles.size() == 2) {
            EuclideanPoint a = interCircles.get(1);
            EuclideanPoint b = interCircles.get(0);
            double aq = a.distance(p);
            double ap = a.distance(this);
            double bq = b.distance(p);
            double bp = b.distance(this);
            double distance;

            if (aq >= ap && bp >= bq) {
                distance = Math.log((aq * bp) / (ap * bq));
            } else {
                distance = Math.log((ap * bq) / (aq * bp));
            }
            return distance / 2d;
        } else {
            return Double.MAX_VALUE;
        }
    }

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p) {
        //TODO implement method in its own domain
        TransformablePoint self = toHyperboloid().toPoincareDiskPoint();
        TransformablePoint other = p.toHyperboloid().toPoincareDiskPoint();
        TransformablePoint middle = self.getMiddlePoint(other);

        return middle.toHyperboloid().toBeltramiDiskPoint();
    }

    public static double euclideanDistanceFromDiskCenter(double distanceHyperbolic) {
        return Math.tanh(distanceHyperbolic);
    }

    public static double hyperbolicDistanceFromDiskCenter(double euclideanRadius) {
        return mUtils.argtanh(euclideanRadius);
    }
}
