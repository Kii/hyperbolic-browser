package hyperbolicbrowser.geometricmodels.geometricobjects.points;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.CartesianCoordinate;
import hyperbolicbrowser.utils.mUtils;

import static hyperbolicbrowser.utils.mUtils.argcosh;

public class PoincareHalfPlanePoint extends TransformablePoint {
    public PoincareHalfPlanePoint(String name) {
        super(name);
    }

    public PoincareHalfPlanePoint(double x, double y) {
        super(x, y);
    }

    public PoincareHalfPlanePoint(double x, double y, String name) {
        super(x, y, name);
    }

    public PoincareHalfPlanePoint(EuclideanPoint copy) {
        super(copy);
    }

    public PoincareHalfPlanePoint(CartesianCoordinate cartesianCoordinate, String name) {
        super(cartesianCoordinate, name);
    }

    @Override
    public void rotate(double angle) {
        EuclideanPoint numerator, denominator;

        numerator = EuclideanPoint.multiply(this, Math.cos(angle));
        numerator.sum(Math.sin(angle));

        denominator = EuclideanPoint.multiply(this, -Math.sin(angle));
        denominator.sum(Math.cos(angle));

        numerator.divide(denominator);
        set(numerator);
    }

    @Override
    public void translate(double distanceEucl) {
        throw new UnsupportedOperationException("translate(double) is not implemented in PoincareHalfPlanePoint");
    }

    @Override
    public void translate(EuclideanPoint translationPoint) {
        sum(-1d * translationPoint.getX());
        multiply(1d / translationPoint.getY());
    }

    @Override
    public HyperboloidPoint toHyperboloid() {
        return mUtils.halfPlaneToDisk(this).toHyperboloid();
    }

    @Override
    public double distance(TransformablePoint p) {
        EuclideanPoint antiP1 = EuclideanPoint.multiply(this, -1d);
        EuclideanPoint subtract = EuclideanPoint.sum(p, antiP1);
        double num1 = subtract.getX();
        num1 = Math.pow(num1, 2d);
        double num2 = subtract.getY();
        num2 = Math.pow(num2, 2d);

        double denum = 2d * getY() * p.getY();

        double fraction = ((num1 + num2) / denum) + 1d;

        return argcosh(fraction);
    }

    @Override
    public TransformablePoint getMiddlePoint(TransformablePoint p) {
        //TODO implement method in its own domain
        TransformablePoint self = toHyperboloid().toPoincareDiskPoint();
        TransformablePoint other = p.toHyperboloid().toPoincareDiskPoint();
        TransformablePoint middle = self.getMiddlePoint(other);

        return middle.toHyperboloid().toPoincareHalfPlanePoint();
    }
}
