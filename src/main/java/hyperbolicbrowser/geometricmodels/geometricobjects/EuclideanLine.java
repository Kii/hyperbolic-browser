/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels.geometricobjects;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincent Dolez (vincent.dolez@isen-lille.fr)
 */
public class EuclideanLine extends PrintablePath {
    private static final long serialVersionUID = 7702577553891035063L;
    private double a, b, c;
    private static double margeAllowed = Math.pow(10d, -5d);

    public EuclideanLine(EuclideanPoint p1, EuclideanPoint p2) {
        this(p1, p2, "");
    }

    public EuclideanLine(EuclideanPoint p1, EuclideanPoint p2, String name) {
        super(p1, p2, name);
        if (p1.equals(p2)) {
            throw new RuntimeException("Cannot instantiate line with 2 identical points");
        }

        a = p1.getY() - p2.getY();
        b = p2.getX() - p1.getX();
        c = p1.getX() * p2.getY() - p2.getX() * p1.getY();

        if (a == 0d && b == 0d) {
            throw new RuntimeException("Cannot instantiate line a == 0 AND b == 0");
        }
    }

    public static EuclideanLine build(double a, double b, double c, String name) {
        if (a == 0d && b == 0d) {
            throw new RuntimeException("Cannot instantiate line a == 0 AND b == 0");
        }
        // aX + bY + c = 0
        // Y = (-a/b) x + (-c/b)
        if(Math.abs(b) < margeAllowed) { // vertical line --> X = -c / a
            EuclideanPoint p1 = new EuclideanPoint(-c/a, 0d);
            EuclideanPoint p2 = new EuclideanPoint(-c/a, 1d);
            return new EuclideanLine(p1, p2, name);
        } else {
            EuclideanPoint p1 = new EuclideanPoint(0d, -c/b);
            EuclideanPoint p2 = new EuclideanPoint(1d, (-a/b) + (-c/b));
            return new EuclideanLine(p1, p2, name);
        }
    }

    public static EuclideanLine build(double coef, double absOrigin, String name) {
        if (coef == Double.MAX_VALUE || coef == -Double.MAX_VALUE) {
            throw new RuntimeException("Cannot instantiate line with infinite coef yet");
        }

        EuclideanPoint p1 = new EuclideanPoint(0d, absOrigin);
        EuclideanPoint p2 = new EuclideanPoint(1d, coef + absOrigin);
        return new EuclideanLine(p1, p2, name);
    }

    public double getImage(double x) {
        if (b != 0d) { //
            return (-a / b) * x - (c / b);
        } else {
            throw new RuntimeException("Line is vertical and does not allow to find y from x");
        }
    }

    public double getCore(double y) {
        if (a != 0d) {
            return (-b / a) * y - (c / a);
        } else {
            throw new RuntimeException("Line is horizontal and does not allow to find x from y");
        }
    }

    //ax + by + c = 0
    //y = (-a/b) x + (-c/b)
    //x = (-b/a) y + (-c/a)
    //c = -ax - by
    public EuclideanLine orthogonal(EuclideanPoint p) {
        double x0 = p.getX();
        double y0 = p.getY();
        double orthoA, orthoB, orthoC;

        if (isVertical()) { // vertical line (for every y, x = -c/a)
            // we need an horizontal line going through p
            // y is constant
            orthoA = 0d;
            orthoB = 1d;
            orthoC = -y0;
            return EuclideanLine.build(orthoA, orthoB, orthoC, "");
        } else if (isHorizontal()) { //horizontal line (for every x, y = -c/b)
            //we need a vertical line going through p
            orthoA = 1d;
            orthoB = 0d;
            orthoC = -x0;
            return EuclideanLine.build(orthoA, orthoB, orthoC, "");
        } else {
            //y = (-a/b) x + (-c/b)
            double coef = (-a / b);

            double orthoCoef = -1d / coef;
            double orthoAbsOrigin = -orthoCoef * x0 + y0;

            return EuclideanLine.build(orthoCoef, orthoAbsOrigin, "");
        }
    }

    public List<EuclideanPoint> intersect(EuclideanCircle circle) {
        EuclideanPoint p1, p2;
        List<EuclideanPoint> solutions = new ArrayList<>();
        EuclideanLine ortho = orthogonal(circle.getCenter());
        EuclideanPoint intersection = intersect(ortho);
        double distanceFromCenterToLine = circle.getCenter().distance(intersection);

        if(distanceFromCenterToLine > circle.getRadius()) {
            return solutions;
        } else if(Math.abs(distanceFromCenterToLine - circle.getRadius()) < margeAllowed) {
            solutions.add(intersection);
            return solutions;
        }

        if (!isVertical()) {
            double coef = (-a / b);
            double absOrigin = (-c / b);

            if (Math.abs(coef) > margeAllowed && Math.abs(absOrigin) > margeAllowed) {
                p1 = new EuclideanPoint(-absOrigin / coef, 0d, "");
            } else {
                p1 = new EuclideanPoint(1d, coef + absOrigin, "");
            }
            p2 = new EuclideanPoint(0d, absOrigin, "");

            double a, b, c, k, k1, k2;
            a = Math.pow((p2.getX() - p1.getX()), 2d) + Math.pow((p2.getY() - p1.getY()), 2d);
            b = 2d * (((p2.getX() - p1.getX()) * (p1.getX() - circle.getCenter().getX())) + ((p2.getY() - p1.getY()) * (p1.getY() - circle.getCenter().getY())));
            c = Math.pow(circle.getCenter().getX(), 2d) + Math.pow(circle.getCenter().getY(), 2d) + Math.pow(p1.getX(), 2d) + Math.pow(p1.getY(), 2d) - 2d * (circle.getCenter().getX() * p1.getX() + circle.getCenter().getY() * p1.getY()) - Math.pow(circle.getRadius(), 2d);

            double delta = Math.pow(b, 2d) - (4d * a * c);

            if (delta < 0d) {
                System.out.println("delta < 0");
            } else if (delta == 0d) {
                k = -b / (2d * a);
                solutions.add(new EuclideanPoint(p1.getX() + k * (p2.getX() - p1.getX()), p1.getY() + k * (p2.getY() - p1.getY()), "interCircle"));
                return solutions;
            } else {
                k1 = (-b - Math.sqrt(delta)) / (2d * a);
                k2 = (-b + Math.sqrt(delta)) / (2d * a);

                solutions.add(new EuclideanPoint(p1.getX() + k1 * (p2.getX() - p1.getX()), p1.getY() + k1 * (p2.getY() - p1.getY()), "interCircle1"));
                solutions.add(new EuclideanPoint(p1.getX() + k2 * (p2.getX() - p1.getX()), p1.getY() + k2 * (p2.getY() - p1.getY()), "interCircle2"));
                return solutions;
            }
        } else {
            double x = -c / a;
            double bd, cd;
            bd = -2d * circle.getCenter().getY();
            cd = Math.pow(circle.getCenter().getY(), 2d) - Math.pow(circle.getRadius(), 2d) + Math.pow(x - circle.getCenter().getX(), 2d);
            double delta = Math.pow(bd, 2d) - 4d * cd;
            if (delta > 0d) {
                double y1, y2;
                y1 = (-bd - Math.sqrt(delta)) / 2d;
                solutions.add(new EuclideanPoint(x, y1));
                y2 = (-bd + Math.sqrt(delta)) / 2d;
                solutions.add(new EuclideanPoint(x, y2));
            } else if (delta == 0d) {
                solutions.add(new EuclideanPoint(x, -bd/2d));
            }
        }
        return solutions;
    }

    public EuclideanPoint intersect(EuclideanLine line) {
        //y = mx + p = m1x + p1
        //  (m - m1)x = (p1 - p)
        //  x = (p1 - p) / (m - m1)
        
        if((isVertical() && line.isVertical()) || (isHorizontal() && line.isHorizontal())) {
            return null;
        } else {
            if(isVertical()) { //x is constant
                double x = getCore(0d);
                double y = line.getImage(x);
                return new EuclideanPoint(x, y);
            } else if (line.isVertical()) { //y is constant
                double x = line.getCore(0d);
                double y = getImage(x);
                return new EuclideanPoint(x, y);
            } else {
                double m, p, lm, lp;
                m = (-a/b);
                p = (-c/b);
                lm = (-line.a / line.b);
                lp = (-line.c / line.b);

                if (Math.abs(m - lm) > margeAllowed) {
                    double x, y;
                    x = (lp - p) / (m - lm);
                    y = (m * (lp - p) / (m - lm)) + p;
                    return new EuclideanPoint(x, y);
                } else {
                    return null;
                }
            }
        }
    }

    public boolean isVertical() {
        return (Math.abs(b) <= margeAllowed);
    }

    public boolean isHorizontal() {
        return (Math.abs(a) <= margeAllowed);
    }

    public boolean increase() {
    	if(!isVertical() && !isHorizontal()) {
    		return (Math.signum(-b / a) > 0d);
    	} else {
    		return false;
    	}
    }
    
    public boolean decrease() {
    	if(!isVertical() && !isHorizontal()) {
    		return (Math.signum(-b / a) < 0d);
    	} else {
    		return false;
    	}
    }

    public boolean containsPoint(EuclideanPoint p) {
        //double margeAllowed = 0d;
        if(isVertical()) {
            //return (p.getX() == getCore(p.getY()));
            return Math.abs(p.getX() - getCore(p.getY())) <= margeAllowed;
        } else {
            return Math.abs(p.getY() - getImage(p.getX())) <= margeAllowed;
        }
    }

    public static double distanceFromPointToEuclideanSegment(EuclideanPoint point, EuclideanLine line, EuclideanPoint p1Borne, EuclideanPoint p2Borne) {
        EuclideanLine ortho = line.orthogonal(point);
        EuclideanPoint intersection = line.intersect(ortho);
        double distance = point.distance(intersection);
        boolean pointInSegment = false;
        if (line.increase()) {
            pointInSegment = (line.getImage(intersection.getX()) >= line.getImage(p1Borne.getX()) && line.getImage(intersection.getX()) <= line.getImage(p2Borne.getX())) || (line.getImage(intersection.getX()) <= line.getImage(p1Borne.getX()) && line.getImage(intersection.getX()) > line.getImage(p2Borne.getX()));
        } else if (line.decrease()) {
            pointInSegment = (line.getImage(intersection.getX()) <= line.getImage(p1Borne.getX()) && line.getImage(intersection.getX()) >= line.getImage(p2Borne.getX())) || (line.getImage(intersection.getX()) >= line.getImage(p1Borne.getX()) && line.getImage(intersection.getX()) < line.getImage(p2Borne.getX()));
        } else if (line.isVertical()) {
            pointInSegment = (intersection.getY() >= p1Borne.getY() && intersection.getY() <= p2Borne.getY()) || (intersection.getY() <= p1Borne.getY() && intersection.getY() >= p2Borne.getY());
        } else if (line.isHorizontal()) {
            pointInSegment = (intersection.getX() >= p1Borne.getX() && intersection.getX() <= p2Borne.getX()) || (intersection.getX() <= p1Borne.getX() && intersection.getX() >= p2Borne.getX());
        }
        if (pointInSegment) {
            return distance;
        } else {
            return Math.min(point.distance(p1Borne), point.distance(p2Borne));
        }
    }

    public double distance(EuclideanPoint point, EuclideanPoint p1Borne, EuclideanPoint p2Borne) {
        return distanceFromPointToEuclideanSegment(point, this, p1Borne, p2Borne);
    }

    public static double distanceFromPointToEuclideanLine(EuclideanPoint point, EuclideanLine line) {
        EuclideanLine ortho = line.orthogonal(point);
        EuclideanPoint intersection = line.intersect(ortho);
        return point.distance(intersection);
    }

    public double distance(EuclideanPoint p) {
        return distanceFromPointToEuclideanLine(p, this);
    }

    public EuclideanPoint getPointAtDistance(EuclideanPoint p, double dist) {
        //ax + by + c = 0
        //y = (-c - ax) / b <==> y = (-a/b)*x - c/b
        //x = (-by - c) / a <==> x = (-b/a)*y - c/a
        if(containsPoint(p)) {
            if(dist == 0d) {
                return new EuclideanPoint(p);
            }
            if(isHorizontal()) {
                return new EuclideanPoint(p.getX() + dist, p.getY());
            } else if (isVertical()) {
                return new EuclideanPoint(p.getX(), p.getY() + dist);
            } else {
                double dx = dist; //(dx0 + dist) - dx0
                double dy = getImage(p.getX() + dist) - p.getY(); //(dy1 - dy0)

                EuclideanPoint diffPoint = new EuclideanPoint(dx, dy);

                //normalize point
                diffPoint.multiply(1d/diffPoint.getRadius());

                //good distance point
                diffPoint.multiply(Math.abs(dist));

                return EuclideanPoint.sum(p, diffPoint);
            }
        } else {
            throw new RuntimeException("Point p is not on line");
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a).append("x + ").append(b).append("y + ").append(c).append(" = 0");
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        boolean equal = false;
        if (object instanceof EuclideanLine) {
            EuclideanLine line = (EuclideanLine) object;
            if (a != 0d && b != 0d && c != 0d) {
                equal = line.a / a == line.b / b && line.c / c == line.a / a;
            } else if (a == 0 && b != 0 && c != 0) {
                equal = line.a == a && line.c / c == line.a / a;
            } else if (b == 0 && a != 0 && c != 0) {
                equal = line.b == b && line.c / c == line.a / a;
            } else if (c == 0 && a != 0 && b != 0) {
                equal = line.c == c && line.b / b == line.a / a;
            } else if (a == 0 && c == 0 && b != 0) {
                equal = true;
            } else if (b == 0 && c == 0 && a != 0) {
                equal = true;
            }
        }
        return equal;
    }
}
