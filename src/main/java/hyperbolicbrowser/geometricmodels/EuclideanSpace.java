/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class EuclideanSpace extends GeometricSpace {
	private static final long serialVersionUID = -2202098480271793604L;

	public EuclideanSpace() {
        super();
    }

    @Override
    public Map<EuclideanPoint, Double> getClosestPoint(EuclideanPoint p) {
        EuclideanPoint closest;
        Map<EuclideanPoint, Double> returnMap = new HashMap<>(1);
        if (points.isEmpty()) {
            return null;
        } else {
            closest = (EuclideanPoint) points.get(0);
            double previousDist = Double.MAX_VALUE;
            for (PrintablePoint tmp : points) {
                if (tmp instanceof EuclideanPoint) {
                    EuclideanPoint euclideanPoint = (EuclideanPoint) tmp;
                    double dist = euclideanPoint.distance(p);
                    if (previousDist > dist) {
                        closest = (EuclideanPoint) tmp;
                        previousDist = dist;
                    }
                }
            }
            returnMap.put(closest, previousDist);
            return returnMap;
        }
    }
}
