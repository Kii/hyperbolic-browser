/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.geometricmodels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.*;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.listeners.GeometricSpaceListener;
import hyperbolicbrowser.toolbar.infosToolbar.CircleListViewer;
import hyperbolicbrowser.toolbar.infosToolbar.LineListViewer;
import hyperbolicbrowser.toolbar.infosToolbar.PointListViewer;
import hyperbolicbrowser.toolbar.infosToolbar.SegmentListViewer;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public abstract class GeometricSpace implements Serializable, ListSelectionListener {
    private static final long serialVersionUID = 3946613863833463816L;

    protected List<PrintablePoint> points;
    protected List<PrintablePath> lines;
    protected List<PrintablePath> segments;
    protected List<PrintableCircle> circles;
    protected transient List<GeometricSpaceListener> geometricSpaceListeners;

    public GeometricSpace() {
        points = new ArrayList<>();
        lines = new ArrayList<>();
        segments = new ArrayList<>();
        circles = new ArrayList<>();
        geometricSpaceListeners = new ArrayList<>();
    }

    /**
     * Used to know if a point was clicked
     *
     * @param p Euclidean coordinates
     * @return Euclidean coordinates of the closest point
     */
    public abstract Map<EuclideanPoint, Double> getClosestPoint(EuclideanPoint p);

    public void modifyObject(String oldName, PrintableObject modificationObject) {
        PrintableObject oldObject = getObjectByName(oldName);
        if(oldObject == null) {
            if(!oldName.equals(modificationObject.getName())) {
                fireObjectChanged(oldName, modificationObject);
            }
        } else {
            oldObject.setName(modificationObject.getName());
            oldObject.setColor(modificationObject.getColor());
            fireObjectChanged(oldName, modificationObject);
        }
    }

    public PrintableObject getObjectByName(String s) {
        PrintableObject o = getPointByName(s);
        if(o != null) {
            return o;
        }
        
        o = getLineByName(s);
        if(o != null) {
            return o;
        }
        
        o = getSegmentByName(s);
        if(o != null) {
            return o;
        }
        
        o = getCircleByName(s);
        if(o != null) {
            return o;
        }
        return null;
    }
    
    public PrintablePoint getPointByName(String s) {
        for (PrintablePoint gp : points) {
            if (gp.getName().equals(s)) {
                return gp;
            }
        }
        return null;
    }
    
    public PrintablePath getLineByName(String s) {
        for (PrintablePath gl : lines) {
            if (gl.getName().equals(s)) {
                return gl;
            }
        }
        return null;
    }

    public PrintablePath getSegmentByName(String s) {
        for (PrintablePath gl : segments) {
            if (gl.getName().equals(s)) {
                return gl;
            }
        }
        return null;
    }

    public PrintableCircle getCircleByName(String s) {
        for (PrintableCircle gl : circles) {
            if (gl.getName().equals(s)) {
                return gl;
            }
        }
        return null;
    }

    public List<PrintablePoint> getPoints() {
        return points;
    }

    public List<PrintablePoint> getSelectedPoints() {
        List<PrintablePoint> selectedPoints = new LinkedList<>();
        for(PrintablePoint p : points) {
            if(p.isSelected()) {
                selectedPoints.add(p);
            }
        }
        return selectedPoints;
    }

    public List<PrintablePath> getLines() {
        return lines;
    }

    public List<PrintablePath> getSelectedLines() {
        List<PrintablePath> selectedLines = new LinkedList<>();
        for(PrintablePath l : lines) {
            if(l.isSelected()) {
                selectedLines.add(l);
            }
        }
        return selectedLines;
    }

    public List<PrintablePath> getSegments() {
        return segments;
    }

    public List<PrintablePath> getSelectedSegments() {
        List<PrintablePath> selectedSegments = new LinkedList<>();
        for(PrintablePath s : segments) {
            if(s.isSelected()) {
                selectedSegments.add(s);
            }
        }
        return selectedSegments;
    }

    public List<PrintableCircle> getCircles() {
        return circles;
    }

    public List<PrintableCircle> getSelectedCircles() {
        List<PrintableCircle> selectedCircles = new ArrayList<>();
        for(PrintableCircle c : circles) {
            if(c.isSelected()) {
                selectedCircles.add(c);
            }
        }
        return selectedCircles;
    }

    public void addPoint(PrintablePoint p) {
        if (p != null) {
            for (PrintablePoint gp : points) {
                if (gp.getName().equals(p.getName())) {
                    // A point with the same name was found
                    return;
                }
            }
            points.add(p);
            fireNewPointEvent(p);
        }
    }

    public void removePoint(PrintablePoint p) {
        if (p != null) {
            removeLinesFromPoint(p);
            removeSegmentsFromPoint(p);
            removeCirclesFromPoint(p);
            points.remove(p);
            firePointRemovedEvent(p);
        }
    }

    public void selectPoint(PrintablePoint p, boolean selected) {
        p.setSelected(selected);
        firePointSelectionEvent(p);
    }

    public void selectPoint(PrintablePoint p) {
        p.setSelected(!p.isSelected());
        firePointSelectionEvent(p);
    }

    public void flyOverPoint(PrintablePoint p, boolean b) {
        p.setFlyOver(b);
        fireFlyOverPointEvent(p);
    }

    public void flyOverSegment(PrintablePath s, boolean b) {
        s.setFlyOver(b);
        fireFlyOverSegmentEvent(s);
    }

    public void flyOverLine(PrintablePath s, boolean b) {
        s.setFlyOver(b);
        fireFlyOverLineEvent(s);
    }

    public void flyOverCircle(PrintableCircle c, boolean b) {
        c.setFlyOver(b);
        fireFlyOverCircleEvent(c);
    }

    public void clearFlyOver() {
        for(PrintablePoint p : points) {
            flyOverPoint(p, false);
        }
        for(PrintablePath s : segments) {
            flyOverSegment(s, false);
        }
        for(PrintablePath l :lines) {
            flyOverLine(l, false);
        }
        for(PrintableCircle c : circles) {
            flyOverCircle(c, false);
        }
    }

    public void clearSelectedObjects() {
        for(PrintablePoint p : points) {
            selectPoint(p, false);
        }
        for(PrintablePath s : segments) {
            selectSegment(s, false);
        }
        for(PrintablePath l : lines) {
            selectLine(l, false);
        }
        for(PrintableCircle c : circles) {
            selectCircle(c, false);
        }
    }

    public void addLine(PrintablePath l) {
        if (l != null) {
            if (lines == null) {
                lines = new ArrayList<>();
            }
            if (!lines.isEmpty()) {
                for (PrintablePath gl : lines) {
                    if (gl.getName().equals(l.getName())
                            || (gl.getP1().getName().equals(l.getP1().getName())
                            && gl.getP2().getName().equals(l.getP2().getName()))
                            || (gl.getP1().getName().equals(l.getP2().getName())
                            && gl.getP2().getName().equals(l.getP1().getName()))) {
						// A line with the same name or the same points was found
                        return;
                    }
                }
            }
            lines.add(l);
            fireNewLineEvent(l);
        }
    }

    public void removeLine(PrintablePath l) {
        if (lines.contains(l)) {
            lines.remove(l);
            fireLineRemovedEvent(l);
        }
    }

    private void removeLinesFromPoint(PrintablePoint p) {
        if (p != null) {
            ArrayList<PrintablePath> lines2 = new ArrayList<>(lines.size());
            lines2.addAll(lines);
            for (PrintablePath l : lines2) {
                if (l.getP1() == p || l.getP2() == p) {
                    lines.remove(l);
                }
            }
        }
    }

    public void selectLine(PrintablePath l, boolean selected) {
        l.setSelected(selected);
        fireLineSelectionEvent(l);
    }

    public void selectLine(PrintablePath l) {
        l.setSelected(!l.isSelected());
        fireLineSelectionEvent(l);
    }

    public void addSegment(PrintablePath l) {
        if (l != null) {
            for (PrintablePath gl : lines) {
                if (gl.getName().equals(l.getName())
                        || (gl.getP1().getName().equals(l.getP1().getName())
                        && gl.getP2().getName().equals(l.getP2().getName()))
                        || (gl.getP1().getName().equals(l.getP2().getName())
                        && gl.getP2().getName().equals(l.getP1().getName()))) {
					// a segment with the same name or the same points was found
                    return;
                }
            }
            segments.add(l);
            fireNewSegmentEvent(l);
        }
    }

    public void removeSegment(PrintablePath l) {
        if (segments.contains(l)) {
            segments.remove(l);
            fireSegmentRemovedEvent(l);
        }
    }

    private void removeSegmentsFromPoint(PrintablePoint p) {
        if (p != null) {
            ArrayList<PrintablePath> segments2 = new ArrayList<>(segments.size());
            segments2.addAll(segments);
            for (PrintablePath s : segments2) {
                if (s.getP1() == p || s.getP2() == p) {
                    segments.remove(s);
                }
            }
        }
    }

    public void selectSegment(PrintablePath s, boolean selected) {
        s.setSelected(selected);
        fireSegmentSelectionEvent(s);
    }

    public void selectSegment(PrintablePath s) {
        s.setSelected(!s.isSelected());
        fireSegmentSelectionEvent(s);
    }

    public void addCircle(PrintableCircle circle) {
        if (circles == null) {
            circles = new ArrayList<>();
        }
        if (!circles.isEmpty()) {
            for (PrintableCircle c : circles) {
                if (c.getName().equals(circle.getName())) {
                    return;
                }
            }
        }
        circles.add(circle);
        fireNewCircleEvent(circle);
    }

    public void removeCircle(PrintableCircle circle) {
        if (circle != null) {
            circles.remove(circle);
            fireCircleRemovedEvent(circle);
        }
    }

    private void removeCirclesFromPoint(PrintablePoint p) {
        if (p != null) {
            ArrayList<PrintableCircle> circles2 = new ArrayList<>(circles.size());
            circles2.addAll(circles);
            for (PrintableCircle circle : circles2) {
                if (circle.getCenter() == p || circle.getPointOnCircle() == p) {
                    circles.remove(circle);
                    fireCircleRemovedEvent(circle);
                }
            }
        }
    }

    public void selectCircle(PrintableCircle c, boolean selected) {
        c.setSelected(selected);
        fireCircleSelectionEvent(c);
    }

    public void selectCircle(PrintableCircle c) {
        c.setSelected(!c.isSelected());
        fireCircleSelectionEvent(c);
    }

    public void addMiddlePoint(PrintablePath l) {
        if (l != null) {
            PrintablePoint p1 = l.getP1();
            PrintablePoint p2 = l.getP2();
            TransformablePoint middle = null;

            if (p1 instanceof HyperboloidPoint && p2 instanceof HyperboloidPoint) {
                PoincareDiskPoint pt1 = ((HyperboloidPoint) p1).toPoincareDiskPoint();
                PoincareDiskPoint pt2 = ((HyperboloidPoint) p2).toPoincareDiskPoint();
                middle = pt1.getMiddlePoint(pt2);
            }
            if (middle != null) {
                HyperboloidPoint hp = middle.toHyperboloid();
                points.add(hp);
                fireNewPointEvent(hp);
            }
        }
    }

    public void clearModel() {
        points.clear();
        lines.clear();
        segments.clear();
        circles.clear();

        fireSpaceCleared();
    }

    public void addGeometricSpaceListener(GeometricSpaceListener l) {
        if (l != null) {
            geometricSpaceListeners.add(l);
        }
    }

    public void removeGeometricSpaceListener(GeometricSpaceListener l) {
        if (l != null) {
            geometricSpaceListeners.remove(l);
        }
    }

    private void fireNewPointEvent(PrintablePoint p) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.pointAdded(p);
        }
    }

    private void fireFlyOverPointEvent(PrintablePoint p) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.pointFlyOver(p);
        }
    }

    public void firePointMovedEvent(PrintablePoint p) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.pointMoved(p);
        }
    }

    private void firePointRemovedEvent(PrintablePoint p) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.pointRemoved(p);
        }
    }

    private void firePointSelectionEvent(PrintablePoint p) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.pointSelection(p);
        }
    }

    private void fireNewLineEvent(PrintablePath gl) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.lineAdded(gl);
        }
    }

    private void fireFlyOverLineEvent(PrintablePath s) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.lineFlyOver(s);
        }
    }

    private void fireLineRemovedEvent(PrintablePath gl) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.lineRemoved(gl);
        }
    }

    private void fireLineSelectionEvent(PrintablePath gl) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.lineSelection(gl);
        }
    }

    private void fireNewSegmentEvent(PrintablePath gl) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.segmentAdded(gl);
        }
    }

    private void fireFlyOverSegmentEvent(PrintablePath s) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.segmentFlyOver(s);
        }
    }

    private void fireSegmentRemovedEvent(PrintablePath gl) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.segmentRemoved(gl);
        }
    }

    private void fireSegmentSelectionEvent(PrintablePath s) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.segmentSelection(s);
        }
    }

    private void fireNewCircleEvent(PrintableCircle c) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.circleAdded(c);
        }
    }

    private void fireFlyOverCircleEvent(PrintableCircle c) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.circleFlyOver(c);
        }
    }

    private void fireCircleRemovedEvent(PrintableCircle c) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.circleRemoved(c);
        }
    }

    private void fireCircleSelectionEvent(PrintableCircle c) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.circleSelection(c);
        }
    }

    private void fireSpaceCleared() {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.spaceCleared();
        }
    }

    public void set(GeometricSpace model) {
        clearModel();
        
        for (PrintablePoint p : model.getPoints()) {
            addPoint(p);
        }

        for (PrintablePath l : model.getLines()) {
            PrintablePoint p1 = getPointByName(l.getP1().getName());
            PrintablePoint p2 = getPointByName(l.getP2().getName());
            PrintablePath line = new PrintablePath(p1, p2, l.getName());
            line.setColor(l.getColor());
            addLine(line);
        }

        for (PrintablePath l : model.getSegments()) {
            PrintablePoint p1 = getPointByName(l.getP1().getName());
            PrintablePoint p2 = getPointByName(l.getP2().getName());
            PrintablePath segment = new PrintablePath(p1, p2, l.getName());
            segment.setColor(l.getColor());
            addSegment(segment);
        }

        for (PrintableCircle circle : model.getCircles()) {
            PrintablePoint center = getPointByName(circle.getCenter().getName());
            PrintablePoint pointOnCircle = getPointByName(circle.getPointOnCircle().getName());
            PrintableCircle hypCircle = new PrintableCircle(center, pointOnCircle, circle.getName());
            hypCircle.setColor(circle.getColor());
            addCircle(hypCircle);
        }
        
        fireSpaceUpdated(this);
    }

    private void fireObjectChanged(String oldName, PrintableObject modificationObject) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.objectChanged(oldName, modificationObject);
        }
    }
    
    private void fireSpaceUpdated(GeometricSpace newSpace) {
        for (GeometricSpaceListener l : geometricSpaceListeners) {
            l.spaceUpdated((HyperboloidSpace) newSpace);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if (!e.getValueIsAdjusting()) { // if selection has ended
            ListSelectionModel listSelection;
            if (source instanceof PointListViewer) {
                PointListViewer plv = (PointListViewer) source;
                listSelection = plv.getSelectionModel();
                for (int i = 0; i < points.size(); i++) {
                    selectPoint(points.get(i), listSelection.isSelectedIndex(i));
                }
                plv.repaint();
            } else if (source instanceof SegmentListViewer) {
                SegmentListViewer slv = (SegmentListViewer) source;
                listSelection = slv.getSelectionModel();
                for (int i = 0; i < segments.size(); i++) {
                    selectSegment(segments.get(i), listSelection.isSelectedIndex(i));
                }
                slv.repaint();
            } else if (source instanceof LineListViewer) {
                LineListViewer llv = (LineListViewer) source;
                listSelection = llv.getSelectionModel();
                for (int i = 0; i < lines.size(); i++) {
                    selectLine(lines.get(i), listSelection.isSelectedIndex(i));
                }
                llv.repaint();
            } else if (source instanceof CircleListViewer) {
                CircleListViewer clv = (CircleListViewer) source;
                listSelection = clv.getSelectionModel();
                for (int i = 0; i < circles.size(); i++) {
                    selectCircle(circles.get(i), listSelection.isSelectedIndex(i));
                }
                clv.repaint();
            }
        }
    }

    @Override
    public String toString() {
        return "GeometricSpace{" + "points=" + points.size() + ", lines=" + lines.size() + ", segments="
                + segments.size() + ", circles=" + circles.size() + '}';
    }
}
