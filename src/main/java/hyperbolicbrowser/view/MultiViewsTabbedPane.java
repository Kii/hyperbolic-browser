/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.view;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public abstract class MultiViewsTabbedPane extends JTabbedPane {
    
    /**
	 * Generated
	 */
	private static final long serialVersionUID = -3972502869035274552L;
	protected JComponent mainComponent;
    
    public MultiViewsTabbedPane(String title){
        this(title, new JPanel());
    }

    public MultiViewsTabbedPane(String title, JComponent component) {
        super(SwingConstants.TOP);

        addTab(title, component);
        mainComponent = component;
    }

    @Override
    public void addTab(String title, Component component) {
        super.addTab(title, new JScrollPane(component, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
    }
}
