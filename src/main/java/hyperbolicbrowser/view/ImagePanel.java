/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hyperbolicbrowser.view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */


public class ImagePanel extends JPanel{

    /**
	 * Generated
	 */
	private static final long serialVersionUID = -5926091441944884732L;
	private BufferedImage image;

    public ImagePanel(String fileName) {
       try {                
          image = ImageIO.read(new File(fileName));
       } catch (IOException ex) {
            // handle exception...
       }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
    }

}