/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Vincent Dolez (vincent.dolez@isen-lille.fr)
 */
public class HyperbolicFileFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        return (f.getPath().endsWith(".hyp") || f.isDirectory());
    }

    @Override
    public String getDescription() {
        return "Hyperbolic Spaces (.hyp)";
    }
    
}
