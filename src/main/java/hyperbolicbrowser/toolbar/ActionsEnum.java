/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar;

/**
 *
 * @author ISEN
 */
public enum ActionsEnum {
    ADD_LINE,
    ADD_SEGMENT,
    ADD_MIDDLE_POINT,
    ADD_CIRCLE,
    ADD_POINTS,
    ADD_POINT,
    CENTER_POINT,
    CLEAR,
    DYNAMIC_TRANSLATE,
    GRAB,
    REMOVE,
    ROTATE,
    SELECT
}
