package hyperbolicbrowser.toolbar.infosToolbar;

import javax.swing.DefaultListSelectionModel;
import javax.swing.event.ListSelectionListener;

public class ListViewerSelectionModel extends DefaultListSelectionModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3799857431459622172L;

	public ListViewerSelectionModel() {
		super();
	}
	
	public void addSelectionIntervalWithoutEvents(int idx0, int idx1) {
		ListSelectionListener[] listeners = getListSelectionListeners();
		for(ListSelectionListener l : listeners) {
			removeListSelectionListener(l);
		}
		addSelectionInterval(idx0, idx1);
		for(ListSelectionListener l : listeners) {
			addListSelectionListener(l);
		}
	}
	
	public void removeIndexIntervalWithoutEvent(int idx0, int idx1) {
		ListSelectionListener[] listeners = getListSelectionListeners();
		for(ListSelectionListener l : listeners) {
			removeListSelectionListener(l);
		}
		removeSelectionInterval(idx0, idx1);
		for(ListSelectionListener l : listeners) {
			addListSelectionListener(l);
		}
	}
}
