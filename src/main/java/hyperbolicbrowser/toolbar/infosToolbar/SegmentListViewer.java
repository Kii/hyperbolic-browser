/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar.infosToolbar;

import java.util.List;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class SegmentListViewer extends ListViewer<PrintablePath> {

    /**
	 * Generated
	 */
	private static final long serialVersionUID = 6433638300330972865L;

	public SegmentListViewer(String title, List<PrintablePath> list, int width, int height) {
        super(title, list, width, height);
    }

}
