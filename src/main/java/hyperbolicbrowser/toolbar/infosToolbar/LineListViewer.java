/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar.infosToolbar;

import java.util.List;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class LineListViewer extends ListViewer<PrintablePath> {

    /**
	 * Generated
	 */
	private static final long serialVersionUID = 1889272845450717329L;

	public LineListViewer(String title, List<PrintablePath> list, int width, int height) {
        super(title, list, width, height);
    }

}
