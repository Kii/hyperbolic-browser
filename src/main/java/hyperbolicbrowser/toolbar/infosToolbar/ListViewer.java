/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar.infosToolbar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public abstract class ListViewer<T extends PrintableObject> extends JPanel implements ActionListener, ListSelectionListener {

    /**
     * Generated
     */
    private static final long serialVersionUID = 2537800814822191194L;
    protected JTable table;
    protected DefaultListModel<T> listModel;
    protected JList<T> jList;
    protected JButton deselectButton;
    protected JButton selectAllButton;
    protected List<ListSelectionListener> jListSelectionListeners;

	public ListViewer(String title, List<T> list, int width, int height) {
        super(new BorderLayout());

        JPanel header = new JPanel();
        header.setLayout(new BoxLayout(header, BoxLayout.PAGE_AXIS));
        //this.setPreferredSize(new Dimension(width, height / 10));

        JLabel hLabel = new JLabel(title);
        hLabel.setFont(new Font("Serif", Font.BOLD, 15));

        deselectButton = new JButton("Deselect");
        selectAllButton = new JButton("Select All");
        JPanel pan1 = new JPanel();

        pan1.add(deselectButton);
        pan1.add(selectAllButton);

        deselectButton.addActionListener(this);
        selectAllButton.addActionListener(this);

        hLabel.setForeground(Color.BLACK);
        header.setBackground(Color.CYAN);
        header.add(hLabel);
        header.add(pan1);

        this.add(header, BorderLayout.NORTH);

        listModel = new DefaultListModel<>();
        jList = new JList<>(listModel);

        jList.setSelectionModel(new ListViewerSelectionModel());
        jList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jList.getSelectionModel().addListSelectionListener(this);
        jList.setLayoutOrientation(JList.VERTICAL);
        jList.setVisibleRowCount(-1);
        
        jListSelectionListeners = new LinkedList<>();
        
        JScrollPane listScroller = new JScrollPane(jList);
        this.add(listScroller, BorderLayout.CENTER);

        this.setBackground(Color.BLUE);
        this.setPreferredSize(new Dimension(width, height));
    }

    public boolean isSameJList(JList<T> l) {
        return l == this.jList;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == deselectButton) {
            jList.getSelectionModel().clearSelection();
        } else if (ae.getSource() == selectAllButton) {
            jList.setSelectionInterval(0, (jList.getModel().getSize() >= 1) ? (jList.getModel().getSize() - 1) : 0);
        }
    }
    
    @Override
	public void valueChanged(ListSelectionEvent e) {
		for(ListSelectionListener selectionListener : jListSelectionListeners) {
			selectionListener.valueChanged(new ListSelectionEvent(this, e.getFirstIndex(), e.getLastIndex(), e.getValueIsAdjusting()));
		}
	}

	public void addListSelectionListener(ListSelectionListener selectionListener) {
    	jListSelectionListeners.add(selectionListener);
    }

    public void setSelectedIndex(int i) {
        jList.setSelectedIndex(i);
    }
    
    public ListModel<T> getModel() {
    	return jList.getModel();
    }
    
    public ListSelectionModel getSelectionModel() {
    	return jList.getSelectionModel();
    }
}
