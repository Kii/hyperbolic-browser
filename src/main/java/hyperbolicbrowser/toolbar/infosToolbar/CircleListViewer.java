package hyperbolicbrowser.toolbar.infosToolbar;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;

import java.util.List;

public class CircleListViewer extends ListViewer<PrintableCircle> {

    public CircleListViewer(String title, List<PrintableCircle> list, int width, int height) {
        super(title, list, width, height);
    }

}
