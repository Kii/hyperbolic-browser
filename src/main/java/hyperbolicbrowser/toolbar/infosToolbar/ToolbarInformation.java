/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar.infosToolbar;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import hyperbolicbrowser.geometricmodels.GeometricSpace;
import hyperbolicbrowser.geometricmodels.HyperboloidSpace;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePath;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableObject;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintableCircle;
import hyperbolicbrowser.listeners.GeometricSpaceListener;

/**
 *
 * @author ISEN
 */
public class ToolbarInformation extends JPanel implements GeometricSpaceListener {

    /**
     * Generated
     */
    private static final long serialVersionUID = -8202173829856382584L;
    private PointListViewer pointsListViewer;
    private LineListViewer linesListViewer;
    private SegmentListViewer segmentsListViewer;
    private CircleListViewer circleListViewer;
    private static int nbToolBar = 0;
    private String idListener;

    private static final int nbViewer = 4;

    public ToolbarInformation(GeometricSpace m, int width, int height) {
        super(new BorderLayout());

        pointsListViewer = new PointListViewer("Points", m.getPoints(), width, height / nbViewer);
        pointsListViewer.addListSelectionListener(m);
        linesListViewer = new LineListViewer("Lines", m.getLines(), width, height / nbViewer);
        linesListViewer.addListSelectionListener(m);
        segmentsListViewer = new SegmentListViewer("Segments", m.getSegments(), width, height / nbViewer);
        segmentsListViewer.addListSelectionListener(m);
        circleListViewer = new CircleListViewer("Circles", m.getCircles(), width, height / nbViewer);
        circleListViewer.addListSelectionListener(m);

        JSplitPane thirdSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, circleListViewer, segmentsListViewer);
        thirdSplitPane.setResizeWeight(1d / 2d);
        JSplitPane secondSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, linesListViewer, thirdSplitPane);
        secondSplitPane.setResizeWeight(1d / 3d);
        JSplitPane firstSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, pointsListViewer, secondSplitPane);
        firstSplitPane.setResizeWeight(1d / 4d);
        add(firstSplitPane, BorderLayout.CENTER);

        idListener = "ToolbarInformation" + String.valueOf(nbToolBar++);
    }

    @Override
    public void pointAdded(PrintablePoint p) {
        DefaultListModel<PrintablePoint> lm = (DefaultListModel<PrintablePoint>) pointsListViewer.getModel();
        lm.addElement(p);
    }

    @Override
    public void pointMoved(PrintablePoint p) {
    }

    @Override
    public void pointRemoved(PrintablePoint p) {
        DefaultListModel<PrintablePoint> lm = (DefaultListModel<PrintablePoint>) pointsListViewer.getModel();
        lm.removeElement(p);
    }

    @Override
    public void pointSelection(PrintablePoint p) {
        DefaultListModel<PrintablePoint> lm = (DefaultListModel<PrintablePoint>) pointsListViewer.getModel();
        int idx = lm.indexOf(p);
        if (p.isSelected()) {
            ((ListViewerSelectionModel) pointsListViewer.getSelectionModel()).addSelectionIntervalWithoutEvents(idx, idx);
        } else {
            ((ListViewerSelectionModel) pointsListViewer.getSelectionModel()).removeIndexIntervalWithoutEvent(idx, idx);
        }
        pointsListViewer.repaint();
    }

    @Override
    public void pointFlyOver(PrintablePoint p) {}

    @Override
    public void lineAdded(PrintablePath l) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) linesListViewer.getModel();
        lm.addElement(l);
    }

    @Override
    public void lineRemoved(PrintablePath l) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) linesListViewer.getModel();
        lm.removeElement(l);
    }

    @Override
    public void lineSelection(PrintablePath l) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) linesListViewer.getModel();
        int idx = lm.indexOf(l);
        if (l.isSelected()) {
            ((ListViewerSelectionModel) linesListViewer.getSelectionModel()).addSelectionIntervalWithoutEvents(idx, idx);
        } else {
            ((ListViewerSelectionModel) linesListViewer.getSelectionModel()).removeIndexIntervalWithoutEvent(idx, idx);
        }
        linesListViewer.repaint();
    }

    @Override
    public void lineFlyOver(PrintablePath l) { }

    @Override
    public void segmentAdded(PrintablePath s) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) segmentsListViewer.getModel();
        lm.addElement(s);
    }

    @Override
    public void segmentFlyOver(PrintablePath s) {}

    @Override
    public void segmentRemoved(PrintablePath s) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) segmentsListViewer.getModel();
        lm.removeElement(s);
    }

    @Override
    public void segmentSelection(PrintablePath s) {
        DefaultListModel<PrintablePath> lm = (DefaultListModel<PrintablePath>) segmentsListViewer.getModel();
        int idx = lm.indexOf(s);
        if (s.isSelected()) {
            ((ListViewerSelectionModel) segmentsListViewer.getSelectionModel()).addSelectionIntervalWithoutEvents(idx, idx);
        } else {
            ((ListViewerSelectionModel) segmentsListViewer.getSelectionModel()).removeIndexIntervalWithoutEvent(idx, idx);
        }
        segmentsListViewer.repaint();
    }

    @Override
    public void circleAdded(PrintableCircle c) {
		DefaultListModel<PrintableCircle> lm = (DefaultListModel<PrintableCircle>) circleListViewer.getModel();
		lm.addElement(c);
    }

    @Override
    public void circleRemoved(PrintableCircle c) {
        DefaultListModel<PrintableCircle> lm = (DefaultListModel<PrintableCircle>) circleListViewer.getModel();
        lm.removeElement(c);
    }

    @Override
    public void circleSelection(PrintableCircle c) {
        DefaultListModel<PrintableCircle> lm = (DefaultListModel<PrintableCircle>) circleListViewer.getModel();
        int idx = lm.indexOf(c);
        if (c.isSelected()) {
            ((ListViewerSelectionModel) circleListViewer.getSelectionModel()).addSelectionIntervalWithoutEvents(idx, idx);
        } else {
            ((ListViewerSelectionModel) circleListViewer.getSelectionModel()).removeIndexIntervalWithoutEvent(idx, idx);
        }
        circleListViewer.repaint();
    }

    @Override
    public void circleFlyOver(PrintableCircle c) { }

    @Override
    public void spaceCleared() {
        DefaultListModel<PrintablePoint> lmp = (DefaultListModel<PrintablePoint>) pointsListViewer.getModel();
        lmp.clear();
        DefaultListModel<PrintablePath> lms = (DefaultListModel<PrintablePath>) segmentsListViewer.getModel();
        lms.clear();
        DefaultListModel<PrintablePath> lml = (DefaultListModel<PrintablePath>) linesListViewer.getModel();
        lml.clear();
        DefaultListModel<PrintableCircle> lmc = (DefaultListModel<PrintableCircle>) circleListViewer.getModel();
        lmc.clear();
    }

    @Override
    public void spaceUpdated(HyperboloidSpace space) {
        // TODO Auto-generated method stub
    }

    @Override
    public String getIdListener() {
        return idListener;
    }

    @Override
    public void objectChanged(String oldName, PrintableObject p) {
        repaint();
    }
}
