/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar.infosToolbar;

import java.util.List;

import hyperbolicbrowser.geometricmodels.geometricobjects.printables.PrintablePoint;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class PointListViewer extends ListViewer<PrintablePoint> {

    /**
	 * Generated
	 */
	private static final long serialVersionUID = 7950470327251580664L;

	public PointListViewer(String title, List<PrintablePoint> list, int width, int height) {
        super(title, list, width, height);
    }

}
