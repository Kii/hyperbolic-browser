/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hyperbolicbrowser.toolbar;

import hyperbolicbrowser.listeners.ActionButtonListener;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class ActionButtonHelpPanel extends JPanel implements ActionButtonListener {
    
    /**
	 * Generated
	 */
	private static final long serialVersionUID = -3389345811941900801L;
	JTextArea text;
    
    public ActionButtonHelpPanel(int width, int height ){
        text = new JTextArea();
        text.setText("This is a help panel.\n\nClick on the buttons to know how to use them.");
        text.setEditable(false);
        text.setBackground(Color.WHITE);
        text.setCaretColor(Color.BLACK);
        text.setPreferredSize(new Dimension(width, height));
        text.setSize(width, height);
        text.setVisible(true);

        setPreferredSize(new Dimension(width, height));
        setSize(width, height);
        setVisible(true);
        add(text);
    }

    @Override
    public void currentActionChanged(ActionsEnum ae) {
        switch (ae) {
            case SELECT:
                text.setText("SELECT OBJECT:\n\n"
                        + "Click on an object to select it. \n\nKeep 'ctrl' pressed and the previous selection will persist.");
                break;
            case REMOVE:
                text.setText("REMOVE SELECTED OBJECTS:\n\n"
                        + "Select objects and double-click on the button");
                break;
            case CLEAR:
                text.setText("CLEAR ALL OBJECTS:\n\n"
                        + "Double click on the button to clear space");
                break;
            case ADD_POINTS:
                text.setText("ADD POINTS :\n\n"
                        + "Click on the surface and a pop-up will appear. \n\nThen chose what kind of generation you want in the selection list. \n\n Fill the information needed.");
                break;
            case ADD_POINT:
                text.setText("ADD POINT :\n\n"
                        + "Click where you want on the surface to add some points.");
                break;
            case ADD_LINE:
                text.setText("ADD LINE :\n\n"
                        + "Click next to 2 points to add an hyperbolic line.");
                break;
            case ADD_SEGMENT:
                text.setText("ADD SEGMENT :\n\n"
                        + "Click next to 2 points to add an hyperbolic segment.");
                break;
            case ADD_MIDDLE_POINT:
                text.setText("ADD MIDDLE POINT :\n\n"
                        + "Click next to 2 points to place a point at the hyperbolic middle of it.");
                break;
            case ADD_CIRCLE:
                text.setText("ADD CIRCLE :\n\n"
                        + "Click first to place the center, then click again set radius.");
                break;
            case GRAB:
                text.setText("GRAB POINT:\n\n"
                        + "Grab a point by drag and drop.\n"
                        + "The point will follow the mouse while the mouse click is pressed.");
                break;
            case CENTER_POINT:
                text.setText("CENTER POINT :\n\n"
                        + "Click next to a point to center it.\n"
                        + "Every objects will be translated with it.");
                break;
            case DYNAMIC_TRANSLATE:
                text.setText("DYNAMIC TRANSLATE :\n\n"
                        + "Drag and drop the mouse click on the surface.\n"
                        + "The objects will be translated.");
                break;
            case ROTATE:
                text.setText("ROTATE :\n\n"
                        + "Drag and drop the mouse click on the surface.\n"
                        + "The objects will be rotated around the 'center'.");
                break;
        }
    }
}
