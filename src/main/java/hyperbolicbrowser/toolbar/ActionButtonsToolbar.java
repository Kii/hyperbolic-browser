/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar;

import hyperbolicbrowser.HyperbolicBrowserFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;

import hyperbolicbrowser.listeners.ActionButtonListener;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class ActionButtonsToolbar extends JPanel {

    /**
     * Generated
     */
    private static final long serialVersionUID = -6350425429636246401L;
    ActionButtonHelpPanel helpPanel;
    private ButtonGroup buttonGroup;

    public ActionButtonsToolbar(int width, int height) {

        super(new BorderLayout());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setBackground(Color.DARK_GRAY);
        //buttonPanel.setPreferredSize(new Dimension(width * (4/5), height));

        helpPanel = new ActionButtonHelpPanel(width / 5, height);

        JToggleButton select = new ActionButton(ActionsEnum.SELECT, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_select.png")));
        JToggleButton remove = new ActionButton(ActionsEnum.REMOVE, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_remove.png")));
        JToggleButton clear = new ActionButton(ActionsEnum.CLEAR, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_clear.png")));
        JToggleButton addPoints = new ActionButton(ActionsEnum.ADD_POINTS, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_tiling.png")));

        JToggleButton addPoint = new ActionButton(ActionsEnum.ADD_POINT, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_add_point.png")));
        JToggleButton addLine = new ActionButton(ActionsEnum.ADD_LINE, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_add_hyperbolic_line.png")));
        JToggleButton addSegment = new ActionButton(ActionsEnum.ADD_SEGMENT, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_add_hyperbolic_segment.png")));
        JToggleButton addMiddlePoint = new ActionButton(ActionsEnum.ADD_MIDDLE_POINT, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_add_hyperbolic_midPoint.png")));
        JToggleButton addCircle = new ActionButton(ActionsEnum.ADD_CIRCLE, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_add_hyperbolic_circle.png")));
        JToggleButton grab = new ActionButton(ActionsEnum.GRAB, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_grab.png")));

        JToggleButton centerPoint = new ActionButton(ActionsEnum.CENTER_POINT, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_center_point.png")));
        JToggleButton dynamicTranslate = new ActionButton(ActionsEnum.DYNAMIC_TRANSLATE, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_dynamic_translate.png")));
        JToggleButton dynamicRotate = new ActionButton(ActionsEnum.ROTATE, new ImageIcon(HyperbolicBrowserFrame.class.getResource("/images/toolbar_rotate.png")));

        buttonGroup = new ButtonGroup();

        buttonGroup.add(select);
        buttonGroup.add(remove);
        buttonGroup.add(clear);
        buttonGroup.add(addPoints);
        buttonGroup.add(addPoint);
        buttonGroup.add(addLine);
        buttonGroup.add(addSegment);
        buttonGroup.add(addMiddlePoint);
        buttonGroup.add(addCircle);
        buttonGroup.add(grab);
        buttonGroup.add(centerPoint);
        buttonGroup.add(dynamicTranslate);
        buttonGroup.add(dynamicRotate);

        buttonPanel.add(select);
        buttonPanel.add(remove);
        buttonPanel.add(clear);
        buttonPanel.add(addPoints);
        buttonPanel.add(addPoint);
        buttonPanel.add(addLine);
        buttonPanel.add(addSegment);
        buttonPanel.add(addMiddlePoint);
        buttonPanel.add(addCircle);
        buttonPanel.add(grab);
        buttonPanel.add(centerPoint);
        buttonPanel.add(dynamicTranslate);
        buttonPanel.add(dynamicRotate);

        JScrollPane scrollPane = new JScrollPane(buttonPanel);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        add(scrollPane, BorderLayout.CENTER);
        add(helpPanel, BorderLayout.EAST);
        addActionButtonListener(helpPanel);

        setSize(new Dimension(width, height));
    }

    public void addActionButtonListener(ActionButtonListener l) {
        if (l != null) {
            Enumeration<AbstractButton> buttons = buttonGroup.getElements();
            while (buttons.hasMoreElements()) {
                AbstractButton currentB = buttons.nextElement();

                if (currentB instanceof ActionButton) {
                    ActionButton aB = (ActionButton) currentB;
                    aB.addActionButtonListener(l);
                }
            }
        }
    }

    public void removeActionsButtonListener(ActionButtonListener l) {
        if (l != null) {
            Enumeration<AbstractButton> buttons = buttonGroup.getElements();
            while (buttons.hasMoreElements()) {
                AbstractButton currentB = buttons.nextElement();

                if (currentB instanceof ActionButton) {
                    ActionButton aB = (ActionButton) currentB;
                    aB.removeActionButtonListener(l);
                }
            }
        }
    }
}
