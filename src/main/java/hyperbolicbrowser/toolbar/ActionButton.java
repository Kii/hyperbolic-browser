/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperbolicbrowser.toolbar;

import hyperbolicbrowser.listeners.ActionButtonListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

/**
 *
 * @author Rousselle Rémi (remi.rousselle@isen-lille.fr)
 */
public class ActionButton extends JToggleButton implements ActionListener {

    /**
	 * Generated 
	 */
	private static final long serialVersionUID = -2582148345130704064L;
	private ActionsEnum action;
    private ArrayList<ActionButtonListener> listeners;

    public ActionButton(ActionsEnum action, ImageIcon icon) {
        super(icon);
        this.action = action;
        listeners = new ArrayList<>();
        addActionListener(this);
    }

    public void addActionButtonListener(ActionButtonListener l) {
        if (l != null) {
            listeners.add(l);
        }
    }
    
    public void removeActionButtonListener(ActionButtonListener l) {
        if (l != null) {
            listeners.remove(l);
        }
    }
    
    public void fireActionEvent(){
        for(ActionButtonListener l : listeners){
            l.currentActionChanged(action);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        fireActionEvent();
    }
}
