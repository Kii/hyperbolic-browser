package euclidean;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanLine;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TestEuclideanLine extends TestEuclidean {

    private EuclideanPoint tmpPoint;
    private EuclideanLine tmpLine;

    @Test
    public void testEuclideanLinesEquality() {
        EuclideanLine line = EuclideanLine.build(1d, 1d, 1d, "");
        EuclideanLine line2 = EuclideanLine.build(2d, 2d, 2d, "");
        assertEquals(line, line2);

        EuclideanLine absyss = EuclideanLine.build(0d, 0d, "");
        assertEquals(Oi, absyss);
        absyss = EuclideanLine.build(0d, 1d, 0d, "");
        assertEquals(Oi, absyss);

        EuclideanLine ordonnees = EuclideanLine.build(1d, 0d, 0d, "");
        assertEquals(Oj, ordonnees);
    }

    @Test
    public void testLineCoreAndImage() {
        double a;
        a = Oi.getImage(0d);
        assertEquals(0d, a, margeErreur);
        a = Oi.getImage(1d);
        assertEquals(0d, a, margeErreur);

        a = Oj.getCore(0d);
        assertEquals(0d, a, margeErreur);
        a = Oj.getCore(1d);
        assertEquals(0d, a, margeErreur);

        a = unitLine.getImage(0d);
        assertEquals(0d, a, margeErreur);
        a = unitLine.getImage(1d);
        assertEquals(1d, a, margeErreur);
        a = unitLine.getImage(-1d);
        assertEquals(-1d, a, margeErreur);

        a = unitLine.getCore(0d);
        assertEquals(0d, a, margeErreur);
        a = unitLine.getCore(1d);
        assertEquals(1d, a, margeErreur);
        a = unitLine.getCore(-1d);
        assertEquals(-1d, a, margeErreur);

        a = ij.getCore(0d);
        assertEquals(1d, a, margeErreur);
        a = ij.getImage(1d);
        assertEquals(0d, a, margeErreur);

        tmpLine = new EuclideanLine(O, new EuclideanPoint(1d, 2d));
        a = tmpLine.getCore(0d);
        assertEquals(0d, a, margeErreur);
        a = tmpLine.getImage(0d);
        assertEquals(0d, a, margeErreur);
        a = tmpLine.getImage(1d);
        assertEquals(2d, a, margeErreur);
        a = tmpLine.getCore(2d);
        assertEquals(1d, a, margeErreur);
        a = tmpLine.getImage(0.5d);
        assertEquals(1d, a, margeErreur);
        a = tmpLine.getCore(1d);
        assertEquals(0.5d, a, margeErreur);
    }

    @Test
    public void testIncreaseOrDecrease() {
        assertTrue(unitLine.increase());
        assertFalse(unitLine.decrease());

        assertTrue(ij.decrease());
        assertFalse(ij.increase());

        tmpLine = new EuclideanLine(O, new EuclideanPoint(1d, 2d));
        assertTrue(tmpLine.increase());
        assertFalse(tmpLine.decrease());

        tmpLine = new EuclideanLine(O, new EuclideanPoint(1d, -2d));
        assertTrue(tmpLine.decrease());
        assertFalse(tmpLine.increase());
    }

    @Test
    public void testIfPointIsOnLine() {
        assertTrue(Oi.containsPoint(O));
        assertTrue(Oi.containsPoint(i));
        assertTrue(Oi.containsPoint(EuclideanPoint.multiply(i, 2d)));
        assertTrue(Oi.containsPoint(EuclideanPoint.multiply(i, -5d)));

        assertTrue(Oj.containsPoint(O));
        assertTrue(Oj.containsPoint(j));
        assertTrue(Oj.containsPoint(EuclideanPoint.multiply(j, 2d)));
        assertTrue(Oj.containsPoint(EuclideanPoint.multiply(j, -5d)));

        assertTrue(unitLine.containsPoint(O));
        assertTrue(unitLine.containsPoint(unit));
        assertTrue(unitLine.containsPoint(EuclideanPoint.multiply(unit, 2d)));
        assertTrue(unitLine.containsPoint(EuclideanPoint.multiply(unit, -5d)));
        assertTrue(unitLine.containsPoint(EuclideanPoint.multiply(unit, Math.E)));

        assertTrue(ij.containsPoint(i));
        assertTrue(ij.containsPoint(j));
        assertTrue(ij.containsPoint(new EuclideanPoint(0.5d, 0.5d)));
        assertFalse(ij.containsPoint(new EuclideanPoint(0.6d, 0.5d)));
        assertFalse(ij.containsPoint(new EuclideanPoint(0.5d, 0.6d)));
        assertTrue(ij.containsPoint(new EuclideanPoint(1.5d, -0.5d)));
        assertTrue(ij.containsPoint(new EuclideanPoint(-1d, 2d)));
    }

    @Test
    public void testLineVerticalOrHorizontal() {
        assertTrue(Oi.isHorizontal());
        assertFalse(Oi.isVertical());

        assertTrue(Oj.isVertical());
        assertFalse(Oj.isHorizontal());

        assertFalse(ij.isVertical());
        assertFalse(ij.isHorizontal());
        assertFalse(unitLine.isVertical());
        assertFalse(unitLine.isHorizontal());

        tmpLine = new EuclideanLine(i, unit);
        assertTrue(tmpLine.isVertical());

        tmpLine = new EuclideanLine(j, unit);
        assertTrue(tmpLine.isHorizontal());
    }

    @Test
    public void testLineOrthogonal() {
        EuclideanLine line;
        tmpLine = new EuclideanLine(i, unit);
        line = Oi.orthogonal(i);
        assertEquals(tmpLine, line);

        line = unitLine.orthogonal(unit);
        assertEquals(EuclideanLine.build(-1d, 2d, ""), line);

        tmpLine = EuclideanLine.build(1d, 1d, "");
        line = tmpLine.orthogonal(j);
        assertEquals(EuclideanLine.build(-1d, 1d, ""), line);
    }

    @Test
    public void testGetDistantPointOnLine() {
        tmpPoint = Oi.getPointAtDistance(O, 1d);
        assertEquals(i, tmpPoint);

        tmpPoint = Oi.getPointAtDistance(i, -1d);
        assertEquals(O, tmpPoint);
        tmpPoint = Oj.getPointAtDistance(O, 1d);
        assertEquals(j, tmpPoint);
        tmpPoint = Oj.getPointAtDistance(j, -1d);
        assertEquals(O, tmpPoint);

        tmpPoint = unitLine.getPointAtDistance(O, Math.sqrt(2d));
        assertEquals(0d, unit.distance(tmpPoint), margeErreur);
        tmpPoint = unitLine.getPointAtDistance(unit, Math.sqrt(2d));
        assertEquals(0d, EuclideanPoint.multiply(unit, 2d).distance(tmpPoint), margeErreur);
        tmpPoint = unitLine.getPointAtDistance(unit, -Math.sqrt(2d));
        assertEquals(0d, O.distance(tmpPoint), margeErreur);

        tmpLine = EuclideanLine.build(1d, 1d, "");
        tmpPoint = tmpLine.getPointAtDistance(j, Math.sqrt(2d));
        assertEquals(new EuclideanPoint(1d, 2d), tmpPoint);

        tmpLine = EuclideanLine.build(1d, -1d, "");
        tmpPoint = tmpLine.getPointAtDistance(new EuclideanPoint(0d, -1d), Math.sqrt(2d));
        assertEquals(0d, tmpPoint.distance(new EuclideanPoint(1d, 0d)), margeErreur);
    }

    @Test
    public void testEuclideanLinesIntersect() {
        tmpPoint = Oi.intersect(Oj);
        assertEquals(O, tmpPoint);

        tmpPoint = Oi.intersect(ij);
        assertEquals(i, tmpPoint);

        tmpPoint = Oj.intersect(ij);
        assertEquals(j, tmpPoint);

        tmpPoint = Oj.intersect(Oj);
        assertNull(tmpPoint);

        tmpPoint = Oj.intersect(new EuclideanLine(i, unit));
        assertNull(tmpPoint);

        tmpLine = unitLine.orthogonal(unit);
        assertEquals(unit, unitLine.intersect(tmpLine));

        tmpLine = unitLine.orthogonal(unit);
        assertEquals(unit, unitLine.intersect(tmpLine));

        tmpLine = new EuclideanLine(O, unit);
        tmpPoint = EuclideanPoint.multiply(unit, 0.5d);
        assertEquals(tmpPoint, tmpLine.intersect(ij));

        tmpLine = EuclideanLine.build(1d, 1d, "");
        tmpPoint = tmpLine.intersect(EuclideanLine.build(-1d, 1d, ""));
        assertEquals(j, tmpPoint);
    }

    @Test
    public void testLineIntersectsCircle() {
        List<EuclideanPoint> intersections;

        intersections = Oi.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertTrue(i.equals(intersections.get(0)) || i.equals(intersections.get(1)));
        tmpPoint = EuclideanPoint.multiply(i, -1d);
        assertTrue(tmpPoint.equals(intersections.get(0)) || tmpPoint.equals(intersections.get(1)));

        intersections = Oj.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertTrue(j.equals(intersections.get(0)) || j.equals(intersections.get(1)));
        tmpPoint = EuclideanPoint.multiply(j, -1d);
        assertTrue(tmpPoint.equals(intersections.get(0)) || tmpPoint.equals(intersections.get(1)));

        tmpLine = new EuclideanLine(O, EuclideanPoint.multiply(j, 1d));
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertEquals(j, intersections.get(1));
        assertEquals(EuclideanPoint.multiply(j, -1d), intersections.get(0));

        tmpLine = new EuclideanLine(O, EuclideanPoint.multiply(j, -1d));
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertEquals(j, intersections.get(1));
        assertEquals(EuclideanPoint.multiply(j, -1d), intersections.get(0));

        tmpLine = new EuclideanLine(O, new EuclideanPoint(0d, -0.762d));
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertEquals(j, intersections.get(1));
        assertEquals(EuclideanPoint.multiply(j, -1d), intersections.get(0));

        intersections = ij.intersect(unitCircle);
        assertEquals(2, intersections.size());
        assertTrue(j.equals(intersections.get(0)) || j.equals(intersections.get(1)));
        assertTrue(i.equals(intersections.get(0)) || i.equals(intersections.get(1)));

        tmpLine = unitLine;
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(2, intersections.size());
        tmpPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI / 4d), "");
        assertEquals(0d, Math.min(tmpPoint.distance(intersections.get(0)), tmpPoint.distance(intersections.get(1))), margeErreur);
        tmpPoint = new EuclideanPoint(new PolarCoordinate(1d, 5d * Math.PI / 4d), "");
        assertEquals(0d, Math.min(tmpPoint.distance(intersections.get(0)), tmpPoint.distance(intersections.get(1))), margeErreur);

        tmpLine = EuclideanLine.build(-1d, 0d, "");
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(2, intersections.size());
        tmpPoint = new EuclideanPoint(new PolarCoordinate(1d, 3d * Math.PI / 4d), "");
        assertEquals(0d, Math.min(tmpPoint.distance(intersections.get(0)), tmpPoint.distance(intersections.get(1))), margeErreur);
        tmpPoint = new EuclideanPoint(new PolarCoordinate(1d, -Math.PI / 4d), "");
        assertEquals(0d, Math.min(tmpPoint.distance(intersections.get(0)), tmpPoint.distance(intersections.get(1))), margeErreur);

        tmpLine = EuclideanLine.build(0d, 1d, "");
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(1, intersections.size());
        assertEquals(j, intersections.get(0));

        tmpLine = EuclideanLine.build(0d, -1d, "");
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(1, intersections.size());
        assertEquals(EuclideanPoint.multiply(j, -1d), intersections.get(0));

        tmpLine = new EuclideanLine(i, new EuclideanPoint(1d, 1d));
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(1, intersections.size());
        assertEquals(i, intersections.get(0));

        tmpLine = new EuclideanLine(EuclideanPoint.multiply(i, -1d), new EuclideanPoint(-1d, 1d));
        intersections = tmpLine.intersect(unitCircle);
        assertEquals(1, intersections.size());
        assertEquals(EuclideanPoint.multiply(i, -1d), intersections.get(0));
    }

    @Test
    public void testDistanceFromPointToLine() {
        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanLine(i, Oj), margeErreur);
        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanLine(j, Oi), margeErreur);

        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanLine(O, ij), margeErreur);
        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanLine(i, unitLine), margeErreur);
        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanLine(j, unitLine), margeErreur);

        assertEquals(0d, EuclideanLine.distanceFromPointToEuclideanLine(O, unitLine), margeErreur);
        assertEquals(0d, EuclideanLine.distanceFromPointToEuclideanLine(unit, unitLine), margeErreur);
        assertEquals(0d, EuclideanLine.distanceFromPointToEuclideanLine(EuclideanPoint.multiply(unit, -1d), unitLine), margeErreur);
    }

    @Test
    public void testDistanceFromPointToSegment() {
        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanSegment(i, Oj, O, j), margeErreur);
        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanSegment(j, Oi, O, i), margeErreur);

        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanSegment(O, ij, i, j), margeErreur);
        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanSegment(i, unitLine, O, unit), margeErreur);
        assertEquals(Math.sqrt(2d)/2d, EuclideanLine.distanceFromPointToEuclideanSegment(j, unitLine, O, unit), margeErreur);

        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanSegment(unit, Oi, O, i), margeErreur);
        assertEquals(1d, EuclideanLine.distanceFromPointToEuclideanSegment(unit, Oj, O, j), margeErreur);

        assertEquals(0d, EuclideanLine.distanceFromPointToEuclideanSegment(O, unitLine, O, unit), margeErreur);
        assertEquals(0d, EuclideanLine.distanceFromPointToEuclideanSegment(unit, unitLine, O, unit), margeErreur);

        EuclideanPoint tmp;
        tmp = new EuclideanPoint(new PolarCoordinate(1d, Math.PI * (5d / 4d)));
        assertEquals(tmp.getRadius(), EuclideanLine.distanceFromPointToEuclideanSegment(tmp, unitLine, O, unit), margeErreur);
        tmp = new EuclideanPoint(new PolarCoordinate(2d, Math.PI / 4d));
        assertEquals(tmp.distance(unit), EuclideanLine.distanceFromPointToEuclideanSegment(tmp, unitLine, O, unit), margeErreur);
    }
}
