package euclidean;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestEuclideanCircle extends TestEuclidean {
    @Test
    public void testIntersections() {
        List<EuclideanPoint> intersections;
        EuclideanCircle circle;
        EuclideanPoint firstPoint, secondPoint;
        double distance, angle;

        circle = new EuclideanCircle(unit, 0.1d);
        intersections = unitCircle.getIntersectionPoints(circle);
        assertEquals(0, intersections.size());

        for(double i = 0d; i < 10d; i++) {
            circle = new EuclideanCircle(O, i/10d);
            intersections = unitCircle.getIntersectionPoints(circle);
            assertEquals(0, intersections.size());
        }

        for(double i = 0d; i < 10d; i++) {
            angle = (Math.PI*i) / 5d;
            firstPoint = new EuclideanPoint(new PolarCoordinate(1d, angle));
            secondPoint = new EuclideanPoint(new PolarCoordinate(2d, angle));
            circle = new EuclideanCircle(secondPoint, 1d);

            intersections = unitCircle.getIntersectionPoints(circle);
            assertEquals(1, intersections.size());
            distance = firstPoint.distance(intersections.get(0));
            assertEquals(0d, distance, margeErreur);
        }

        circle = new EuclideanCircle(i, 1d);
        intersections = unitCircle.getIntersectionPoints(circle);
        assertEquals(2, intersections.size());
        firstPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI/3d));
        secondPoint = new EuclideanPoint(new PolarCoordinate(1d, -Math.PI/3d));

        distance = firstPoint.distance(intersections.get(0));
        assertEquals(0d, distance, margeErreur);
        distance = secondPoint.distance(intersections.get(1));
        assertEquals(0d, distance, margeErreur);

        circle = new EuclideanCircle(j, 1d);
        intersections = unitCircle.getIntersectionPoints(circle);
        assertEquals(2, intersections.size());
        firstPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI/3d + Math.PI/2d));
        secondPoint = new EuclideanPoint(new PolarCoordinate(1d, -Math.PI/3d + Math.PI/2d));

        distance = firstPoint.distance(intersections.get(0));
        assertEquals(0d, distance, margeErreur);
        distance = secondPoint.distance(intersections.get(1));
        assertEquals(0d, distance, margeErreur);
    }

    @Test
    public void testDistanceFromPointToCircle() {
        assertEquals(0d, unitCircle.distance(i), margeErreur);
        assertEquals(0d, unitCircle.distance(j), margeErreur);
        assertEquals(unit.distance(new EuclideanPoint(new PolarCoordinate(1d, Math.PI/4d))), unitCircle.distance(unit), margeErreur);
    }

    @Test
    public void testDistanceFromPointToArc() {
        assertEquals(0d, unitCircle.distance(i, -Math.PI/3d, Math.PI/3d), margeErreur);
        assertEquals(0d, unitCircle.distance(i, Math.PI/3d, -Math.PI/3d), margeErreur);
        assertEquals(i.distance(j), unitCircle.distance(i, Math.PI/2d, Math.PI), margeErreur);
    }
}
