package euclidean;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;

import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dolezv
 */
public class TestEuclideanPoint extends TestEuclidean {

    private EuclideanPoint tmp;

    @Test
    public void testPolarAndCarthesianMatch() {
        EuclideanPoint polarPoint, euclideanPoint;
        polarPoint = new EuclideanPoint(new PolarCoordinate(0d, 0d));
        assertEquals(O, polarPoint);
        polarPoint = new EuclideanPoint(new PolarCoordinate(0d, Math.PI));
        assertEquals(O, polarPoint);
        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, 0d));
        assertEquals(i, polarPoint);
        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI/2d));
        assertEquals(j, polarPoint);
        polarPoint = new EuclideanPoint(new PolarCoordinate(Math.sqrt(2d), Math.PI/4d));
        assertEquals(unit, polarPoint);
        polarPoint = new EuclideanPoint(new PolarCoordinate(Math.sqrt(2d), Math.PI + Math.PI/4d));
        assertEquals(EuclideanPoint.multiply(unit, -1d), polarPoint);

        euclideanPoint = new EuclideanPoint(1d, 1d);
        assertEquals(Math.sqrt(2d), euclideanPoint.getRadius(), margeErreur);
        euclideanPoint = new EuclideanPoint(1d, -1d);
        assertEquals(Math.sqrt(2d), euclideanPoint.getRadius(), margeErreur);
        euclideanPoint = new EuclideanPoint(-1d, 1d);
        assertEquals(Math.sqrt(2d), euclideanPoint.getRadius(), margeErreur);
        euclideanPoint = new EuclideanPoint(-1d, -1d);
        assertEquals(Math.sqrt(2d), euclideanPoint.getRadius(), margeErreur);

        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, 0d));
        assertEquals(1d, polarPoint.getX(), margeErreur);
        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI));
        assertEquals(-1d, polarPoint.getX(), margeErreur);
        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, Math.PI/2d));
        assertEquals(1d, polarPoint.getY(), margeErreur);
        polarPoint = new EuclideanPoint(new PolarCoordinate(1d, -Math.PI/2d));
        assertEquals(-1d, polarPoint.getY(), margeErreur);
    }

    @Test
    public void testEuclideanPointSum() {
        tmp = EuclideanPoint.sum(O, i);
        assertEquals(i, tmp);
        tmp = EuclideanPoint.sum(O, j);
        assertEquals(j, tmp);
        tmp = EuclideanPoint.sum(i, j);
        assertEquals(unit, tmp);

        tmp = new EuclideanPoint(O);
        tmp.sum(1d);
        assertEquals(i, tmp);
        tmp.sum(j);
        assertEquals(unit, tmp);
        tmp.sum(-1d);
        assertEquals(j, tmp);
    }

    @Test
    public void testEuclideanPointSubtract() {
        tmp = EuclideanPoint.subtract(i, i);
        assertEquals(O, tmp);

        tmp = EuclideanPoint.subtract(j, j);
        assertEquals(O, tmp);
    }

    @Test
    public void testEuclideanPointMultiply() {
    	EuclideanPoint inverseI = EuclideanPoint.multiply(i, -1d);
    	assertEquals(new EuclideanPoint(-1d, 0d, ""), inverseI);
    	
    	EuclideanPoint inverseJ = EuclideanPoint.multiply(j, -1d);
    	assertEquals(new EuclideanPoint(0d, -1d, ""), inverseJ);
    	
    	EuclideanPoint xy = new EuclideanPoint(4d, 3d);
    	EuclideanPoint inverseXY = EuclideanPoint.multiply(xy, -1d);
    	assertEquals(new EuclideanPoint(-4d, -3d), inverseXY);
    	EuclideanPoint oppositeXY = new EuclideanPoint(new PolarCoordinate(xy.getRadius(), xy.getAngle()+Math.PI));
    	assertEquals(new EuclideanPoint(-4d, -3d), oppositeXY);
    }

    @Test
    public void testEuclideanPointAngleAlwaysBetweenZeroAndTwoPI() {
        assertEquals(0d, i.getAngle(), margeErreur);
        assertEquals(Math.PI/2d, j.getAngle(), margeErreur);
        assertEquals(Math.PI/4d, unit.getAngle(), margeErreur);

        EuclideanPoint tmp = new EuclideanPoint();
        tmp.setPolar(1d, 3d*Math.PI / 2d);
        assertEquals(3d*Math.PI / 2d, tmp.getAngle(), margeErreur);
        tmp.setPolar(1d, -Math.PI/2d);
        assertEquals(3d*Math.PI / 2d, tmp.getAngle(), margeErreur);
        tmp.setPolar(1d, -Math.PI/2d + 4*Math.PI);
        assertEquals(3d*Math.PI / 2d, tmp.getAngle(), margeErreur);
        tmp.setPolar(1d, -Math.PI/2d - 24*Math.PI);
        assertEquals(3d*Math.PI / 2d, tmp.getAngle(), margeErreur);
    }
}
