package euclidean;

import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanCircle;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanLine;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;

public class TestEuclidean {

    protected final double margeErreur;
    protected final EuclideanPoint O, i, j, unit;
    protected final EuclideanLine Oi, Oj, ij, unitLine;
    protected final EuclideanCircle unitCircle;

    public TestEuclidean() {
        margeErreur = Math.pow(10d, -7d);

        O = new EuclideanPoint(0d, 0d, "O");
        i = new EuclideanPoint(1d, 0d, "i");
        j = new EuclideanPoint(0d, 1d, "j");
        unit = new EuclideanPoint(1d, 1d);

        Oi = new EuclideanLine(O, i, "Oi");
        Oj = new EuclideanLine(O, j, "Oj");
        ij = new EuclideanLine(i, j, "ij");
        unitLine = new EuclideanLine(O, unit, "unitLine");

        unitCircle = new EuclideanCircle(1d, "unitcircle");
    }
}
