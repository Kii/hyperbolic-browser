package hyperbolic;

import euclidean.TestEuclidean;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.BeltramiDiskPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class TestBeltramiKleinDisk extends TestEuclidean {

    @Test
    public void testFonctionDistance() {
        int max = 100;
        double step = 1d/(double)max, stepTmp;
        for(double i = 0d; i < max; i = i + 1d) {
            stepTmp = step*i;
            assertEquals(stepTmp, BeltramiDiskPoint.hyperbolicDistanceFromDiskCenter(BeltramiDiskPoint.euclideanDistanceFromDiskCenter(stepTmp)), margeErreur);
            assertEquals(stepTmp, BeltramiDiskPoint.euclideanDistanceFromDiskCenter(BeltramiDiskPoint.hyperbolicDistanceFromDiskCenter(stepTmp)), margeErreur);
        }
    }

    @Test
    public void testRotation() {
        TransformablePoint complexCenter = new BeltramiDiskPoint(0.2d, 0.5d);
        TransformablePoint complexMousePosition = new BeltramiDiskPoint(0.5d, 0.5d);

        double distanceBetween = complexCenter.distance(complexMousePosition);

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        for(double i = 1; i <= 4; i++) {
            BeltramiDiskPoint.rotate(Math.PI/i, listComplexPoint);
            assertEquals(distanceBetween, listComplexPoint.get(0).distance(listComplexPoint.get(1)), margeErreur);
        }
    }

    @Test
    public void testTranslationPreserveDistances() {
        TransformablePoint complexCenter = new BeltramiDiskPoint(0.2d, -0.5d);
        TransformablePoint complexMousePosition = new BeltramiDiskPoint(0.5d, 0.5d);

        double oldDistanceBetween, newDistanceBetween, dist;

        oldDistanceBetween = complexCenter.distance(complexMousePosition);

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        BeltramiDiskPoint.translate(0.3d, listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        BeltramiDiskPoint.rotate(Math.PI / 3d, listComplexPoint);
        BeltramiDiskPoint.translate(0.2d, listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        BeltramiDiskPoint.translate(new EuclideanPoint(), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        BeltramiDiskPoint.translate(new EuclideanPoint(new PolarCoordinate(0.9d, Math.PI/4d)), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);
    }

    @Test
    public void testCenterPoint() {
        TransformablePoint complexCenter = new BeltramiDiskPoint(0.2d, -0.5d);
        TransformablePoint complexMousePosition = new BeltramiDiskPoint(0.5d, 0.5d);
        double dist, oldDistanceBetween, newDistanceBetween;

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        oldDistanceBetween = complexCenter.distance(complexMousePosition);

        BeltramiDiskPoint.translate(new EuclideanPoint(listComplexPoint.get(0)), listComplexPoint);
        dist = O.distance(listComplexPoint.get(0));
        assertEquals(0d, dist, margeErreur);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        BeltramiDiskPoint.translate(new EuclideanPoint(listComplexPoint.get(1)), listComplexPoint);
        dist = O.distance(listComplexPoint.get(1));
        assertEquals(0d, dist, margeErreur);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);
    }

    @Test
    public void testMiddlePoint() {
        TransformablePoint complexCenter, complexMousePosition, middlePoint;
        double hyperDistance, d1, d2;

        complexCenter = new BeltramiDiskPoint(EuclideanPoint.ORIGIN);
        complexMousePosition = new BeltramiDiskPoint(0.5d, 0d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);

        complexCenter = new BeltramiDiskPoint(0d, 0.5d);
        complexMousePosition = new BeltramiDiskPoint(0.5d, 0d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);

        complexCenter = new BeltramiDiskPoint(-0.5d, 0d);
        complexMousePosition = new BeltramiDiskPoint(0.5d, 0d);
        assertNotSame(complexCenter, complexMousePosition);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        System.out.println(complexCenter);
        System.out.println(complexMousePosition);
        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);
        assertEquals(0d, middlePoint.getRadius(), margeErreur);

        complexCenter = new BeltramiDiskPoint(0d, 0.5d);
        complexMousePosition = new BeltramiDiskPoint(0d, -0.5d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);
        assertEquals(0d, middlePoint.getRadius(), margeErreur);
    }
}
