package hyperbolic;

import euclidean.TestEuclidean;
import hyperbolicbrowser.geometricmodels.geometricobjects.EuclideanPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.PoincareDiskPoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.PoincareHalfPlanePoint;
import hyperbolicbrowser.geometricmodels.geometricobjects.coordinates.PolarCoordinate;
import hyperbolicbrowser.geometricmodels.geometricobjects.points.TransformablePoint;
import hyperbolicbrowser.utils.mUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestPoincareHalfplane extends TestEuclidean {

    @Test
    public void testRotation() {
        TransformablePoint complexCenter = new PoincareHalfPlanePoint(0.2d, 0.5d);
        TransformablePoint complexMousePosition = new PoincareHalfPlanePoint(0.5d, 0.5d);

        double distanceBetween = complexCenter.distance(complexMousePosition);

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        for(double i = 1; i <= 4; i++) {
            PoincareHalfPlanePoint.rotate(Math.PI/i, listComplexPoint);
            assertEquals(distanceBetween, listComplexPoint.get(0).distance(listComplexPoint.get(1)), margeErreur);
        }
    }

    @Test
    public void testTranslationPreserveDistances() {
        TransformablePoint complexCenter = new PoincareHalfPlanePoint(1d, 0.5d);
        TransformablePoint complexMousePosition = new PoincareHalfPlanePoint(0.5d, 0.5d);

        double oldDistanceBetween, newDistanceBetween;

        oldDistanceBetween = complexCenter.distance(complexMousePosition);

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(0.3d, 0.1d), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        PoincareHalfPlanePoint.rotate(Math.PI / 3d, listComplexPoint);
        PoincareHalfPlanePoint.translate(new EuclideanPoint(0.2d, 2d), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(0.2d, 2d), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(new PolarCoordinate(0.9d, Math.PI/4d)), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(new PolarCoordinate(0.1d, Math.PI/7d)), listComplexPoint);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);
    }

    @Test
    public void testCenterPoint() {
        TransformablePoint origin = mUtils.diskToHalfPlane(new PoincareDiskPoint(EuclideanPoint.ORIGIN));

        TransformablePoint complexCenter = new PoincareHalfPlanePoint(-3d, 2.5d);
        TransformablePoint complexMousePosition = new PoincareHalfPlanePoint(0.5d, 0.5d);
        double dist, oldDistanceBetween, newDistanceBetween;

        List<TransformablePoint> listComplexPoint = new ArrayList<>(2);
        listComplexPoint.add(complexCenter);
        listComplexPoint.add(complexMousePosition);

        oldDistanceBetween = complexCenter.distance(complexMousePosition);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(listComplexPoint.get(0)), listComplexPoint);
        dist = origin.distance(listComplexPoint.get(0));
        assertEquals(0d, dist, margeErreur);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);

        PoincareHalfPlanePoint.translate(new EuclideanPoint(listComplexPoint.get(1)), listComplexPoint);
        dist = origin.distance(listComplexPoint.get(1));
        assertEquals(0d, dist, margeErreur);
        newDistanceBetween = listComplexPoint.get(0).distance(listComplexPoint.get(1));
        assertEquals(oldDistanceBetween, newDistanceBetween, margeErreur);
    }

    @Test
    public void testMiddlePoint() {
        TransformablePoint complexCenter, complexMousePosition, middlePoint;
        double hyperDistance, d1, d2;

        complexCenter = new PoincareHalfPlanePoint(3d, 3d);
        complexMousePosition = new PoincareHalfPlanePoint(-3d, 3d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(0d, middlePoint.getX(), margeErreur);
        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);


        complexCenter = new PoincareHalfPlanePoint(0d, 0.5d);
        complexMousePosition = new PoincareHalfPlanePoint(0.5d, 0.5d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);

        complexCenter = new PoincareHalfPlanePoint(-0.5d, 0.5d);
        complexMousePosition = new PoincareHalfPlanePoint(0.5d, 0.5d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);
        assertEquals(0d, middlePoint.getX(), margeErreur);

        complexCenter = new PoincareHalfPlanePoint(0d, 0.5d);
        complexMousePosition = new PoincareHalfPlanePoint(0d, 1d);
        middlePoint = complexCenter.getMiddlePoint(complexMousePosition);

        hyperDistance = complexCenter.distance(complexMousePosition);;
        d1 = complexCenter.distance(middlePoint);
        d2 = middlePoint.distance(complexMousePosition);

        assertEquals(d1, d2, margeErreur);
        assertEquals(d1 + d2, hyperDistance, margeErreur);
        assertEquals(0d, middlePoint.getX(), margeErreur);
    }
}
